﻿*** Settings ***
Library             Selenium2Library
Library             DatabaseLibrary
Library             BuiltIn
Library             String
Library             openpyxl
Library             ExcelLibrary
Library             Collections
Library             OperatingSystem
Library             Screenshot
Resource            Skootar_Keywords_Resource/skootar-calreturntrip-keywords-file.robot
Variables           Skootar_Variables/skootar-variables.py
Variables           Skootar_Xpaths/skootar-calreturntrip-xpath.py
Suite Setup         Connect DB
Suite Teardown      Test Teardown Testcase 75

*** Test Cases ***
Test step 0: Connect DB                                     Connect DB
Test step 1: Open browser                                   Open Browser for Run Test
Test step 2: Login                                          Login with Email
Test step 3: Login Success                                  Login Success
Test step 4: STEP1 : Detail Direction Origin                Profile Origin
Test step 5: STEP1 : Detail Direction Destination 2         Profile Destination 2
Test step 6: Next step                                      Go to Step 2
Test step 7: STEP2 : Choose Service                         Select Service
Test step 8: STEP2 : Choose Option                          Select Option
Test step 9: Next step                                      Go to Step 3
Test step 10: Check Distance                                Calculate Distance
Test step 11: Check Price                                   Calculate Price
Test step 12: STEP3 : Sum Job                               Check Point Pay
End Case                                                    End Case Testcase 75