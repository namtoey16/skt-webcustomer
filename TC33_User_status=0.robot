﻿*** Settings ***
Library             Selenium2Library
Library             DatabaseLibrary
Library             BuiltIn
Library             String
Library             openpyxl
Library             ExcelLibrary
Library             Collections
Library             OperatingSystem
Library             Screenshot
Resource            Skootar_Keywords_Resource/skootar-ban-keywords-file.robot
Variables           Skootar_Variables/skootar-variables.py
Variables           Skootar_Xpaths/skootar-ban0-xpath.py
Suite Setup         Connect DB
Suite Teardown      Test Teardown Testcase 33

*** Test Cases ***
# Check Price No Login
Connect DB status=0                 Connect DB status=0
Test step 1: Open Browser           Open Browser for Run Test
Test step 2: Login                  Login with Email
Test step 3: Alert OTP              Sent OTP to User
Test step 4: User receive OTP       Input OTP
Test step 5: Login Success          Login Success
End Case                            End Case Testcase 33