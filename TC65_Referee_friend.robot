﻿*** Settings ***
Library             Selenium2Library
Library             DatabaseLibrary
Library             BuiltIn
Library             String
Library             openpyxl
Library             ExcelLibrary
Library             Collections
Library             OperatingSystem
Library             Screenshot
Resource            Skootar_Keywords_Resource/skootar-referfriend-keywords-file.robot
Variables           Skootar_Variables/skootar-variables.py
Variables           Skootar_Xpaths/skootar-referfriend-xpath.py
Suite Setup         Connect DB
Suite Teardown      Test Teardown Testcase 65

*** Test Cases ***
Test step 0: Connect DB                                     Connect DB
Test step 1: Open browser                                   Open Browser for Run Test
Test step 2: Open Link Refer                                Open Link Refer
Test step 3: Signup Refer                                   Signup Refer
Test step 4: Input Form Signup                              Input Form Signup
Test step 5: Click Signup                                   Click Signup
Test step 6: Check Free Credit Referer Before               Check Free Credit Referer Before
Test step 7: Input OTP                                      Input OTP
Test step 8: Setup Profile                                  Setup Profile
Test step 9: Check Free Credit Referee                      Check Free Credit Referee
Test step 10: Order Job                                     Order Job
Test step 11: Check Free Credit Referer After               Check Free Credit Referer After
Test step 12: Clear Referee DB                              Clear Referee DB
End Case                                                    End Case Testcase 65