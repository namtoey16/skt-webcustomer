﻿*** Settings ***
Library             Selenium2Library
Library             DatabaseLibrary
Library             BuiltIn
Library             String
Library             openpyxl
Library             ExcelLibrary
Library             Collections
Library             OperatingSystem
Library             Screenshot
Resource            Skootar_Keywords_Resource/skootar-ban-keywords-file.robot
Variables           Skootar_Variables/skootar-variables.py
Variables           Skootar_Xpaths/skootar-ban-2-xpath.py
Suite Setup         Connect DB
Suite Teardown      Test Teardown Testcase 36

*** Test Cases ***
# Check Price No Login
Connect DB                          Connect DB
Test step 1: Open browser           Open Browser for Run Test
Test step 2: Login                  Login with Email
Test step 3: Alert ban              Login Fail Ban -2
End Case                            End Case Testcase 36