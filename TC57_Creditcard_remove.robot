﻿*** Settings ***
Library             Selenium2Library
Library             DatabaseLibrary
Library             BuiltIn
Library             String
Library             openpyxl
Library             ExcelLibrary
Library             Collections
Library             OperatingSystem
Library             Screenshot
Resource            Skootar_Keywords_Resource/skootar-promotion-keywords-file.robot
Variables           Skootar_Variables/skootar-variables.py
Variables           Skootar_Xpaths/skootar-promotion-xpath.py
Suite Setup         Connect DB
Suite Teardown      Test Teardown Testcase 57


*** Test Cases **
Test step 0: Connect DB                 Connect DB
Test step 1: Open browser               Open Browser for Run Test
Test step 2: Login                      Login with Email Coperate
Test step 3: Login Success              Login Success
Test step 4: Go to Payment              Go to Payment
Remove Creditcard                       Remove Creditcard
End Case                                End Case Testcase 57