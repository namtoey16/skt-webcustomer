﻿*** Settings ***
Library             Selenium2Library
Library             DatabaseLibrary
Library             BuiltIn
Library             String
Library             openpyxl
Library             ExcelLibrary
Library             Collections
Library             OperatingSystem
Library             Screenshot
Resource            Skootar_Keywords_Resource/skootar-payinvoice-keywords-file.robot
Variables           Skootar_Variables/skootar-variables.py
Variables           Skootar_Xpaths/skootar-payinvoice-xpath.py
Suite Setup         Connect DB
Suite Teardown      Test Teardown Testcase 42

*** Test Cases ***
# Check Price No Login
Test step 0: Connect DB                             Connect DB
Test step 1: Open browser                           Open Browser for Run Test
Test step 2: Login                                  Login with Email
Test step 3: Alert ban                              Login Fail Ban -1
Test step 4: Close Alert Ban                        Close Alert Ban
Test step 5: Status Ban User                        Status Ban User
Test step 6: Login Success                          Login Success
Test step 7: Alert Invoice                          Alert Invoice
Test step 8: Go To Tab Invoice                      Go To Tab Invoice
Test step 9: Pay Invoice                            Pay Invoice
Test step 10: Status Unban User                     Status Unban User
End Case                                            End Case Testcase 42