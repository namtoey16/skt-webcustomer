﻿*** Settings ***
Library             Selenium2Library
Library             DatabaseLibrary
Library             BuiltIn
Library             String
Library             openpyxl
Library             ExcelLibrary
Library             Collections
Library             OperatingSystem
Library             Screenshot
Resource            Skootar_Keywords_Resource/skootar-referfriend-keywords-file.robot
Variables           Skootar_Variables/skootar-variables.py
Variables           Skootar_Xpaths/skootar-referfriend-xpath.py
Suite Setup         Connect DB
Suite Teardown      Test Teardown Testcase 64

*** Test Cases ***
Test step 0: Connect DB                                     Connect DB
Test step 1: Open browser                                   Open Browser for Run Test
Test step 2: Login                                          Login with Email
Test step 3: Login Success                                  Login Success
Test step 4: STEP1 : Refer Friend                           Refer Friend
Test step 5: STEP2 : Copy Link For Share                    Copy Link For Share
Test step 6: Open Link Refer                                Open Link Refer
Test step 7: Signup Refer                                   Signup Refer
End Case                                                    End Case Testcase 64