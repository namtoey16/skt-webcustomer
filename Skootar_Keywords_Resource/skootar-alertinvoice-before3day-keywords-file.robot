﻿*** Keywords ***

Connect DB
    connect to database  pymysql    ${DB_name}  ${DB_user}  ${DB_pass}  ${DB_host}  ${DB_port}
    Execute SQL String   ${query_invoice_before3day}


Account Customer
    Open Excel Document     ${path_test_report}customer.xlsx     doc_id=doc1
    ${username}=  Read Excel Cell   row_num=2       col_num=1
    ${password}=  Read Excel Cell   row_num=2       col_num=2
    Log To Console                                  \n${user_name}
    Log To Console                                  ${password}
    Save Excel Document                             ${path_test_report}customer.xlsx
    Close All Excel Documents


Open Browser for Run Test
    Open Browser       ${URL}       ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed   0.3
    Sleep    2s


Login with Email
    Click Element                   ${LOG_IN}
    Wait Until Page Contains Element        ${MAIL}
    Wait Until Page Contains Element        ${PAS_WORD}
    Input Text                              ${MAIL}           ${EMAIL_ALR_INVOICE}
    Input Text                              ${PAS_WORD}        ${PASSWORD_ALR_INVOICE}
    Click Element                           ${LOGIN_EMAIL}


Login Success
    Wait Until Page Does Not Contain Element          ${P_LOADER}
    Wait Until Page Contains Element                  ${LOGO}
    Page Should Contain Element                       ${Icon1}
    Page Should Contain Element                       ${Icon2}
    Page Should Contain Element                       ${Icon3}
    Page Should Contain Element                       ${Icon4}
    Page Should Contain Element                       ${Icon5}
    Page Should Contain Element                       ${Icon6}
    Page Should Contain Element                       ${Icon7}
    Page Should Contain Element                       ${Icon8}
    Page Should Contain Element                       ${Icon9}
    Page Should Contain Element                       ${Icon10}


Alert Invoice
    Wait Until Page Contains       ใบแจ้งหนี้ของคุณ จะครบกำหนดชำระในอีก
    Page Should Contain            ใบแจ้งหนี้ของคุณ จะครบกำหนดชำระในอีก


Sent OTP to User
    Wait Until Page Contains       กรุณากรอกรหัส OTP ที่ถูกส่งไปที่เบอร์
    Page Should Contain            กรุณากรอกรหัส OTP ที่ถูกส่งไปที่เบอร์


Input OTP
#    Execute SQL String   ${query_user_otp}
    ${query_user_otp}=      Query    SELECT verify_code FROM tbl_user WHERE user_name = 'namtoey16@gmail.com';
    set test variable  ${query_user_otp}   ${query_user_otp[0][0]}
    Input Text      ${input_otp}        ${query_user_otp}


Login Fail Ban -1
    Wait Until Page Contains       คุณถูกระงับการใช้งานชั่วคราว โปรดติดต่อ 02-105-4429
    Page Should Contain            คุณถูกระงับการใช้งานชั่วคราว โปรดติดต่อ 02-105-4429


Login Fail Ban -2
    Wait Until Page Contains       บัญชีนี้ถูกระงับการใช้งาน
    Page Should Contain            บัญชีนี้ถูกระงับการใช้งาน


End Case Testcase 38
    #Take Screenshot                     ${path_test_report}TC_${INDEX_TESTCASE38}.jpg
    Close Browser


#===========================================================================================================================

Test Teardown Testcase 38
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE38}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE38}
    Disconnect From Database


Save Test Result Pass Testcase 38
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE38}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE38}.html
    ${INDEX_TESTCASE38}=             Evaluate            int(${INDEX_TESTCASE38}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE38}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

Save Test Result Fail Testcase 38
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE38}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE38}.html
    ${INDEX_TESTCASE38}=             Evaluate            int(${INDEX_TESTCASE38}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE38}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#===========================================================================================================================
