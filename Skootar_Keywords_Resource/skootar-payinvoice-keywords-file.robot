﻿*** Keywords ***

Connect DB
    connect to database  pymysql    ${DB_name}  ${DB_user}  ${DB_pass}  ${DB_host}  ${DB_port}
    Execute SQL String   ${query_user_status}
    Execute SQL String   ${query_invoice_over}


Account Customer
    Open Excel Document     ${path_test_report}customer.xlsx     doc_id=doc1
    ${username}=  Read Excel Cell   row_num=2       col_num=1
    ${password}=  Read Excel Cell   row_num=2       col_num=2
    Log To Console                                  \n${user_name}
    Log To Console                                  ${password}
    Save Excel Document                             ${path_test_report}customer.xlsx
    Close All Excel Documents


Open Browser for Run Test
    Open Browser       ${URL}       ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed   0.3
    Sleep    2s


Login with Email
    Click Element                                      ${LOG_IN}
    Wait Until Page Contains Element                   ${MAIL}
    Wait Until Page Contains Element                   ${PAS_WORD}
    Input Text                  ${MAIL}                ${EMAIL_ALR_INVOICE}
    Input Text                  ${PAS_WORD}            ${PASSWORD_ALR_INVOICE}
    Click Element                                      ${LOGIN_EMAIL}


Login Fail Ban -1
    Wait Until Page Contains                          คุณถูกระงับการใช้งานชั่วคราว โปรดติดต่อ 02-105-4429
    Page Should Contain                               คุณถูกระงับการใช้งานชั่วคราว โปรดติดต่อ 02-105-4429


Status Ban User
    ${status_ban_user}=    Query    select user_status from SKOOTAR_DEV.tbl_user where user_name="12345company@gmail.com";
    set test variable           ${status_ban_user}         ${status_ban_user[0][0]}
    Log To Console              ${\n}User status is ${status_ban_user}


Close Alert Ban
    Wait Until Page Contains                          ยืนยัน
    Click Button                                      ยืนยัน


Login Success
    Wait Until Page Does Not Contain Element          ${P_LOADER}
    Wait Until Page Contains Element                  ${LOGO}
    Page Should Contain Element                       ${Icon1}
    Page Should Contain Element                       ${Icon2}
    Page Should Contain Element                       ${Icon3}
    Page Should Contain Element                       ${Icon4}
    Page Should Contain Element                       ${Icon5}
    Page Should Contain Element                       ${Icon6}
    Page Should Contain Element                       ${Icon7}
    Page Should Contain Element                       ${Icon8}
    Page Should Contain Element                       ${Icon9}
    Page Should Contain Element                       ${Icon10}


Alert Invoice
    Wait Until Page Contains       ใบแจ้งหนี้ของคุณเลยกำหนดชำระแล้ว
    Page Should Contain            ใบแจ้งหนี้ของคุณเลยกำหนดชำระแล้ว


Go To Tab Invoice
#    Click Element                       ${Icon3}
    Click Button                                    ไปหน้าชำระเงิน
    Wait Until Page Contains Element                ${INV_STATUS_1}
    Location Should Be                              https://uat.skootar.com/invoice

Pay Invoice
    Click Button                                    ${PAY_INVOICE}
    Wait Until Page Contains                        ต้องการหักภาษี ณ ที่จ่าย หรือไม่
    Click Button                                    ถัดไป
    Wait Until Page Contains                        คิวอาร์โค้ดผ่านแอปฯ
    Click Element                                   ${PAYMENT_INVOICE}
    Wait Until Page Contains Element                ${CHOOSE_CREDITCARD}
    Click Element                                   ${CHOOSE_CREDITCARD}
    Scroll Element Into View                        ${PAY_INVOICE_SUCCESS}
    Click Button                                    ${PAY_INVOICE_SUCCESS}
    Wait Until Page Contains                        คุณต้องการชำระเงินผ่านบัตรเครดิต
    Click Button                                    ยืนยัน
    Wait Until Page Contains                        ชำระเงินเรียบร้อยแล้ว


Status Unban User
    ${status_unban_user}=    Query    select user_status from SKOOTAR_DEV.tbl_user where user_name="12345company@gmail.com";
    set test variable           ${status_unban_user}         ${status_unban_user[0][0]}
    Log To Console              ${\n}Now user status is ${status_unban_user}


End Case Testcase 42
    #Take Screenshot                     ${path_test_report}TC_${INDEX_TESTCASE42}.jpg
    Close Browser


#===========================================================================================================================

Test Teardown Testcase 42
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE42}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE42}
    Disconnect From Database


Save Test Result Pass Testcase 42
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE42}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE42}.html
    ${INDEX_TESTCASE42}=             Evaluate            int(${INDEX_TESTCASE42}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE42}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

Save Test Result Fail Testcase 42
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE42}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE42}.html
    ${INDEX_TESTCASE42}=             Evaluate            int(${INDEX_TESTCASE42}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE42}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#=======================================================================================================================