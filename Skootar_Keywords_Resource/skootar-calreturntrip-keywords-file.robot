﻿*** Keywords ***

Connect DB
    connect to database  pymysql    ${DB_name}  ${DB_user}  ${DB_pass}  ${DB_host}  ${DB_port}
    Execute SQL String   ${query_set_usergroup}
#    Execute SQL String   ${query_user_freecredit}
#    ${query_user_freecredit} =    Query    SELECT free_credit FROM tbl_user WHERE user_name = 'namtoey@skootar.com';
#    ${DB_free_credit}=    Set Variable    ${query_user_freecredit[0][0]}
#    Log to Console      ${DB_free_credit}


Open Browser for Run Test
    Open Browser       ${URL}       ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed   0.3
    Sleep    2s

Login with Email
    Click Element                           ${LOG_IN}
    Wait Until Page Contains Element        ${MAIL}
    Wait Until Page Contains Element        ${PAS_WORD}
    Input Text                              ${MAIL}            ${EMAIL_EXTRA2}
    Input Text                              ${PAS_WORD}        ${PASSWORD_EXTRA2}
    Click Element                           ${LOGIN_EMAIL}


New User
    Wait Until Page Contains           ยินดีต้อนรับลูกค้าใหม่
    Click Button                       ยืนยัน


Alert Promotion
    Wait Until Page Contains         งาน Thailand E-Commerce รับส่วนลด 8% ไม่จำกัดจำนวนครั้ง
    Click Button                     ${Copycode}
    Wait Until Page Contains         คัดลอกโค้ดสำเร็จแล้ว


Login Success
    Wait Until Page Does Not Contain Element          ${P_LOADER}
    Wait Until Page Contains Element                  ${LOGO}
    Page Should Contain Element                       ${Icon1}
    Page Should Contain Element                       ${Icon2}
    Page Should Contain Element                       ${Icon3}
    Page Should Contain Element                       ${Icon4}
    Page Should Contain Element                       ${Icon5}
    Page Should Contain Element                       ${Icon6}
    Page Should Contain Element                       ${Icon7}
    Page Should Contain Element                       ${Icon8}
    Page Should Contain Element                       ${Icon9}
    Page Should Contain Element                       ${Icon10}


Profile Origin
    Click Element                    ${Add1stplace}
    Input Text              ${LOC_1}     อโศก\ue007
    Sleep                   3s
#    Click Element                    ${NowLocation}
    Click Element                        ${CHOOSE_LOC1}
    Wait Until Page Contains Element     ${NAM_ORG}
    Press Keys          ${NAM_ORG}       CTRL+a\ue017
    Input Text          ${NAM_ORG}       ${NAMEORG}
    Press Keys          ${PHON_ORG}       CTRL+a\ue017
    Input Text          ${PHON_ORG}      ${PHONEORG}
    Click Button                       ยืนยัน


Profile Destination 1
    Click Element                        ${Add2ndplace}
    Input Text              ${LOC_2}     สระบุรี\ue007
    Sleep                   3s
#    Wait Until Page Contains Element     ${CHOOSE_LOC2}
    Click Element                        ${CHOOSE_LOC2}
    Wait Until Page Contains Element     ${NAM_DES}
    Press Keys          ${NAM_DES}       CTRL+a\ue017
    Input Text          ${NAM_DES}       ${NAMEDES}
    Press Keys          ${PHON_DES}       CTRL+a\ue017
    Input Text          ${PHON_DES}      ${PHONEDES}
    Click Button                       ยืนยัน


Profile Destination 2
    Click Element                        ${Add2ndplace}
    Input Text              ${LOC_2}     ดอนเมือง\ue007
    Sleep                   3s
#    Wait Until Page Contains Element     ${CHOOSE_LOC2}
    Click Element                        ${CHOOSE_LOC2}
    Wait Until Page Contains Element     ${NAM_DES}
    Press Keys          ${NAM_DES}       CTRL+a\ue017
    Input Text          ${NAM_DES}       ${NAMEDES}
    Press Keys          ${PHON_DES}       CTRL+a\ue017
    Input Text          ${PHON_DES}      ${PHONEDES}
    Click Button                       ยืนยัน


Go to Step 2
#    Wait Until Page Contains           ราคานี้อาจมีการเปลี่ยนแปลงตามการวิ่งงานจริง เช่น กรณีแก้ไข/เพิ่ม/ลด สถานที่ มีค่าที่จอดรถ ค่ารอ ค่าพัสดุน้ำหนักเกิน
    Click Element                       ${NEXT_TO_STEP2}


Select Service
    Click Element              ${SELECT_SERVICE}



Select Option
    Wait Until Page Does Not Contain Element                ${P_LOADER}
    ${PRICE_OPTION}=            Query  ${query_price_option}
    set test variable           ${PRICE_OPTION}             ${PRICE_OPTION[0][0]}
    ${PRICE_OPTION}=            Evaluate                    float(${PRICE_OPTION})
    Set Global variable                                     ${PRICE_OPTION}
    Log To Console              \nPrice Option ${PRICE_OPTION} bath
    Click Element               ${SELECT_OPTION}


Calculate Distance
    Wait Until Page Does Not Contain Element                ${P_LOADER}
    ${TOTALDISTANCE}            Get Text        ${TOTALDISTANCE}
    Set Global variable                         ${TOTALDISTANCE}
    Log To Console              \nTotal Distance ${TOTALDISTANCE} km


Calculate Price
    Wait Until Page Does Not Contain Element                ${P_LOADER}
    ${PRICE_USERGROUP}=         Query  ${query_price.format(distance = ${TOTALDISTANCE})}
    set test variable           ${PRICE_USERGROUP}          ${PRICE_USERGROUP[0][0]}
    Set Global variable                         ${PRICE_USERGROUP}
    Run Keyword If              ${PRICE_OPTION} != 0.5    Option Not Return Trip
    Run Keyword If              ${PRICE_OPTION} == 0.5    Option Return Trip


Option Not Return Trip
        ${PRICE_USERGROUP}=         Evaluate                    float(${PRICE_USERGROUP})
        ${NETPRICE}=                Evaluate                    float(${PRICE_USERGROUP}+${PRICE_OPTION})
        ${NETPRICE}=                Evaluate                    math.ceil(${NETPRICE})      math
        ${NETPRICE}=                Evaluate                    int(${NETPRICE})
        ${PRICE_USERGROUP}          Convert To String           ${PRICE_USERGROUP}
        ${NETPRICE}                 Convert To String           ${NETPRICE}
        Log To Console              \nThis Is Price Option Not *0.5
        Log To Console              Price ${PRICE_USERGROUP} bath
        Log To Console              Option Price ${PRICE_OPTION} bath
        Log To Console              Net Price must be ${NETPRICE} bath
        Wait Until Page Contains Element                        ${CHK_PRICE}
        Element Text Should Be      ${CHK_PRICE}                ${NETPRICE}


Option Return Trip
        ${PRICE_USERGROUP}=         Evaluate                    float(${PRICE_USERGROUP})
        ${PRICEOPTION_RETURN}=      Evaluate                   float(${PRICE_USERGROUP}*${PRICE_OPTION})
        Set Global variable                                     ${PRICE_USERGROUP}
        Set Global variable                                     ${PRICEOPTION_RETURN}
        Run Keyword If              ${PRICEOPTION_RETURN} >= 200    Option Return Over 200฿
        Run Keyword If              ${PRICEOPTION_RETURN} < 200     Option Return Less than 200฿


Option Return Over 200฿
        ${NETPRICE}=                Evaluate                    float(${PRICE_USERGROUP}+200)
        ${NETPRICE}=                Evaluate                    math.ceil(${NETPRICE})      math
        ${NETPRICE}=                Evaluate                    int(${NETPRICE})
        ${PRICE_USERGROUP}          Convert To String           ${PRICE_USERGROUP}
        ${NETPRICE}                 Convert To String           ${NETPRICE}
        Log To Console              \nThis Is Price Option *0.5 And Over 200 bath
        Log To Console              Price ${PRICE_USERGROUP} bath
        Log To Console              Option Price 200 bath
        Log To Console              Net Price must be ${NETPRICE} bath
        Wait Until Page Contains Element                        ${CHK_PRICE}
        Element Text Should Be      ${CHK_PRICE}                ${NETPRICE}


Option Return Less than 200฿
        ${NETPRICE}=                Evaluate                    float(${PRICE_USERGROUP}+${PRICEOPTION_RETURN})
        ${NETPRICE}=                Evaluate                    math.ceil(${NETPRICE})      math
        ${NETPRICE}=                Evaluate                    int(${NETPRICE})
        ${PRICE_USERGROUP}          Convert To String           ${PRICE_USERGROUP}
        ${NETPRICE}                 Convert To String           ${NETPRICE}
        Log To Console              \nThis Is Price Option *0.5 And Less than 200 bath
        Log To Console              Price ${PRICE_USERGROUP} bath
        Log To Console              Option Price ${PRICEOPTION_RETURN} bath
        Log To Console              Net Price must be ${NETPRICE} bath
        Wait Until Page Contains Element                        ${CHK_PRICE}
        Element Text Should Be      ${CHK_PRICE}                ${NETPRICE}


Go to Step 3
    Sleep   3s
    Click Button                      ขั้นตอนถัดไป


Check Point Pay
    Page Should Contain                เลือกวิธีชำระเงิน


End Case Testcase 74
    #Take Screenshot                     ${path_test_report}TC_${INDEX_TESTCASE75}.jpg
    Close Browser


End Case Testcase 75
    #Take Screenshot                     ${path_test_report}TC_${INDEX_TESTCASE75}.jpg
    Close Browser


#===========================================================================================================================

Test Teardown Testcase 74
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE74}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE74}
    Disconnect From Database


Save Test Result Pass Testcase 74
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE74}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE74}.html
    ${INDEX_TESTCASE74}=             Evaluate            int(${INDEX_TESTCASE74}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE74}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents


Save Test Result Fail Testcase 74
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE74}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE74}.html
    ${INDEX_TESTCASE74}=             Evaluate            int(${INDEX_TESTCASE74}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE74}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#===========================================================================================================================

Test Teardown Testcase 75
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE75}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE75}
    Disconnect From Database


Save Test Result Pass Testcase 75
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE75}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE75}.html
    ${INDEX_TESTCASE75}=             Evaluate            int(${INDEX_TESTCASE75}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE75}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents


Save Test Result Fail Testcase 75
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE75}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE75}.html
    ${INDEX_TESTCASE75}=             Evaluate            int(${INDEX_TESTCASE75}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE75}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Move File                                           ${path_test_report}sum-test-report.xlsx          ${path_test_report}sum-test-report_${TODAY}.xlsx
    Close All Excel Documents

#===========================================================================================================================

