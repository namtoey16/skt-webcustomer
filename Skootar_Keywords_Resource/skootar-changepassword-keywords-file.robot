﻿*** Keywords ***


Connect DB
    connect to database  pymysql    ${DB_name}  ${DB_user}  ${DB_pass}  ${DB_host}  ${DB_port}
    Execute SQL String   ${query_set_usergroup}


Open Browser For Run Test
    Open Browser                    ${URL}              ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed   0.3
    Sleep    2s

Login With Email
    Click Element                           ${LOG_IN}
    Wait Until Page Contains Element        ${MAIL}
    Wait Until Page Contains Element        ${PAS_WORD}
    Input Text                              ${MAIL}             ${EMAIL_CHANGEPW}
    Input Text                              ${PAS_WORD}         ${PASSWORD_CHANGEPW1}
    Click Element                           ${LOGIN_EMAIL}


Login Success
    Wait Until Page Does Not Contain Element            ${P_LOADER}
    Wait Until Page Contains Element                    ${LOGO}
    Page Should Contain Element                         ${Icon1}
    Page Should Contain Element                         ${Icon2}
    Page Should Contain Element                         ${Icon3}
    Page Should Contain Element                         ${Icon4}
    Page Should Contain Element                         ${Icon5}
    Page Should Contain Element                         ${Icon6}
    Page Should Contain Element                         ${Icon7}
    Page Should Contain Element                         ${Icon8}
    Page Should Contain Element                         ${Icon9}
    Page Should Contain Element                         ${Icon10}


Go To Setting
    Click Element                                       ${Icon8}
    Wait Until Page Contains Element                    ${GOTO_CHANGEPW}


Go To Change Password
    Click Element                                       ${GOTO_CHANGEPW}
    Wait Until Page Contains Element                    ${CURRENT_PW}
    Wait Until Page Contains Element                    ${NEW_PW}
    Wait Until Page Contains Element                    ${CFNEW_PW}


Clear Field
    Press Keys                      ${CURRENT_PW}       CTRL+a\ue017
    Press Keys                      ${NEW_PW}           CTRL+a\ue017
    Press Keys                      ${CFNEW_PW}         CTRL+a\ue017


Test Case Under 6characters
    Input Text                      ${CURRENT_PW}       00000
    Input Text                      ${NEW_PW}           aaaa
    Input Text                      ${CFNEW_PW}         !@#$%&
    Wait Until Page Contains                            รหัสผ่านต้องมีอย่างน้อย 6 ตัวอักษร


Test Case Password Mismatch
    Input Text                      ${CURRENT_PW}       ${PASSWORD_CHANGEPW_1}
    Input Text                      ${NEW_PW}           999999
    Input Text                      ${CFNEW_PW}         888888
    Wait Until Page Contains                            รหัสผ่านไม่ตรงกัน


Test Case Invalid Password
    Input Text                      ${CURRENT_PW}       123456789
    Input Text                      ${NEW_PW}           ${PASSWORD_CHANGEPW_2}
    Input Text                      ${CFNEW_PW}         ${PASSWORD_CHANGEPW_2}
    Click Element                                       ${CF_CHANGEPW}

Test Case Set New Password
    Input Text                      ${CURRENT_PW}       ${PASSWORD_CHANGEPW_1}
    Input Text                      ${NEW_PW}           ${PASSWORD_CHANGEPW_2}
    Input Text                      ${CFNEW_PW}         ${PASSWORD_CHANGEPW_2}
    Click Element                                       ${CF_CHANGEPW}


Test Case Set Old Password
    Input Text                      ${CURRENT_PW}       ${PASSWORD_CHANGEPW_2}
    Input Text                      ${NEW_PW}           ${PASSWORD_CHANGEPW_1}
    Input Text                      ${CFNEW_PW}         ${PASSWORD_CHANGEPW_1}
    Click Element                                       ${CF_CHANGEPW}


Change Password Success
    Wait Until Page Contains                            ทำการเปลี่ยนรหัสผ่านเรียบร้อยแล้ว


Change Password Invalid
    Wait Until Page Contains                            รหัสผ่านผิด กรุณาลองใหม่อีกครั้ง


End Case Testcase 9
    #Take Screenshot                     ${path_test_report}TC_${INDEX_TESTCASE9}.jpg
    Close Browser


#===========================================================================================================================

Test Teardown Testcase 9
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE9}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE9}
    Disconnect From Database


Save Test Result Pass Testcase 9
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE9}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE9}.html
    ${INDEX_TESTCASE9}=             Evaluate            int(${INDEX_TESTCASE9}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE9}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents


Save Test Result Fail Testcase 9
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE9}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE9}.html
    ${INDEX_TESTCASE9}=             Evaluate            int(${INDEX_TESTCASE9}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE9}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#===========================================================================================================================
