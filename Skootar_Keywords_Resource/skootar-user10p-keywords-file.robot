﻿*** Keywords ***
Connect DB
    connect to database  pymysql    ${DB_name}  ${DB_user}  ${DB_pass}  ${DB_host}  ${DB_port}
    Execute SQL String   ${query_set_usergroup}


Account Customer
    Open Excel Document     ${path_test_report}customer.xlsx     doc_id=doc1
    ${username}=  Read Excel Cell   row_num=2       col_num=1
    ${password}=  Read Excel Cell   row_num=2       col_num=2
    Log To Console                                  \n${user_name}
    Log To Console                                  ${password}
    Save Excel Document                             ${path_test_report}customer.xlsx
    Close All Excel Documents


Open Browser for Run Test
    Open Browser       ${URL}       ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed   0.3
    Sleep    2s

Login with Email
    Click Element                   ${LOG_IN}
    Wait Until Page Contains Element        ${MAIL}
    Wait Until Page Contains Element        ${PAS_WORD}
    Input Text                              ${MAIL}           ${EMAIL}
    Input Text                              ${PAS_WORD}        ${PASSWORD}
    Click Element                           ${LOGIN_EMAIL}


New User
    Wait Until Page Contains           ยินดีต้อนรับลูกค้าใหม่
    Click Button                       ยืนยัน


Alert Promotion
    Wait Until Page Contains         งาน Thailand E-Commerce รับส่วนลด 8% ไม่จำกัดจำนวนครั้ง
    Click Button                     ${Copycode}
    Wait Until Page Contains         คัดลอกโค้ดสำเร็จแล้ว


Login Success
    Wait Until Page Does Not Contain Element          ${P_LOADER}
    Wait Until Page Contains Element                  ${LOGO}
    Page Should Contain Element                       ${Icon1}
    Page Should Contain Element                       ${Icon2}
    Page Should Contain Element                       ${Icon3}
    Page Should Contain Element                       ${Icon4}
    Page Should Contain Element                       ${Icon5}
    Page Should Contain Element                       ${Icon6}
    Page Should Contain Element                       ${Icon7}
    Page Should Contain Element                       ${Icon8}
    Page Should Contain Element                       ${Icon9}
    Page Should Contain Element                       ${Icon10}



Profile Origin
    Click Element                    ${Add1stplace}
    Click Element                    ${NowLocation}
    Wait Until Page Contains Element     ${NAM_ORG}
    Press Keys          ${NAM_ORG}       CTRL+a\ue017
    Input Text          ${NAM_ORG}       ${NAMEORG}
    Press Keys          ${PHON_ORG}       CTRL+a\ue017
    Input Text          ${PHON_ORG}      ${PHONEORG}
    Click Button                       ยืนยัน


Profile Destination 1
    Click Element                    ${Add2ndplace}
    Click Element                    ${NowLocation}
    Wait Until Page Contains Element     ${NAM_DES}
    Press Keys          ${NAM_DES}       CTRL+a\ue017
    Input Text          ${NAM_DES}       ${NAMEDES}
    Press Keys          ${PHON_DES}       CTRL+a\ue017
    Input Text          ${PHON_DES}      ${PHONEDES}
    Click Button                       ยืนยัน

Profile Destination 2
    Click Element                    ${Add3rdplace}
    Click Element                    ${NowLocation}
    Wait Until Page Contains Element     ${NAM_DES}
    Press Keys          ${NAM_DES}       CTRL+a\ue017
    Input Text          ${NAM_DES}       ${NAMEDES}
    Press Keys          ${PHON_DES}       CTRL+a\ue017
    Input Text          ${PHON_DES}      ${PHONEDES}
    Click Button                       ยืนยัน


Profile Destination 3
    Click Element                    ${Add4thplace}
    Click Element                    ${NowLocation}
    Wait Until Page Contains Element     ${NAM_DES}
    Press Keys          ${NAM_DES}       CTRL+a\ue017
    Input Text          ${NAM_DES}       ${NAMEDES}
    Press Keys          ${PHON_DES}       CTRL+a\ue017
    Input Text          ${PHON_DES}      ${PHONEDES}
    Click Button                       ยืนยัน


Profile Destination 4
    Click Element                    ${Add5thplace}
    Click Element                    ${NowLocation}
    Wait Until Page Contains Element     ${NAM_DES}
    Press Keys          ${NAM_DES}       CTRL+a\ue017
    Input Text          ${NAM_DES}       ${NAMEDES}
    Press Keys          ${PHON_DES}       CTRL+a\ue017
    Input Text          ${PHON_DES}      ${PHONEDES}
    Click Button                       ยืนยัน


Profile Destination 5
    Click Element                    ${Add6thplace}
    Click Element                    ${NowLocation}
    Wait Until Page Contains Element     ${NAM_DES}
    Press Keys          ${NAM_DES}       CTRL+a\ue017
    Input Text          ${NAM_DES}       ${NAMEDES}
    Press Keys          ${PHON_DES}       CTRL+a\ue017
    Input Text          ${PHON_DES}      ${PHONEDES}
    Click Button                       ยืนยัน


Profile Destination 6
    Click Element                    ${Add7thplace}
    Click Element                    ${NowLocation}
    Wait Until Page Contains Element     ${NAM_DES}
    Press Keys          ${NAM_DES}       CTRL+a\ue017
    Input Text          ${NAM_DES}       ${NAMEDES}
    Press Keys          ${PHON_DES}       CTRL+a\ue017
    Input Text          ${PHON_DES}      ${PHONEDES}
    Click Button                       ยืนยัน


Profile Destination 7
    Click Element                    ${Add8thplace}
    Click Element                    ${NowLocation}
    Wait Until Page Contains Element     ${NAM_DES}
    Press Keys          ${NAM_DES}       CTRL+a\ue017
    Input Text          ${NAM_DES}       ${NAMEDES}
    Press Keys          ${PHON_DES}       CTRL+a\ue017
    Input Text          ${PHON_DES}      ${PHONEDES}
    Click Button                       ยืนยัน


Profile Destination 8
    Click Element                    ${Add9thplace}
    Click Element                    ${NowLocation}
    Wait Until Page Contains Element     ${NAM_DES}
    Press Keys          ${NAM_DES}       CTRL+a\ue017
    Input Text          ${NAM_DES}       ${NAMEDES}
    Press Keys          ${PHON_DES}       CTRL+a\ue017
    Input Text          ${PHON_DES}      ${PHONEDES}
    Click Button                       ยืนยัน


Profile Destination 9
    Click Element                    ${Add10thplace}
    Click Element                    ${NowLocation}
    Wait Until Page Contains Element     ${NAM_DES}
    Press Keys          ${NAM_DES}       CTRL+a\ue017
    Input Text          ${NAM_DES}       ${NAMEDES}
    Press Keys          ${PHON_DES}       CTRL+a\ue017
    Input Text          ${PHON_DES}      ${PHONEDES}
    Click Button                       ยืนยัน


Go to Step 2
#    Wait Until Page Contains           ราคานี้อาจมีการเปลี่ยนแปลงตามการวิ่งงานจริง เช่น กรณีแก้ไข/เพิ่ม/ลด สถานที่ มีค่าที่จอดรถ ค่ารอ ค่าพัสดุน้ำหนักเกิน
    Click Element                       ${NEXT_TO_STEP2}


Select Service
    Click Element              ${SELECT_SERVICE}


Select Option
    Wait Until Page Does Not Contain Element                ${P_LOADER}
    ${PRICE_OPTION}=            Query  ${query_price_option}
    set test variable           ${PRICE_OPTION}             ${PRICE_OPTION[0][0]}
    ${PRICE_OPTION}=            Evaluate                    float(${PRICE_OPTION})
    Set Global variable                                     ${PRICE_OPTION}
    Log To Console              \nPrice Option ${PRICE_OPTION} bath
    Click Element               ${SELECT_OPTION}


Calculate Distance
    Wait Until Page Does Not Contain Element                ${P_LOADER}
    ${TOTALDISTANCE}            Get Text        ${TOTALDISTANCE}
    Set Global variable                         ${TOTALDISTANCE}
    Log To Console              \nTotal Distance ${TOTALDISTANCE} km


Calculate Price
    Wait Until Page Does Not Contain Element                ${P_LOADER}
    ${PRICE_USERGROUP}=         Query  ${query_price.format(distance = ${TOTALDISTANCE})}
    set test variable           ${PRICE_USERGROUP}          ${PRICE_USERGROUP[0][0]}
    Run Keyword If              ${PRICE_OPTION} != 0.5    Option Not Return Trip
    Run Keyword If              ${PRICE_OPTION} == 0.5    Option Return Trip


Option Not Return Trip
    ${PRICE_USERGROUP}=         Evaluate                    float(${PRICE_USERGROUP})
    ${PRICE_ADD9POINT}=         Query  ${query_PRICE_ADD9POINT}
    set test variable           ${PRICE_ADD9POINT}          ${PRICE_ADD9POINT[0][0]}
    ${PRICE_ADD9POINT}=         Evaluate                    float(${PRICE_ADD9POINT}*1)
    ${NETPRICE}=                Evaluate                    float(${PRICE_USERGROUP}+${PRICE_OPTION}+${PRICE_ADD9POINT})
    ${NETPRICE}=                Evaluate                    math.ceil(${NETPRICE})      math
    ${NETPRICE}=                Evaluate                    int(${NETPRICE})
    ${PRICE_USERGROUP}          Convert To String           ${PRICE_USERGROUP}
    ${PRICE_ADD9POINT}          Convert To String           ${PRICE_ADD9POINT}
    ${NETPRICE}                 Convert To String           ${NETPRICE}
    Log To Console              \nPrice Option Not *0.5
    Log To Console              Price ${PRICE_USERGROUP} bath
    Log To Console              Add 9 Point ${PRICE_ADD9POINT} bath
    Log To Console              Net Price must be ${NETPRICE} bath
    Wait Until Page Contains Element                        ${CHK_PRICE}
    Element Text Should Be      ${CHK_PRICE}                ${NETPRICE}


Option Return Trip
    ${PRICE_USERGROUP}=         Evaluate                    float(${PRICE_USERGROUP})
    ${PRICE_ADD9POINT}=         Query  ${query_PRICE_ADD9POINT}
    set test variable           ${PRICE_ADD9POINT}          ${PRICE_ADD9POINT[0][0]}
    ${PRICE_ADD9POINT}=         Evaluate                    float(${PRICE_ADD9POINT}*9)
    ${NETPRICE}=                Evaluate                    float(${PRICE_USERGROUP}+(${PRICE_USERGROUP}*${PRICE_OPTION})+${PRICE_ADD9POINT})
    ${NETPRICE}=                Evaluate                    math.ceil(${NETPRICE})      math
    ${NETPRICE}=                Evaluate                    int(${NETPRICE})
    ${PRICE_USERGROUP}          Convert To String           ${PRICE_USERGROUP}
    ${PRICE_ADD9POINT}          Convert To String           ${PRICE_ADD9POINT}
    ${NETPRICE}                 Convert To String           ${NETPRICE}
    Log To Console              \nPrice Option *0.5
    Log To Console              Price ${PRICE_USERGROUP} bath
    Log To Console              Add 9 Point ${PRICE_ADD9POINT} bath
    Log To Console              Net Price must be ${NETPRICE} bath
    Wait Until Page Contains Element                        ${CHK_PRICE}
    Element Text Should Be      ${CHK_PRICE}                ${NETPRICE}


Go to Step 3
    Sleep   3s
    Click Button                      ขั้นตอนถัดไป


Check Point Pay
    Page Should Contain                เลือกวิธีชำระเงิน


End Case Testcase 73
    #Take Screenshot                     ${path_test_report}TC_${INDEX_TESTCASE7}.jpg
    Close Browser


#===========================================================================================================================

Test Teardown Testcase 73
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE73}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE73}
    Disconnect From Database


Save Test Result Pass Testcase 73
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE73}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE73}.html
    ${INDEX_TESTCASE73}=             Evaluate            int(${INDEX_TESTCASE73}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE73}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

Save Test Result Fail Testcase 73
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE73}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE73}.html
    ${INDEX_TESTCASE73}=             Evaluate            int(${INDEX_TESTCASE73}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE73}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#===========================================================================================================================