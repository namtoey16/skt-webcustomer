﻿*** Keywords ***

Connect DB
    connect to database  pymysql    ${DB_name}  ${DB_user}  ${DB_pass}  ${DB_host}  ${DB_port}
    Execute SQL String   ${set_phone_nouse1}
    Execute SQL String   ${set_phone_nouse2}
    Execute SQL String   ${set_phone_nouse3}


Account Customer
    Open Excel Document     ${path_test_report}customer.xlsx     doc_id=doc1
    ${username}=  Read Excel Cell   row_num=2       col_num=1
    ${password}=  Read Excel Cell   row_num=2       col_num=2
    Log To Console                                  \n${user_name}
    Log To Console                                  ${password}
    Save Excel Document                             ${path_test_report}customer.xlsx
    Close All Excel Documents


Clear Referee DB
    Execute SQL String   ${clear_referee}
    Execute SQL String   ${clear_credit_referer}


Open Browser for Run Test
    Open Browser       ${URL}       ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed   0.3
    Sleep    2s

Login with Email
    Click Element                   ${LOG_IN}
    Wait Until Page Contains Element        ${MAIL}
    Wait Until Page Contains Element        ${PAS_WORD}
    Input Text                              ${MAIL}            ${EMAIL_LOGINREFEREE}
    Input Text                              ${PAS_WORD}        ${PASSWORD_LOGINREFEREE}
    Click Element                           ${LOGIN_EMAIL}


New User
    Wait Until Page Contains           ยินดีต้อนรับลูกค้าใหม่
    Click Button                       ยืนยัน


Alert Promotion
    Wait Until Page Contains         งาน Thailand E-Commerce รับส่วนลด 8% ไม่จำกัดจำนวนครั้ง
    Click Button                     ${Copycode}
    Wait Until Page Contains         คัดลอกโค้ดสำเร็จแล้ว
#   Copy To Clipboard     ${TextToCopy}

Login Success
    Wait Until Page Does Not Contain Element          ${P_LOADER}
    Wait Until Page Contains Element                  ${LOGO}
    Page Should Contain Element                       ${Icon1}
    Page Should Contain Element                       ${Icon2}
    Page Should Contain Element                       ${Icon3}
    Page Should Contain Element                       ${Icon4}
    Page Should Contain Element                       ${Icon5}
    Page Should Contain Element                       ${Icon6}
    Page Should Contain Element                       ${Icon7}
    Page Should Contain Element                       ${Icon8}
    Page Should Contain Element                       ${Icon9}
    Page Should Contain Element                       ${Icon10}


Refer Friend
    Click Element                                     ${TAB_REFER_FRIEND}
    Wait Until Page Contains                          ชวนเพื่อนรับเลย 100 บาท


Copy Link For Share
    Click Element                                     ${COPY_LINK}
    Wait Until Page Contains                          คัดลอกลิงค์เรียบร้อยแล้ว


Open Link Refer
    ${query_refer_code}=      Query    select refer_code from tbl_user where user_name= "test@mail.com";
    set test variable                                 ${query_refer_code}         ${query_refer_code[0][0]}
    Log To Console                                    ${\n} Refer Code is ${query_refer_code}
    Go To                                             ${URL}/signupRefer/${query_refer_code}
    Wait Until Page Contains Element                  ${Signup_Refer}
    #Click Element                   ${input_prepaid_code}
    #Input Text                      ${input_prepaid_code}        ${query_prepaid_code}


Signup Refer
    Click Element                                      ${Signup_Refer}
    Page Should Contain Element                        ${Signup_FbEmail}


Input Form Signup
    Input Text                              ${MAIL}                 ${EMAIL_REFEREE}
    Input Text                              ${PAS_WORD}             ${PASSWORD_REFEREE}
    Input Text                              ${Signup_Mobile}        ${MOBILE}


Click Signup
    Click Element                                     ${Signup}


Check Free Credit Referer Before
    Check If Exists In Database                     ${CREDIT_REFER_BEFORE}
    ${query_free_credit_before}=      Query    select free_credit from tbl_user where user_name= "test@mail.com";
    set test variable                               ${query_free_credit_before}         ${query_free_credit_before[0][0]}
    ${free_credit_before}                     Convert To String                   ${query_free_credit_before}
    Should be equal                                 ${free_credit_before}          0
    Log To Console                                  ${\n} Before Refer Must be 0 >> Free Credit = ${free_credit_before}


Input OTP
    Wait Until Page Contains Element                  ${input_otp}
#    Execute SQL String   ${query_user_otp}
    ${query_user_otp}=      Query    SELECT verify_code FROM tbl_user WHERE phone = '0952483969';
    set test variable                                 ${query_user_otp}                   ${query_user_otp[0][0]}
    Input Text                                        ${input_otp}                        ${query_user_otp}


Setup Profile
    Wait Until Page Contains Element                                ${F_NAME}
    Wait Until Page Contains Element                                ${L_NAME}
    Input Text                          ${F_NAME}                   ${FIRST_NAME}
    Input Text                          ${L_NAME}                   ${LAST_NAME}
    Click Element                                                   ${SETUP_PROFILE}


Check Free Credit Referee
    Sleep                           3s
#Alert Promotion
    Wait Until Page Does Not Contain Element        ${P_LOADER}
    Wait Until Page Contains                        งาน Thailand E-Commerce รับส่วนลด 8% ไม่จำกัดจำนวนครั้ง
    Click Button                                    ${Copycode}
    Wait Until Page Contains                        คัดลอกโค้ดสำเร็จแล้ว
#Check Free Credit
    Check If Exists In Database                     ${CREDIT_REFEREE}
    ${query_free_credit_referee}=      Query    select free_credit from tbl_user where last_name = "referee" and free_credit = "100";
    set test variable                               ${query_free_credit_referee}         ${query_free_credit_referee[0][0]}
    Log To Console                                  ${\n} Referee >> Free Credit = ${query_free_credit_referee}


Order Job
#Login Success
    Wait Until Page Does Not Contain Element          ${P_LOADER}
    Wait Until Page Contains Element                  ${LOGO}
    Page Should Contain Element                       ${Icon1}
    Page Should Contain Element                       ${Icon2}
    Page Should Contain Element                       ${Icon3}
    Page Should Contain Element                       ${Icon4}
    Page Should Contain Element                       ${Icon5}
    Page Should Contain Element                       ${Icon6}
    Page Should Contain Element                       ${Icon7}
    Page Should Contain Element                       ${Icon8}
    Page Should Contain Element                       ${Icon9}
    Page Should Contain Element                       ${Icon10}
#Profile Origin
    Click Element                    ${Add1stplace}
    Click Element                    ${NowLocation}
    Sleep                            3s
    Wait Until Page Contains Element     ${NAM_ORG}
    Press Keys          ${NAM_ORG}       CTRL+a\ue017
    Input Text          ${NAM_ORG}       ${NAMEORG}
    Press Keys          ${PHON_ORG}       CTRL+a\ue017
    Input Text          ${PHON_ORG}      ${PHONEORG}
    Click Button                       ยืนยัน
#Profile Destination 1
    Click Element                    ${Add2ndplace}
    Click Element                    ${NowLocation}
    Wait Until Page Contains Element     ${NAM_DES}
    Press Keys          ${NAM_DES}       CTRL+a\ue017
    Input Text          ${NAM_DES}       ${NAMEDES}
    Press Keys          ${PHON_DES}       CTRL+a\ue017
    Input Text          ${PHON_DES}      ${PHONEDES}
    Click Button                       ยืนยัน
#Go to Step 2
#    Wait Until Page Contains           ราคานี้อาจมีการเปลี่ยนแปลงตามการวิ่งงานจริง เช่น กรณีแก้ไข/เพิ่ม/ลด สถานที่ มีค่าที่จอดรถ ค่ารอ ค่าพัสดุน้ำหนักเกิน
    Click Element                       ${NEXT_TO_STEP2}
#Select Service
    Wait Until Page Does Not Contain Element                ${P_LOADER}
    Click Element                                           ${SELECT_SERVICE}
#Select Option
#    Wait Until Page Does Not Contain Element                ${P_LOADER}
#    ${PRICE_OPTION}=            Query  ${query_price_option}
#    set test variable           ${PRICE_OPTION}             ${PRICE_OPTION[0][0]}
#    ${PRICE_OPTION}=            Evaluate                    float(${PRICE_OPTION})
#    Set Global variable                                     ${PRICE_OPTION}
#    Log To Console              \nPrice Option ${PRICE_OPTION} bath
#    Click Element               ${SELECT_OPTION}
#Calculate Distance
    Wait Until Page Does Not Contain Element                ${P_LOADER}
    ${TOTALDISTANCE}            Get Text        ${TOTALDISTANCE}
    Set Global variable                         ${TOTALDISTANCE}
    Log To Console              \nTotal Distance ${TOTALDISTANCE} km
#Calculate Price
    Wait Until Page Does Not Contain Element                ${P_LOADER}
    ${PRICE_USERGROUP}=         Query  ${query_price.format(distance = ${TOTALDISTANCE})}
    set test variable           ${PRICE_USERGROUP}          ${PRICE_USERGROUP[0][0]}
    ${PRICE_USERGROUP}=         Evaluate                    float(${PRICE_USERGROUP})
    ${NETPRICE}=                Evaluate                    float(${PRICE_USERGROUP})
    ${NETPRICE}=                Evaluate                    math.ceil(${NETPRICE})      math
    ${NETPRICE}=                Evaluate                    int(${NETPRICE})
    ${PRICE_USERGROUP}          Convert To String           ${PRICE_USERGROUP}
    ${NETPRICE}                 Convert To String           ${NETPRICE}
    Log To Console              \nNet Price must be ${NETPRICE} bath
    Wait Until Page Contains Element                        ${CHK_PRICE}
    Element Text Should Be      ${CHK_PRICE}                ${NETPRICE}
#Go to Step 3
    Sleep   3s
    Click Button                      ขั้นตอนถัดไป
#Check Point Pay
    Page Should Contain                เลือกวิธีชำระเงิน
    Click Element                        ${PAYMENT}
    Select Checkbox                      ${CASH}
    Click Button                         บันทึก
    Wait Until Page Contains Element     ${REM}
    Input Text                           ${REM}         ${REMARK}
    Click Element                        ${TIME_CF}
    Click Element                        ${PAY_POINT}
    Wait Until Page Contains Element     ${CREATE_JOB}
    Click Element                        ${CREATE_JOB}
    Wait Until Page Contains             ยืนยันการสั่งงาน
    Click Element                        ${CHK_NEEDRECEIPT}
    Click Element                        ${CONFIRM}


Check Free Credit Referer After
    Wait Until Page Contains                        สั่งงานสำเร็จแล้ว
    Check If Exists In Database                     ${CREDIT_REFER_AFTER}
    ${query_free_credit_after}=      Query    select free_credit from tbl_user where user_name= "test@mail.com";
    set test variable                               ${query_free_credit_after}          ${query_free_credit_after[0][0]}
    ${free_credit_after}                            Convert To String                   ${query_free_credit_after}
    Should be equal                                 ${free_credit_after}                100
    Log To Console                                  ${\n} After Refer Must be 0 >> Free Credit = ${free_credit_after}


End Case Testcase 64
    #Take Screenshot                     ${path_test_report}TC_${INDEX_TESTCASE64}.jpg
    Close Browser


End Case Testcase 65
    #Take Screenshot                     ${path_test_report}TC_${INDEX_TESTCASE65}.jpg
    Close Browser


#===========================================================================================================================

Test Teardown Testcase 64
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE64}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE64}
    Disconnect From Database


Save Test Result Pass Testcase 64
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE64}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE64}.html
    ${INDEX_TESTCASE64}=             Evaluate            int(${INDEX_TESTCASE64}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE64}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

Save Test Result Fail Testcase 64
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE64}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE64}.html
    ${INDEX_TESTCASE64}=             Evaluate            int(${INDEX_TESTCASE64}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE64}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#===========================================================================================================================

Test Teardown Testcase 65
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE65}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE65}
    Disconnect From Database


Save Test Result Pass Testcase 65
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE65}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE65}.html
    ${INDEX_TESTCASE65}=             Evaluate            int(${INDEX_TESTCASE65}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE65}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

Save Test Result Fail Testcase 65
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE65}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE65}.html
    ${INDEX_TESTCASE65}=             Evaluate            int(${INDEX_TESTCASE65}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE65}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#===========================================================================================================================

