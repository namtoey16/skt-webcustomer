﻿*** Keywords ***

Connect DB
    connect to database  pymysql    ${DB_name}  ${DB_user}  ${DB_pass}  ${DB_host}  ${DB_port}
    Execute SQL String   ${query_set_usergroup}


Account Customer
    Open Excel Document     ${path_test_report}customer.xlsx     doc_id=doc1
    ${username}=  Read Excel Cell   row_num=2       col_num=1
    ${password}=  Read Excel Cell   row_num=2       col_num=2
    Log To Console                                  \n${user_name}
    Log To Console                                  ${password}
    Save Excel Document                             ${path_test_report}customer.xlsx
    Close All Excel Documents


Open Browser for Run Test
    Open Browser       ${URL}       ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed   0.3
    Sleep    2s

Login with Email
    Click Element                   ${LOG_IN}
    Wait Until Page Contains Element        ${MAIL}
    Wait Until Page Contains Element        ${PAS_WORD}
    Input Text                              ${MAIL}           ${EMAIL}
    Input Text                              ${PAS_WORD}        ${PASSWORD}
    Click Element                           ${LOGIN_EMAIL}


New User
    Wait Until Page Contains           ยินดีต้อนรับลูกค้าใหม่
    Click Button                       ยืนยัน


Alert Promotion
    Wait Until Page Contains         งาน Thailand E-Commerce รับส่วนลด 8% ไม่จำกัดจำนวนครั้ง
    Click Button                     ${Copycode}
    Wait Until Page Contains         คัดลอกโค้ดสำเร็จแล้ว


Login Success
    Wait Until Page Does Not Contain Element          ${P_LOADER}
    Wait Until Page Contains Element                  ${LOGO}
    Page Should Contain Element                       ${Icon1}
    Page Should Contain Element                       ${Icon2}
    Page Should Contain Element                       ${Icon3}
    Page Should Contain Element                       ${Icon4}
    Page Should Contain Element                       ${Icon5}
    Page Should Contain Element                       ${Icon6}
    Page Should Contain Element                       ${Icon7}
    Page Should Contain Element                       ${Icon8}
    Page Should Contain Element                       ${Icon9}
    Page Should Contain Element                       ${Icon10}


Profile Origin
    Click Element                    ${Add1stplace}
    Click Element                    ${NowLocation}
    Wait Until Page Contains Element     ${NAM_ORG}
    Press Keys          ${NAM_ORG}       CTRL+a\ue017
    Input Text          ${NAM_ORG}       ${NAMEORG}
    Press Keys          ${PHON_ORG}       CTRL+a\ue017
    Input Text          ${PHON_ORG}      ${PHONEORG}
    Click Button                       ยืนยัน


Profile Destination 1
    Click Element                    ${Add2ndplace}
    Click Element                    ${NowLocation}
    Wait Until Page Contains Element     ${NAM_DES}
    Press Keys          ${NAM_DES}       CTRL+a\ue017
    Input Text          ${NAM_DES}       ${NAMEDES}
    Press Keys          ${PHON_DES}       CTRL+a\ue017
    Input Text          ${PHON_DES}      ${PHONEDES}
    Click Button                       ยืนยัน


Go to Step 2
#    Wait Until Page Contains           ราคานี้อาจมีการเปลี่ยนแปลงตามการวิ่งงานจริง เช่น กรณีแก้ไข/เพิ่ม/ลด สถานที่ มีค่าที่จอดรถ ค่ารอ ค่าพัสดุน้ำหนักเกิน
    Click Element                       ${NEXT_TO_STEP2}


Select Service
    Wait Until Page Does Not Contain Element                ${P_LOADER}
    Click Element                                           ${SELECT_SERVICE}


Select Option
    Wait Until Page Does Not Contain Element                ${P_LOADER}
    ${PRICE_OPTION}=            Query  ${query_price_option}
    set test variable           ${PRICE_OPTION}             ${PRICE_OPTION[0][0]}
    ${PRICE_OPTION}=            Evaluate                    float(${PRICE_OPTION})
    Set Global variable                                     ${PRICE_OPTION}
    Log To Console              \nPrice Option ${PRICE_OPTION} bath
    Click Element               ${SELECT_OPTION}


Calculate Distance
    Wait Until Page Does Not Contain Element                ${P_LOADER}
    ${TOTALDISTANCE}            Get Text        ${TOTALDISTANCE}
    Set Global variable                         ${TOTALDISTANCE}
    Log To Console              \nTotal Distance ${TOTALDISTANCE} km


Calculate Price
    Wait Until Page Does Not Contain Element                ${P_LOADER}
    ${PRICE_USERGROUP}=         Query  ${query_price.format(distance = ${TOTALDISTANCE})}
    set test variable           ${PRICE_USERGROUP}          ${PRICE_USERGROUP[0][0]}
    Run Keyword If              ${PRICE_OPTION} != 0.5    Option Not Return Trip
    Run Keyword If              ${PRICE_OPTION} == 0.5    Option Return Trip


Option Not Return Trip
        ${PRICE_USERGROUP}=         Evaluate                    float(${PRICE_USERGROUP})
        ${NETPRICE}=                Evaluate                    float(${PRICE_USERGROUP}+${PRICE_OPTION})
        ${NETPRICE}=                Evaluate                    math.ceil(${NETPRICE})      math
        ${NETPRICE}=                Evaluate                    int(${NETPRICE})
        ${PRICE_USERGROUP}          Convert To String           ${PRICE_USERGROUP}
        ${NETPRICE}                 Convert To String           ${NETPRICE}
        Log To Console              \nPrice Option Not *0.5
        Log To Console              Price ${PRICE_USERGROUP} bath
        Log To Console              Net Price must be ${NETPRICE} bath
        Wait Until Page Contains Element                        ${CHK_PRICE}
        Element Text Should Be      ${CHK_PRICE}                ${NETPRICE}


Option Return Trip
        ${PRICE_USERGROUP}=         Evaluate                    float(${PRICE_USERGROUP})
        ${NETPRICE}=                Evaluate                    float(${PRICE_USERGROUP}+(${PRICE_USERGROUP}*${PRICE_OPTION}))
        ${NETPRICE}=                Evaluate                    math.ceil(${NETPRICE})      math
        ${NETPRICE}=                Evaluate                    int(${NETPRICE})
        ${PRICE_USERGROUP}          Convert To String           ${PRICE_USERGROUP}
        ${NETPRICE}                 Convert To String           ${NETPRICE}
        Log To Console              \nPrice Option *0.5
        Log To Console              Price ${PRICE_USERGROUP} bath
        Log To Console              Net Price must be ${NETPRICE} bath
        Wait Until Page Contains Element                        ${CHK_PRICE}
        Element Text Should Be      ${CHK_PRICE}                ${NETPRICE}


Calculate Price No Option
    Wait Until Page Does Not Contain Element                ${P_LOADER}
    ${PRICE_USERGROUP}=         Query  ${query_price.format(distance = ${TOTALDISTANCE})}
    set test variable           ${PRICE_USERGROUP}          ${PRICE_USERGROUP[0][0]}
    ${NETPRICE}=                Evaluate                    float(${PRICE_USERGROUP})
    ${NETPRICE}=                Evaluate                    math.ceil(${NETPRICE})      math
    ${NETPRICE}=                Evaluate                    int(${NETPRICE})
    ${NETPRICE}                 Convert To String           ${NETPRICE}
    Log To Console              \nNet Price must be ${NETPRICE} bath
    Wait Until Page Contains Element                        ${CHK_PRICE}
    Element Text Should Be      ${CHK_PRICE}                ${NETPRICE}


Go to Step 3
    Sleep   3s
    Click Button                      ขั้นตอนถัดไป


Check Point Pay
    Page Should Contain                เลือกวิธีชำระเงิน



End Case Testcase 18
    #Take Screenshot                     ${path_test_report}TC_${INDEX_TESTCASE18}.jpg
    Close Browser


End Case Testcase 19
    #Take Screenshot                     ${path_test_report}TC_${INDEX_TESTCASE19}.jpg
    Close Browser


End Case Testcase 20
    #Take Screenshot                     ${path_test_report}TC_${INDEX_TESTCASE20}.jpg
    Close Browser


End Case Testcase 21
    #Take Screenshot                     ${path_test_report}TC_${INDEX_TESTCASE21}.jpg
    Close Browser


End Case Testcase 22
    #Take Screenshot                     ${path_test_report}TC_${INDEX_TESTCASE22}.jpg
    Close Browser


End Case Testcase 23
    #Take Screenshot                     ${path_test_report}TC_${INDEX_TESTCASE23}.jpg
    Close Browser


End Case Testcase 24
    #Take Screenshot                     ${path_test_report}TC_${INDEX_TESTCASE24}.jpg
    Close Browser

#===========================================================================================================================

Test Teardown Testcase 18
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE18}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE18}
    Disconnect From Database


Save Test Result Pass Testcase 18
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE18}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE18}.html
    ${INDEX_TESTCASE18}=             Evaluate            int(${INDEX_TESTCASE18}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE18}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents


Save Test Result Fail Testcase 18
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE18}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE18}.html
    ${INDEX_TESTCASE18}=             Evaluate            int(${INDEX_TESTCASE18}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE18}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#===========================================================================================================================

Test Teardown Testcase 19
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE19}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE19}
    Disconnect From Database


Save Test Result Pass Testcase 19
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE19}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE19}.html
    ${INDEX_TESTCASE19}=             Evaluate            int(${INDEX_TESTCASE19}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE19}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents


Save Test Result Fail Testcase 19
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE19}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE19}.html
    ${INDEX_TESTCASE19}=             Evaluate            int(${INDEX_TESTCASE19}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE19}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#===========================================================================================================================

Test Teardown Testcase 20
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE20}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE20}
    Disconnect From Database


Save Test Result Pass Testcase 20
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE20}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE20}.html
    ${INDEX_TESTCASE20}=             Evaluate            int(${INDEX_TESTCASE20}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE20}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents


Save Test Result Fail Testcase 20
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE20}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE20}.html
    ${INDEX_TESTCASE20}=             Evaluate            int(${INDEX_TESTCASE20}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE20}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#===========================================================================================================================

Test Teardown Testcase 21
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE21}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE21}
    Disconnect From Database


Save Test Result Pass Testcase 21
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE21}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE21}.html
    ${INDEX_TESTCASE21}=             Evaluate            int(${INDEX_TESTCASE21}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE21}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents


Save Test Result Fail Testcase 21
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE21}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE21}.html
    ${INDEX_TESTCASE21}=             Evaluate            int(${INDEX_TESTCASE21}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE21}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#===========================================================================================================================

Test Teardown Testcase 22
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE22}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE22}
    Disconnect From Database


Save Test Result Pass Testcase 22
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE22}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE22}.html
    ${INDEX_TESTCASE22}=             Evaluate            int(${INDEX_TESTCASE22}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE22}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents


Save Test Result Fail Testcase 22
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE22}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE22}.html
    ${INDEX_TESTCASE22}=             Evaluate            int(${INDEX_TESTCASE22}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE22}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#===========================================================================================================================

Test Teardown Testcase 23
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE23}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE23}
    Disconnect From Database


Save Test Result Pass Testcase 23
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE23}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE23}.html
    ${INDEX_TESTCASE23}=             Evaluate            int(${INDEX_TESTCASE23}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE23}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents


Save Test Result Fail Testcase 23
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE23}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE23}.html
    ${INDEX_TESTCASE23}=             Evaluate            int(${INDEX_TESTCASE23}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE23}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#===========================================================================================================================

Test Teardown Testcase 24
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE24}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE24}
    Disconnect From Database


Save Test Result Pass Testcase 24
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE24}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE24}.html
    ${INDEX_TESTCASE24}=             Evaluate            int(${INDEX_TESTCASE24}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE24}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents


Save Test Result Fail Testcase 24
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE24}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE24}.html
    ${INDEX_TESTCASE24}=             Evaluate            int(${INDEX_TESTCASE24}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE24}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#===========================================================================================================================
