﻿*** Keywords ***

Connect DB
    connect to database  pymysql    ${DB_name}  ${DB_user}  ${DB_pass}  ${DB_host}  ${DB_port}
    Execute SQL String   ${query_set_usergroup}


Set Headless
    ${chrome options} =     Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome options}   add_argument    headless
    Call Method    ${chrome options}   add_argument    disable-gpu
    Create Webdriver    Chrome    chrome_options=${chrome options}
    Go To    https://uat.skootar.com
    Set Window Size    1920    1080     #Set for Headless


Open Browser for Run Test
    Open Browser       ${URL}       ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed   0.3
    Sleep    2s


Go to Check Price
    Wait Until Page Contains        ระบุเส้นทาง
    Click Element                   ${CheckPrice}


Alert Promotion
    Wait Until Page Contains         งาน Thailand E-Commerce รับส่วนลด 8% ไม่จำกัดจำนวนครั้ง
    Click Button                     ${Copycode}
    Wait Until Page Contains         คัดลอกโค้ดสำเร็จแล้ว


No Login Success
    Wait Until Page Does Not Contain Element          ${P_LOADER}
    Wait Until Page Contains Element                  ${LOGO_NOLOGIN}
    Page Should Contain Element                       ${Icon1}
    Page Should Contain Element                       ${Icon2}
    Page Should Contain Element                       ${Icon3}
    Page Should Contain Element                       ${Icon4}
    Page Should Contain Element                       ${Icon10}


Profile Origin
    Page Should Contain Element                     ${Add1stplace}
    Page Should Contain Element                     ${Add2ndplace}
    Click Element                                   ${Add1stplace}
    Click Element                                   ${NowLocation}
    Wait Until Page Contains Element                ${NAM_ORG}
    Press Keys          ${NAM_ORG}                  CTRL+a\ue017
    Input Text          ${NAM_ORG}                  ${NAMEORG}
    Press Keys          ${PHON_ORG}                 CTRL+a\ue017
    Input Text          ${PHON_ORG}                 ${PHONEORG}
    Click Element                                   ${CF_LOCATION}


Profile Destination 1
    Click Element                                   ${Add2ndplace}
    Click Element                                   ${NowLocation}
    Wait Until Page Contains Element                ${NAM_DES}
    Press Keys          ${NAM_DES}                  CTRL+a\ue017
    Input Text          ${NAM_DES}                  ${NAMEDES}
    Press Keys          ${PHON_DES}                 CTRL+a\ue017
    Input Text          ${PHON_DES}                 ${PHONEDES}
    Click Button                                    ${CF_LOCATION}


Go to Step 2
#    Wait Until Page Contains           ราคานี้อาจมีการเปลี่ยนแปลงตามการวิ่งงานจริง เช่น กรณีแก้ไข/เพิ่ม/ลด สถานที่ มีค่าที่จอดรถ ค่ารอ ค่าพัสดุน้ำหนักเกิน
    Click Element                       ${NEXT_TO_STEP2}


Select Service
    Click Element              ${SELECT_SERVICE}


Select Option
    Wait Until Page Does Not Contain Element                ${P_LOADER}
    ${PRICE_OPTION}=            Query  ${query_price_option}
    set test variable           ${PRICE_OPTION}             ${PRICE_OPTION[0][0]}
    ${PRICE_OPTION}=            Evaluate                    float(${PRICE_OPTION})
    Set Global variable                                     ${PRICE_OPTION}
    Log To Console              \nPrice Option ${PRICE_OPTION} bath
    Click Element               ${SELECT_OPTION}


Calculate Distance
    Wait Until Page Does Not Contain Element                ${P_LOADER}
    ${TOTALDISTANCE}            Get Text        ${TOTALDISTANCE}
    Set Global variable                         ${TOTALDISTANCE}
    Log To Console              \nTotal Distance ${TOTALDISTANCE} km


Calculate Price
    Wait Until Page Does Not Contain Element                ${P_LOADER}
    ${PRICE_USERGROUP}=         Query  ${query_price.format(distance = ${TOTALDISTANCE})}
    set test variable           ${PRICE_USERGROUP}          ${PRICE_USERGROUP[0][0]}
    Run Keyword If              ${PRICE_OPTION} != 0.5    Option Not Return Trip
    Run Keyword If              ${PRICE_OPTION} == 0.5    Option Return Trip


Option Not Return Trip
        ${PRICE_USERGROUP}=         Evaluate                    float(${PRICE_USERGROUP})
        ${NETPRICE}=                Evaluate                    float(${PRICE_USERGROUP}+${PRICE_OPTION})
        ${NETPRICE}=                Evaluate                    math.ceil(${NETPRICE})      math
        ${NETPRICE}=                Evaluate                    int(${NETPRICE})
        ${PRICE_USERGROUP}          Convert To String           ${PRICE_USERGROUP}
        ${NETPRICE}                 Convert To String           ${NETPRICE}
        Log To Console              \nPrice Option Not *0.5
        Log To Console              Price ${PRICE_USERGROUP} bath
        Log To Console              Net Price must be ${NETPRICE} bath
        Wait Until Page Contains Element                        ${CHK_PRICE}
        Element Text Should Be      ${CHK_PRICE}                ${NETPRICE}


Option Return Trip
        ${PRICE_USERGROUP}=         Evaluate                    float(${PRICE_USERGROUP})
        ${NETPRICE}=                Evaluate                    float(${PRICE_USERGROUP}+(${PRICE_USERGROUP}*${PRICE_OPTION}))
        ${NETPRICE}=                Evaluate                    math.ceil(${NETPRICE})      math
        ${NETPRICE}=                Evaluate                    int(${NETPRICE})
        ${PRICE_USERGROUP}          Convert To String           ${PRICE_USERGROUP}
        ${NETPRICE}                 Convert To String           ${NETPRICE}
        Log To Console              \nPrice Option *0.5
        Log To Console              Price ${PRICE_USERGROUP} bath
        Log To Console              Net Price must be ${NETPRICE} bath
        Wait Until Page Contains Element                        ${CHK_PRICE}
        Element Text Should Be      ${CHK_PRICE}                ${NETPRICE}


Go to Step 3
    Sleep   3s
    Click Button                      ขั้นตอนถัดไป
    Page Should Contain                สมัครใช้งาน


End Case Testcase 1
    #Take Screenshot                     ${path_test_report}TC_${INDEX_TESTCASE1}.jpg
    Close Browser


#===========================================================================================================================

Test Teardown Testcase 1
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE1}
    Run Keyword If Any Tests Failed            Save Test Result Fail Testcase ${INDEX_TESTCASE1}
    Disconnect From Database


Save Test Result Pass Testcase 1
    Open Excel                      ${path_test_report}template-test-report.xls
    Put String To Cell              test-report     2   ${INDEX_TESTCASE1}   ${TEST_RESULT_PASS}
    Save Excel                      ${path_test_report}test-report.xls
    Move File                       ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE1}.html
    Move File                       ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE1}.html

Save Test Result Fail Testcase 1
    Open Excel                      ${path_test_report}template-test-report.xls
    Put String To Cell              test-report     2   ${INDEX_TESTCASE1}   ${TEST_RESULT_FAIL}
    Save Excel                      ${path_test_report}test-report.xls
    Move File                       ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE1}.html
    Move File                       ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE1}.html

#=======================================================================================================================