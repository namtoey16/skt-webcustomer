﻿*** Keywords ***

Connect DB
    connect to database  pymysql    ${DB_name}  ${DB_user}  ${DB_pass}  ${DB_host}  ${DB_port}
    Execute SQL String   ${query_set_usergroup}


Open Browser for Run Test
    Open Browser       ${URL}       ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed   0.3
    Sleep    2s


Channel Contact Skootar
    Wait Until Page Contains Element        ${CONTACT_TEL}
    Wait Until Page Contains Element        ${CONTACT_LINE}
    Wait Until Page Contains Element        ${CONTACT_FB}


Go to Business Page
    Wait Until Page Contains Element        ${BUSINESS_PAGE}
    Click Element                           ${BUSINESS_PAGE}


Business Page Channel Contact Skootar
    Location Should Be                      https://uat.skootar.com/business
    Wait Until Page Contains Element        ${BUSCONTACT_TEL}
    Wait Until Page Contains Element        ${BUSCONTACT_LINE}
    Scroll Element Into View                ${BUSCONTACT_LINE}


Contact by phone
    Click Element                           ${CONTACT_TEL}


Contact by line
    Click Element                           ${CONTACT_LINE}


Contact by fb
    Click Element                           ${CONTACT_FB}


Business Contact by phone
    Click Element                           ${BUSCONTACT_TEL}


Business Contact by line
    Click Element                           ${BUSCONTACT_LINE}


End Case Testcase 67
    #Take Screenshot                         ${path_test_report}TC_${INDEX_TESTCASE67}.jpg
    Close Browser


#===========================================================================================================================

Test Teardown Testcase 67
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE67}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE67}
    Disconnect From Database


Save Test Result Pass Testcase 67
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE67}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE67}.html
    ${INDEX_TESTCASE67}=             Evaluate            int(${INDEX_TESTCASE67}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE67}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents


Save Test Result Fail Testcase 67
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE67}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE67}.html
    ${INDEX_TESTCASE67}=             Evaluate            int(${INDEX_TESTCASE67}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE67}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#===========================================================================================================================