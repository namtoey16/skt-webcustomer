﻿*** Keywords ***

Connect DB
    connect to database  pymysql    ${DB_name}  ${DB_user}  ${DB_pass}  ${DB_host}  ${DB_port}
    Execute SQL String   ${query_set_usergroup}


Account Customer
    Open Excel Document     ${path_test_report}customer.xlsx     doc_id=doc1
    ${username}=  Read Excel Cell   row_num=2       col_num=1
    ${password}=  Read Excel Cell   row_num=2       col_num=2
    Log To Console                                  \n${user_name}
    Log To Console                                  ${password}
    Save Excel Document                             ${path_test_report}customer.xlsx
    Close All Excel Documents


Open Browser for Run Test
    Open Browser       ${URL}       ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed   0.3
    Sleep    2s


Login with Email
    Click Element                           ${LOG_IN}
    Wait Until Page Contains Element        ${MAIL}
    Wait Until Page Contains Element        ${PAS_WORD}
    Input Text                              ${MAIL}            ${EMAIL}
    Input Text                              ${PAS_WORD}        ${PASSWORD}
    Click Element                           ${LOGIN_EMAIL}


Login Success
    Wait Until Page Does Not Contain Element          ${P_LOADER}
    Wait Until Page Contains Element                  ${LOGO}
    Page Should Contain Element                       ${Icon1}
    Page Should Contain Element                       ${Icon2}
    Page Should Contain Element                       ${Icon3}
    Page Should Contain Element                       ${Icon4}
    Page Should Contain Element                       ${Icon5}
    Page Should Contain Element                       ${Icon6}
    Page Should Contain Element                       ${Icon7}
    Page Should Contain Element                       ${Icon8}
    Page Should Contain Element                       ${Icon9}
    Page Should Contain Element                       ${Icon10}


Go To Payment
    Click Element                                               ${Icon8}
    Wait Until Page Contains Element                            ${GOTO_PAYMENT}
    Click Element                                               ${GOTO_PAYMENT}
    Wait Until Page Contains Element                            ${SELECT_CCARD}
    Click Element                                               ${SELECT_CCARD}


Remove Creditcard
    ${count} =     Get Element Count    ${CC_CARD}
    Run keyword If  ${count} > 0    Log To Console    Credit Card In List > 0
    : FOR   ${count}    IN RANGE    0   ${count}
        \   Scroll Element Into View                    ${SELECT_CCARD}
        \   Click Element                               ${SELECT_CCARD}
        #\   Execute JavaScript  window.scrollTo(0,5000)
        \   Click Element                               ${CC_CARD}
        \   Wait Until Page Contains                    คุณต้องการลบบัตรเครดิตนี้
        \   Click Button                                ยืนยัน


Creditcard Invalid_security_code
    Wait Until Page Contains Element                    ${SELECT_CCARD}
    Click Element                                       ${SELECT_CCARD}
    Scroll Element Into View                            ${Add_Creditcard}
    #Execute JavaScript                                  window.scrollTo(0,5000)
    Wait Until Page Contains Element                    ${Add_Creditcard}
    Click Element                                       ${Add_Creditcard}
    Input Text                                          ${CREDI_NUM}            3530 1111 1111 0001
    Input Text                                          ${CREDI_NAME}           ${CREDIT_NAME}
    Input Text                                          ${CREDI_EXP}            ${CREDIT_EXP}
    Input Text                                          ${CREDI_CVV}            ${CREDIT_CVV}
    Scroll Element Into View                            ${CF_CREDITCARD}
    Click Button                                        ${CF_CREDITCARD}
    Wait Until Page Contains                            ไม่สามารถเพิ่มบัตรได้ โปรดลองอีกครั้งด้วยบัตรอื่น หรือติดต่อผู้ให้บริการบัตรเครดิตของคุณ


Clear Field
    Press Keys                                          ${CREDI_NUM}            CTRL+a\ue017


Creditcard Fail 3DS Card Enrollment
    Input Text                                          ${CREDI_NUM}            4111 1111 1115 0002
#    Input Text                                         ${CREDI_NAME}           ${CREDIT_NAME}
#    Input Text                                         ${CREDI_EXP}            ${CREDIT_EXP}
#    Input Text                                         ${CREDI_CVV}            ${CREDIT_CVV}
    Scroll Element Into View                            ${CF_CREDITCARD}
    Click Button                                        ${CF_CREDITCARD}
    Wait Until Page Contains                            เพิ่ม/เลือก บัตรสำเร็จแล้วค่ะ ลูกค้าอาจจะได้รับ SMS แจ้งการชาร์จเงิน 20 บาทจากธนาคาร ทั้งนี้เป็นเพียงการ Hold เงินเพื่อทดสอบบัตร และจะทำการคืนเงินจำนวนนี้ให้ในภายหลังค่ะ


Creditcard Fail 3DS Card Validation
    Wait Until Page Contains Element                    ${SELECT_CCARD}
    Click Element                                       ${SELECT_CCARD}
    Scroll Element Into View                            ${Add_Creditcard}
    Wait Until Page Contains Element                    ${Add_Creditcard}
    Click Element                                       ${Add_Creditcard}
    Input Text                                          ${CREDI_NUM}            3530 1111 1119 0003
    Input Text                                          ${CREDI_NAME}           ${CREDIT_NAME}
    Input Text                                          ${CREDI_EXP}            ${CREDIT_EXP}
    Input Text                                          ${CREDI_CVV}            ${CREDIT_CVV}
    Scroll Element Into View                            ${CF_CREDITCARD}
    Click Button                                        ${CF_CREDITCARD}
    Wait Until Page Contains                            เพิ่ม/เลือก บัตรสำเร็จแล้วค่ะ ลูกค้าอาจจะได้รับ SMS แจ้งการชาร์จเงิน 20 บาทจากธนาคาร ทั้งนี้เป็นเพียงการ Hold เงินเพื่อทดสอบบัตร และจะทำการคืนเงินจำนวนนี้ให้ในภายหลังค่ะ


Creditcard Insufficient_fund
    Wait Until Page Contains Element                    ${SELECT_CCARD}
    Click Element                                       ${SELECT_CCARD}
    Scroll Element Into View                            ${Add_Creditcard}
    Wait Until Page Contains Element                    ${Add_Creditcard}
    Click Element                                       ${Add_Creditcard}
    Input Text                                          ${CREDI_NUM}            5555 5511 1111 0011
    Input Text                                          ${CREDI_NAME}           ${CREDIT_NAME}
    Input Text                                          ${CREDI_EXP}            ${CREDIT_EXP}
    Input Text                                          ${CREDI_CVV}            ${CREDIT_CVV}
    Scroll Element Into View                            ${CF_CREDITCARD}
    Click Button                                        ${CF_CREDITCARD}
    Wait Until Page Contains                            เพิ่ม/เลือก บัตรสำเร็จแล้วค่ะ ลูกค้าอาจจะได้รับ SMS แจ้งการชาร์จเงิน 20 บาทจากธนาคาร ทั้งนี้เป็นเพียงการ Hold เงินเพื่อทดสอบบัตร และจะทำการคืนเงินจำนวนนี้ให้ในภายหลังค่ะ


Creditcard Stolen_or_lost_card
    Wait Until Page Contains Element                    ${SELECT_CCARD}
    Click Element                                       ${SELECT_CCARD}
    Scroll Element Into View                            ${Add_Creditcard}
    Wait Until Page Contains Element                    ${Add_Creditcard}
    Click Element                                       ${Add_Creditcard}
    Input Text                                          ${CREDI_NUM}            3530 1111 1118 0012
    Input Text                                          ${CREDI_NAME}           ${CREDIT_NAME}
    Input Text                                          ${CREDI_EXP}            ${CREDIT_EXP}
    Input Text                                          ${CREDI_CVV}            ${CREDIT_CVV}
    Scroll Element Into View                            ${CF_CREDITCARD}
    Click Button                                        ${CF_CREDITCARD}
    Wait Until Page Contains               เพิ่ม/เลือก บัตรสำเร็จแล้วค่ะ ลูกค้าอาจจะได้รับ SMS แจ้งการชาร์จเงิน 20 บาทจากธนาคาร ทั้งนี้เป็นเพียงการ Hold เงินเพื่อทดสอบบัตร และจะทำการคืนเงินจำนวนนี้ให้ในภายหลังค่ะ


Creditcard Failed_processing
    Wait Until Page Contains Element                    ${SELECT_CCARD}
    Click Element                                       ${SELECT_CCARD}
    Scroll Element Into View                            ${Add_Creditcard}
    Wait Until Page Contains Element                    ${Add_Creditcard}
    Click Element                                       ${Add_Creditcard}
    Input Text                                          ${CREDI_NUM}            4111 1111 1112 0013
    Input Text                                          ${CREDI_NAME}           ${CREDIT_NAME}
    Input Text                                          ${CREDI_EXP}            ${CREDIT_EXP}
    Input Text                                          ${CREDI_CVV}            ${CREDIT_CVV}
    Scroll Element Into View                            ${CF_CREDITCARD}
    Click Button                                        ${CF_CREDITCARD}
    Wait Until Page Contains               เพิ่ม/เลือก บัตรสำเร็จแล้วค่ะ ลูกค้าอาจจะได้รับ SMS แจ้งการชาร์จเงิน 20 บาทจากธนาคาร ทั้งนี้เป็นเพียงการ Hold เงินเพื่อทดสอบบัตร และจะทำการคืนเงินจำนวนนี้ให้ในภายหลังค่ะ


Creditcard Payment_rejected
    Wait Until Page Contains Element                    ${SELECT_CCARD}
    Click Element                                       ${SELECT_CCARD}
    Scroll Element Into View                            ${Add_Creditcard}
    Wait Until Page Contains Element                    ${Add_Creditcard}
    Click Element                                       ${Add_Creditcard}
    Input Text                                          ${CREDI_NUM}            4111 1111 1111 0014
    Input Text                                          ${CREDI_NAME}           ${CREDIT_NAME}
    Input Text                                          ${CREDI_EXP}            ${CREDIT_EXP}
    Input Text                                          ${CREDI_CVV}            ${CREDIT_CVV}
    Scroll Element Into View                            ${CF_CREDITCARD}
    Click Button                                        ${CF_CREDITCARD}
    Wait Until Page Contains               เพิ่ม/เลือก บัตรสำเร็จแล้วค่ะ ลูกค้าอาจจะได้รับ SMS แจ้งการชาร์จเงิน 20 บาทจากธนาคาร ทั้งนี้เป็นเพียงการ Hold เงินเพื่อทดสอบบัตร และจะทำการคืนเงินจำนวนนี้ให้ในภายหลังค่ะ


Creditcard Failed_fraud_check
    Wait Until Page Contains Element                    ${SELECT_CCARD}
    Click Element                                       ${SELECT_CCARD}
    Scroll Element Into View                            ${Add_Creditcard}
    Wait Until Page Contains Element                    ${Add_Creditcard}
    Click Element                                       ${Add_Creditcard}
    Input Text                                          ${CREDI_NUM}            5555 5511 1116 0016
    Input Text                                          ${CREDI_NAME}           ${CREDIT_NAME}
    Input Text                                          ${CREDI_EXP}            ${CREDIT_EXP}
    Input Text                                          ${CREDI_CVV}            ${CREDIT_CVV}
    Scroll Element Into View                            ${CF_CREDITCARD}
    Click Button                                        ${CF_CREDITCARD}
    Wait Until Page Contains               เพิ่ม/เลือก บัตรสำเร็จแล้วค่ะ ลูกค้าอาจจะได้รับ SMS แจ้งการชาร์จเงิน 20 บาทจากธนาคาร ทั้งนี้เป็นเพียงการ Hold เงินเพื่อทดสอบบัตร และจะทำการคืนเงินจำนวนนี้ให้ในภายหลังค่ะ


Creditcard Invalid_account_number
    Wait Until Page Contains Element                    ${SELECT_CCARD}
    Click Element                                       ${SELECT_CCARD}
    Scroll Element Into View                            ${Add_Creditcard}
    Wait Until Page Contains Element                    ${Add_Creditcard}
    Click Element                                       ${Add_Creditcard}
    Input Text                                          ${CREDI_NUM}            3530 1111 1113 0017
    Input Text                                          ${CREDI_NAME}           ${CREDIT_NAME}
    Input Text                                          ${CREDI_EXP}            ${CREDIT_EXP}
    Input Text                                          ${CREDI_CVV}            ${CREDIT_CVV}
    Scroll Element Into View                            ${CF_CREDITCARD}
    Click Button                                        ${CF_CREDITCARD}
    Wait Until Page Contains               เพิ่ม/เลือก บัตรสำเร็จแล้วค่ะ ลูกค้าอาจจะได้รับ SMS แจ้งการชาร์จเงิน 20 บาทจากธนาคาร ทั้งนี้เป็นเพียงการ Hold เงินเพื่อทดสอบบัตร และจะทำการคืนเงินจำนวนนี้ให้ในภายหลังค่ะ


Creditcard Success
    Wait Until Page Contains Element                    ${SELECT_CCARD}
    Click Element                                       ${SELECT_CCARD}
    Scroll Element Into View                            ${Add_Creditcard}
    Wait Until Page Contains Element                    ${Add_Creditcard}
    Click Element                                       ${Add_Creditcard}
    Input Text                                          ${CREDI_NUM}            4111 1111 1111 1111
    Input Text                                          ${CREDI_NAME}           ${CREDIT_NAME}
    Input Text                                          ${CREDI_EXP}            ${CREDIT_EXP}
    Input Text                                          ${CREDI_CVV}            ${CREDIT_CVV}
    Scroll Element Into View                            ${CF_CREDITCARD}
    Click Button                                        ${CF_CREDITCARD}
    Wait Until Page Contains               เพิ่ม/เลือก บัตรสำเร็จแล้วค่ะ ลูกค้าอาจจะได้รับ SMS แจ้งการชาร์จเงิน 20 บาทจากธนาคาร ทั้งนี้เป็นเพียงการ Hold เงินเพื่อทดสอบบัตร และจะทำการคืนเงินจำนวนนี้ให้ในภายหลังค่ะ


Profile Origin
    Click Element                       ${Add1stplace}
    Click Element                       ${NowLocation}
    Wait Until Page Contains Element    ${NAM_ORG}
    Press Keys          ${NAM_ORG}      CTRL+a\ue017
    Input Text          ${NAM_ORG}      ${NAMEORG}
    Press Keys          ${PHON_ORG}     CTRL+a\ue017
    Input Text          ${PHON_ORG}     ${PHONEORG}
    Click Button                        ยืนยัน


Profile Destination 1
    Click Element                       ${Add2ndplace}
    Click Element                       ${NowLocation}
    Wait Until Page Contains Element    ${NAM_DES}
    Press Keys          ${NAM_DES}      CTRL+a\ue017
    Input Text          ${NAM_DES}      ${NAMEDES}
    Press Keys          ${PHON_DES}     CTRL+a\ue017
    Input Text          ${PHON_DES}     ${PHONEDES}
    Click Button                        ยืนยัน


End Case Testcase 49
    #Take Screenshot                     ${path_test_report}TC_${INDEX_TESTCASE49}.jpg
    Close Browser


#===========================================================================================================================

Test Teardown Testcase 49
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE49}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE49}
    Disconnect From Database


Save Test Result Pass Testcase 49
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE49}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE49}.html
    ${INDEX_TESTCASE49}=             Evaluate            int(${INDEX_TESTCASE49}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE49}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

Save Test Result Fail Testcase 49
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE49}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE49}.html
    ${INDEX_TESTCASE49}=             Evaluate            int(${INDEX_TESTCASE49}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE49}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#===========================================================================================================================