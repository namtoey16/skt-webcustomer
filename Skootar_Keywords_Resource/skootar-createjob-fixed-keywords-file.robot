﻿*** Keywords ***

Connect DB
    connect to database  pymysql    ${DB_name}  ${DB_user}  ${DB_pass}  ${DB_host}  ${DB_port}
    Execute SQL String   ${query_user_status}
    Execute SQL String   ${query_set_usergroup}
    Execute SQL String   ${query_invoice}


Account Customer
    Open Excel Document     ${path_test_report}customer.xlsx     doc_id=doc1
    ${username}=  Read Excel Cell   row_num=2       col_num=1
    ${password}=  Read Excel Cell   row_num=2       col_num=2
    Log To Console                                  \n${user_name}
    Log To Console                                  ${password}
    Save Excel Document                             ${path_test_report}customer.xlsx
    Close All Excel Documents


Open Browser for Run Test
    Open Browser       ${URL}       ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed   0.3
    Sleep    2s

Login with Email
    Click Element                           ${LOG_IN}
    Wait Until Page Contains Element        ${MAIL}
    Wait Until Page Contains Element        ${PAS_WORD}
    Input Text                              ${MAIL}           ${EMAIL}
    Input Text                              ${PAS_WORD}        ${PASSWORD}
    Click Element                           ${LOGIN_EMAIL}

Login with Email Coperate
    Click Element                           ${LOG_IN}
    Wait Until Page Contains Element        ${MAIL}
    Wait Until Page Contains Element        ${PAS_WORD}
    Input Text                              ${MAIL}           ${EMAIL_COMP}
    Input Text                              ${PAS_WORD}        ${PASSWORD_COMP}
    Click Element                           ${LOGIN_EMAIL}


New User
    Wait Until Page Contains           ยินดีต้อนรับลูกค้าใหม่
    Click Button                       ยืนยัน


Alert Promotion
    Wait Until Page Contains         งาน Thailand E-Commerce รับส่วนลด 8% ไม่จำกัดจำนวนครั้ง
    Click Button                     ${Copycode}
    Wait Until Page Contains         คัดลอกโค้ดสำเร็จแล้ว


Login Success
    Wait Until Page Does Not Contain Element          ${P_LOADER}
    Wait Until Page Contains Element                  ${LOGO}
    Page Should Contain Element                       ${Icon1}
    Page Should Contain Element                       ${Icon2}
    Page Should Contain Element                       ${Icon3}
    Page Should Contain Element                       ${Icon4}
    Page Should Contain Element                       ${Icon5}
    Page Should Contain Element                       ${Icon6}
    Page Should Contain Element                       ${Icon7}
    Page Should Contain Element                       ${Icon8}
    Page Should Contain Element                       ${Icon9}
    Page Should Contain Element                       ${Icon10}


Fixed Fav = True
    Click Element                                     ${Icon4}
    Wait Until Page Contains                          แมสเซ็นเจอร์คนโปรด
    Wait Until Page Contains                          แมสเซ็นเจอร์ในรายชื่อนี้จะได้รับงานของคุณก่อนคนอื่นๆ
    Checkbox Should Not Be Selected                   ${FIXED_FAV}
    Click Element                                     ${FIXED_FAV}
    Wait Until Page Contains                          งานของคุณจะถูกตอบรับช้าลง เมื่อยืนยันให้แมสเซ็นเจอร์กลุ่มนี้รับงานเท่านั้น (แมสเซ็นเจอร์คนอื่นจะไม่เห็นงาน)
    Click Button                                      ยืนยัน
    Checkbox Should Be Selected                       ${FIXED_FAV}
    Click Element                                     ${Icon1}


Fixed Fav = False
    Click Element                                     ${Icon4}
    Wait Until Page Contains                          แมสเซ็นเจอร์คนโปรด
    Wait Until Page Contains                          แมสเซ็นเจอร์ในรายชื่อนี้จะได้รับงานของคุณก่อนคนอื่นๆ
    Checkbox Should Be Selected                       ${FIXED_FAV}
    Click Element                                     ${FIXED_FAV}
    Wait Until Page Contains                          คุณต้องการให้แมสเซ็นเจอร์คนอื่นๆ นอกเหนือจากรายชื่อนี้เห็นงานของคุณด้วยหรือไม่?
    Click Button                                      ยืนยัน
    Checkbox Should Not Be Selected                   ${FIXED_FAV}
    Click Element                                     ${Icon1}


Profile Origin
    Click Element                    ${Add1stplace}
    Click Element                    ${NowLocation}
    Wait Until Page Contains Element     ${NAM_ORG}
    Press Keys          ${NAM_ORG}       CTRL+a\ue017
    Input Text          ${NAM_ORG}       ${NAMEORG}
    Press Keys          ${PHON_ORG}       CTRL+a\ue017
    Input Text          ${PHON_ORG}      ${PHONEORG}
    Click Button                       ยืนยัน


Profile Destination 1
    Click Element                    ${Add2ndplace}
    Click Element                    ${NowLocation}
    Wait Until Page Contains Element     ${NAM_DES}
    Press Keys          ${NAM_DES}       CTRL+a\ue017
    Input Text          ${NAM_DES}       ${NAMEDES}
    Press Keys          ${PHON_DES}       CTRL+a\ue017
    Input Text          ${PHON_DES}      ${PHONEDES}
    Click Button                       ยืนยัน


Go to Step 2
#    Wait Until Page Contains           ราคานี้อาจมีการเปลี่ยนแปลงตามการวิ่งงานจริง เช่น กรณีแก้ไข/เพิ่ม/ลด สถานที่ มีค่าที่จอดรถ ค่ารอ ค่าพัสดุน้ำหนักเกิน
    Click Element                       ${NEXT_TO_STEP2}


Select Service
    Click Element              ${SELECT_SERVICE}


Select Option
    Wait Until Page Does Not Contain Element                ${P_LOADER}
    ${PRICE_OPTION}=            Query  ${query_price_option}
    set test variable           ${PRICE_OPTION}             ${PRICE_OPTION[0][0]}
    ${PRICE_OPTION}=            Evaluate                    float(${PRICE_OPTION})
    Set Global variable                                     ${PRICE_OPTION}
    Log To Console              \nPrice Option ${PRICE_OPTION} bath
    Click Element               ${SELECT_OPTION}


Calculate Distance
    Wait Until Page Does Not Contain Element                ${P_LOADER}
    ${TOTALDISTANCE}            Get Text        ${TOTALDISTANCE}
    Set Global variable                         ${TOTALDISTANCE}
    Log To Console              \nTotal Distance ${TOTALDISTANCE} km


Calculate Price
    Wait Until Page Does Not Contain Element                ${P_LOADER}
    ${PRICE_USERGROUP}=         Query  ${query_price.format(distance = ${TOTALDISTANCE})}
    set test variable           ${PRICE_USERGROUP}          ${PRICE_USERGROUP[0][0]}
    Run Keyword If              ${PRICE_OPTION} != 0.5    Option Not Return Trip
    Run Keyword If              ${PRICE_OPTION} == 0.5    Option Return Trip


Option Not Return Trip
        ${PRICE_USERGROUP}=         Evaluate                    float(${PRICE_USERGROUP})
        ${NETPRICE}=                Evaluate                    float(${PRICE_USERGROUP}+${PRICE_OPTION})
        ${NETPRICE}=                Evaluate                    math.ceil(${NETPRICE})      math
        ${NETPRICE}=                Evaluate                    int(${NETPRICE})
        ${PRICE_USERGROUP}          Convert To String           ${PRICE_USERGROUP}
        ${NETPRICE}                 Convert To String           ${NETPRICE}
        Log To Console              \nPrice Option Not *0.5
        Log To Console              Price ${PRICE_USERGROUP} bath
        Log To Console              Net Price must be ${NETPRICE} bath
        Wait Until Page Contains Element                        ${CHK_PRICE}
        Element Text Should Be      ${CHK_PRICE}                ${NETPRICE}


Option Return Trip
        ${PRICE_USERGROUP}=         Evaluate                    float(${PRICE_USERGROUP})
        ${NETPRICE}=                Evaluate                    float(${PRICE_USERGROUP}+(${PRICE_USERGROUP}*${PRICE_OPTION}))
        ${NETPRICE}=                Evaluate                    math.ceil(${NETPRICE})      math
        ${NETPRICE}=                Evaluate                    int(${NETPRICE})
        ${PRICE_USERGROUP}          Convert To String           ${PRICE_USERGROUP}
        ${NETPRICE}                 Convert To String           ${NETPRICE}
        Log To Console              \nPrice Option *0.5
        Log To Console              Price ${PRICE_USERGROUP} bath
        Log To Console              Net Price must be ${NETPRICE} bath
        Wait Until Page Contains Element                        ${CHK_PRICE}
        Element Text Should Be      ${CHK_PRICE}                ${NETPRICE}


Go to Step 3
    Sleep   3s
    Click Button                      ขั้นตอนถัดไป


Payment Cash
    Page Should Contain                  เลือกวิธีชำระเงิน
    Click Element                        ${PAYMENT}
    Select Checkbox                      ${CASH}
    Click Button                         บันทึก


Payment Creditcard
    Page Should Contain                  เลือกวิธีชำระเงิน
    Click Element                        ${PAYMENT}
    Select Checkbox                      ${CREDIT_CARD}
    Click Button                         บันทึก


Payment Wallet
    Page Should Contain                  เลือกวิธีชำระเงิน
    Click Element                        ${PAYMENT}
    Select Checkbox                      ${WALLET}
    Click Button                         บันทึก


Payment Promptpay
    Page Should Contain                  เลือกวิธีชำระเงิน
    Click Element                        ${PAYMENT}
    Select Checkbox                      ${PROMPTPAY}
    Click Button                         บันทึก


Payment Invoice
    Page Should Contain                  เลือกวิธีชำระเงิน
    Click Element                        ${PAYMENT}
    Select Checkbox                      ${INVOICE}
    Click Button                         บันทึก


Note Job (Fixed In Remark)
    Wait Until Page Contains Element     ${REM}
    Input Text                           ${REM}         ${REMARK}
#    Click Element                        ${TIME_CF}
    Click Element                        ${PAY_POINT}
    Wait Until Page Contains Element     ${CREATE_JOB}
    Click Element                        ${CREATE_JOB}


Note Job (cash,promptpay)
    Wait Until Page Contains Element     ${REM}
#    Input Text                           ${REM}         ${REMARK}
#    Click Element                        ${TIME_CF}
    Click Element                        ${PAY_POINT}
    Wait Until Page Contains Element     ${CREATE_JOB}
    Click Element                        ${CREATE_JOB}


Note Job
    Wait Until Page Contains Element     ${REM}
    Input Text                           ${REM}         ${REMARK}
#    Click Element                        ${TIME_CF}
#    Click Element                       ${PAY_POINT}
    Click Element                        ${CREATE_JOB}


Now Job
    Wait Until Page Contains Element     ${REM}
    Click Element                        ${TIME_NOW_CF}


Schedule Job
    Wait Until Page Contains Element     ${REM}
    Click Element                        ${TIME_SCHEDULE_CF}
    Wait Until Page Contains Element     ${START_TIME}
    Wait Until Page Contains Element     ${END_TIME}
    Click Element                        ${START_TIME}
    Click Element                        //div[@role="presentation"]//div[6]
    Click Element                        ${END_TIME}
    Click Element                        //div[@role="presentation"]//div[6]


Alert Invoice Coperate
    Page Should Contain                 ต้องการออกใบเสร็จในนามบริษัท


Alert Crete Job Success
    Page Should Contain                 ยืนยันการสั่งงาน


End Case Testcase 58
    #Take Screenshot                     ${path_test_report}TC_${INDEX_TESTCASE58}.jpg
    Close Browser


End Case Testcase 59
    #Take Screenshot                     ${path_test_report}TC_${INDEX_TESTCASE59}.jpg
    Close Browser


End Case Testcase 60
    #Take Screenshot                     ${path_test_report}TC_${INDEX_TESTCASE60}.jpg
    Close Browser


End Case Testcase 61
    #Take Screenshot                     ${path_test_report}TC_${INDEX_TESTCASE61}.jpg
    Close Browser


End Case Testcase 62
    #Take Screenshot                     ${path_test_report}TC_${INDEX_TESTCASE62}.jpg
    Close Browser


End Case Testcase 63
    #Take Screenshot                     ${path_test_report}TC_${INDEX_TESTCASE63}.jpg
    Close Browser


#===========================================================================================================================

Test Teardown Testcase 58
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE58}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE58}
    Disconnect From Database


Save Test Result Pass Testcase 58
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE58}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE58}.html
    ${INDEX_TESTCASE58}=             Evaluate            int(${INDEX_TESTCASE58}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE58}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents


Save Test Result Fail Testcase 58
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE58}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE58}.html
    ${INDEX_TESTCASE58}=             Evaluate            int(${INDEX_TESTCASE58}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE58}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#===========================================================================================================================

Test Teardown Testcase 59
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE59}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE59}
    Disconnect From Database


Save Test Result Pass Testcase 59
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE59}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE59}.html
    ${INDEX_TESTCASE59}=             Evaluate            int(${INDEX_TESTCASE59}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE59}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents


Save Test Result Fail Testcase 59
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE59}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE59}.html
    ${INDEX_TESTCASE59}=             Evaluate            int(${INDEX_TESTCASE59}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE59}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#===========================================================================================================================

Test Teardown Testcase 60
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE60}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE60}
    Disconnect From Database


Save Test Result Pass Testcase 60
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE60}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE60}.html
    ${INDEX_TESTCASE60}=             Evaluate            int(${INDEX_TESTCASE60}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE60}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents


Save Test Result Fail Testcase 60
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE60}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE60}.html
    ${INDEX_TESTCASE60}=             Evaluate            int(${INDEX_TESTCASE60}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE60}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#===========================================================================================================================

Test Teardown Testcase 61
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE61}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE61}
    Disconnect From Database


Save Test Result Pass Testcase 61
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE61}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE61}.html
    ${INDEX_TESTCASE61}=             Evaluate            int(${INDEX_TESTCASE61}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE61}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents


Save Test Result Fail Testcase 61
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE61}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE61}.html
    ${INDEX_TESTCASE61}=             Evaluate            int(${INDEX_TESTCASE61}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE61}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#===========================================================================================================================

Test Teardown Testcase 62
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE62}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE62}
    Disconnect From Database


Save Test Result Pass Testcase 62
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE62}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE62}.html
    ${INDEX_TESTCASE62}=             Evaluate            int(${INDEX_TESTCASE62}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE62}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents


Save Test Result Fail Testcase 62
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE62}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE62}.html
    ${INDEX_TESTCASE62}=             Evaluate            int(${INDEX_TESTCASE62}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE62}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#===========================================================================================================================

Test Teardown Testcase 63
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE63}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE63}
    Disconnect From Database


Save Test Result Pass Testcase 63
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE63}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE63}.html
    ${INDEX_TESTCASE63}=             Evaluate            int(${INDEX_TESTCASE63}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE63}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents


Save Test Result Fail Testcase 63
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE63}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE63}.html
    ${INDEX_TESTCASE63}=             Evaluate            int(${INDEX_TESTCASE63}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE63}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#===========================================================================================================================