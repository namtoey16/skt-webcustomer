﻿*** Keywords ***

Connect DB
    connect to database  pymysql    ${DB_name}  ${DB_user}  ${DB_pass}  ${DB_host}  ${DB_port}
#    Execute SQL String   ${query_prepaid_code}

Connect DB TestPromo
    Execute SQL String   ${set_usergroup_promo}

Connect DB Backup
    Execute SQL String   ${set_usergroup_promo2}


Account Customer
    Open Excel Document     ${path_test_report}customer.xlsx     doc_id=doc1
    ${username}=  Read Excel Cell   row_num=2       col_num=1
    ${password}=  Read Excel Cell   row_num=2       col_num=2
    Log To Console                                  \n${user_name}
    Log To Console                                  ${password}
    Save Excel Document                             ${path_test_report}customer.xlsx
    Close All Excel Documents


Open Browser for Run Test
    Open Browser       ${URL}       ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed   0.3
    Sleep    2s


Login with FB
    Click Element                           ${LOG_IN}
    Wait Until Page Contains Element        ${MAIL}
    Wait Until Page Contains Element        ${PAS_WORD}
    Input Text                              ${MAIL}             ${EMAIL_EXTRA}
    Input Text                              ${PAS_WORD}         ${PASSWORD_EXTRA}
    Click Element                           ${LOGIN_EMAIL}


Login with Email Coperate
    Click Element                           ${LOG_IN}
    Wait Until Page Contains Element        ${MAIL}
    Wait Until Page Contains Element        ${PAS_WORD}
    Input Text                              ${MAIL}             ${EMAIL}
    Input Text                              ${PAS_WORD}         ${PASSWORD}
    Click Element                           ${LOGIN_EMAIL}


New User
    Wait Until Page Contains                                    ยินดีต้อนรับลูกค้าใหม่
    Click Button                                                ยืนยัน


Alert Promotion
    Wait Until Page Contains                                    งาน Thailand E-Commerce รับส่วนลด 8% ไม่จำกัดจำนวนครั้ง
    Click Button                                                ${Copycode}
    Wait Until Page Contains                                    คัดลอกโค้ดสำเร็จแล้ว


Go To Payment
    Click Element                                               ${Icon8}
    Wait Until Page Contains Element                            ${GOTO_PAYMENT}
    Click Element                                               ${GOTO_PAYMENT}
    Wait Until Page Contains Element                            ${SELECT_CCARD}
    Click Element                                               ${SELECT_CCARD}


Remove Creditcard
    ${count} =     Get Element Count    ${CC_CARD}
    Run keyword If  ${count} > 0    Log To Console    Credit Card In List > 0
    : FOR   ${count}    IN RANGE    0   ${count}
        \   Scroll Element Into View                    ${SELECT_CCARD}
        \   Click Element                               ${SELECT_CCARD}
        \   Click Element                               ${CC_CARD}
        \   Wait Until Page Contains                    คุณต้องการลบบัตรเครดิตนี้
        \   Click Button                                ยืนยัน


Login Success
    Wait Until Page Does Not Contain Element                    ${P_LOADER}
    Wait Until Page Contains Element                            ${LOGO}
    Page Should Contain Element                                 ${Icon1}
    Page Should Contain Element                                 ${Icon2}
    Page Should Contain Element                                 ${Icon3}
    Page Should Contain Element                                 ${Icon4}
    Page Should Contain Element                                 ${Icon5}
    Page Should Contain Element                                 ${Icon6}
    Page Should Contain Element                                 ${Icon7}
    Page Should Contain Element                                 ${Icon8}
    Page Should Contain Element                                 ${Icon9}
    Page Should Contain Element                                 ${Icon10}


Profile Origin
    Click Element                                               ${Add1stplace}
    Input Text                                  ${LOC_1}        13.741932,100.561634\ue007
#    Wait Until Page Contains Element                            ${CHOOSE_LOC1}
#    Click Element                                               ${CHOOSE_LOC1}
#    Click Element                                               ${NowLocation}
    Wait Until Page Contains Element                            ${NAM_DES}
    Wait Until Page Contains Element                            ${NAM_ORG}
    Press Keys                                  ${NAM_ORG}      CTRL+a\ue017
    Input Text                                  ${NAM_ORG}      ${NAMEORG}
    Press Keys                                  ${PHON_ORG}     CTRL+a\ue017
    Input Text                                  ${PHON_ORG}     ${PHONEORG}
    Click Button                                                ยืนยัน


Profile Destination 1
    Click Element                                               ${Add2ndplace}
    Input Text                                  ${LOC_2}        13.741932,100.561634\ue007
#    Wait Until Page Contains Element                            ${CHOOSE_LOC2}
#    Click Element                                               ${CHOOSE_LOC2}
#   Click Element                                               ${NowLocation}
    Wait Until Page Contains Element                            ${NAM_DES}
    Wait Until Page Contains Element                            ${NAM_ORG}
    Press Keys                                  ${NAM_ORG}      CTRL+a\ue017
    Input Text                                  ${NAM_DES}      ${NAMEDES}
    Press Keys                                  ${PHON_ORG}     CTRL+a\ue017
    Input Text                                  ${PHON_DES}     ${PHONEDES}
    Click Button                                                ยืนยัน


Profile Origin Over Radius
    Click Element                                               ${Add1stplace}
    Input Text                                  ${LOC_1}        โรงพยาบาลจุฬาลงกรณ์\ue007
    Wait Until Page Contains Element                            ${CHOOSE_LOC1}
    Click Element                                               ${CHOOSE_LOC1}
    Wait Until Page Contains Element                            ${NAM_DES}
    Wait Until Page Contains Element                            ${NAM_ORG}
    Press Keys                                  ${NAM_ORG}      CTRL+a\ue017
    Input Text                                  ${NAM_ORG}      ${NAMEORG}
    Press Keys                                  ${PHON_ORG}     CTRL+a\ue017
    Input Text                                  ${PHON_ORG}     ${PHONEORG}
    Click Button                                                ยืนยัน


Profile Destination Over Radius
    Click Element                                               ${Add2ndplace}
    Input Text                                  ${LOC_2}        โรงพยาบาลจุฬาลงกรณ์\ue007
    Wait Until Page Contains Element                            ${CHOOSE_LOC2}
    Click Element                                               ${CHOOSE_LOC2}
    Wait Until Page Contains Element                            ${NAM_DES}
    Wait Until Page Contains Element                            ${NAM_ORG}
    Press Keys                                  ${NAM_DES}      CTRL+a\ue017
    Input Text                                  ${NAM_DES}      ${NAMEDES}
    Press Keys                                  ${PHON_DES}     CTRL+a\ue017
    Input Text                                  ${PHON_DES}     ${PHONEDES}
    Click Button                                                ยืนยัน


Go to Step 2
#    Wait Until Page Contains           ราคานี้อาจมีการเปลี่ยนแปลงตามการวิ่งงานจริง เช่น กรณีแก้ไข/เพิ่ม/ลด สถานที่ มีค่าที่จอดรถ ค่ารอ ค่าพัสดุน้ำหนักเกิน
    Click Element                                               ${NEXT_TO_STEP2}
    Wait Until Page Contains                                    ประเภทบริการหลัก


Go to Step 3
    Sleep   3s
    Click Button                      ขั้นตอนถัดไป


Select Service
    Click Element              ${SELECT_SERVICE}
    Sleep  3s


Select Option
    Wait Until Page Does Not Contain Element                ${P_LOADER}
    ${PRICE_OPTION}=            Query  ${query_price_option}
    set test variable           ${PRICE_OPTION}             ${PRICE_OPTION[0][0]}
    ${PRICE_OPTION}=            Evaluate                    float(${PRICE_OPTION})
    Set Global variable                                     ${PRICE_OPTION}
    Log To Console              \nPrice Option ${PRICE_OPTION} bath
    Click Element               ${SELECT_OPTION}


Calculate Distance
    Wait Until Page Does Not Contain Element                ${P_LOADER}
    ${TOTALDISTANCE}            Get Text        ${TOTALDISTANCE}
    Set Global variable                         ${TOTALDISTANCE}
    Log To Console              \nTotal Distance ${TOTALDISTANCE} km


Calculate Price
    Wait Until Page Does Not Contain Element                ${P_LOADER}
    ${PRICE_USERGROUP}=         Query  ${query_price.format(distance = ${TOTALDISTANCE})}
    set test variable           ${PRICE_USERGROUP}          ${PRICE_USERGROUP[0][0]}
    Run Keyword If              ${PRICE_OPTION} != 0.5    Option Not Return Trip
    Run Keyword If              ${PRICE_OPTION} == 0.5    Option Return Trip


Option Not Return Trip
        ${PRICE_USERGROUP}=         Evaluate                    float(${PRICE_USERGROUP})
        ${NETPRICE}=                Evaluate                    float(${PRICE_USERGROUP}+${PRICE_OPTION})
        ${NETPRICE}=                Evaluate                    math.ceil(${NETPRICE})      math
        ${NETPRICE}=                Evaluate                    int(${NETPRICE})
        ${PRICE_USERGROUP}          Convert To String           ${PRICE_USERGROUP}
        ${NETPRICE}                 Convert To String           ${NETPRICE}
        Log To Console              \nPrice Option Not *0.5
        Log To Console              Price ${PRICE_USERGROUP} bath
        Log To Console              Net Price must be ${NETPRICE} bath
        Wait Until Page Contains Element                        ${CHK_PRICE}
        Element Text Should Be      ${CHK_PRICE}                ${NETPRICE}


Option Return Trip
        ${PRICE_USERGROUP}=         Evaluate                    float(${PRICE_USERGROUP})
        ${NETPRICE}=                Evaluate                    float(${PRICE_USERGROUP}+(${PRICE_USERGROUP}*${PRICE_OPTION}))
        ${NETPRICE}=                Evaluate                    math.ceil(${NETPRICE})      math
        ${NETPRICE}=                Evaluate                    int(${NETPRICE})
        ${PRICE_USERGROUP}          Convert To String           ${PRICE_USERGROUP}
        ${NETPRICE}                 Convert To String           ${NETPRICE}
        Log To Console              \nPrice Option *0.5
        Log To Console              Price ${PRICE_USERGROUP} bath
        Log To Console              Net Price must be ${NETPRICE} bath
        Wait Until Page Contains Element                        ${CHK_PRICE}
        Element Text Should Be      ${CHK_PRICE}                ${NETPRICE}


Calculate Price 2
    ${query_promo_code}=      Query    select promo_code from SKOOTAR_DEV.tbl_promotion where promo_status = 1 and end_date between "2019-11-01 00:00:00" and "2025-10-30 23:59:59" and sub_category = "free_trial";
    set test variable                   ${query_promo_code}         ${query_promo_code[2][0]}
    Click Element                       ${PROMO_CODE}
    Log To Console                      ${\n} Promo Code is ${query_promo_code}
    Input Text                          ${PROMO_CODE}        ${query_promo_code}
    Click Element                       ${PROMO_CF}
    Wait Until Page Contains Element    //*[@id="container"]/div[1]/div[6]/div/div/div[8]/div[2]/span/div[1]


Payment Cash
    Page Should Contain                  เลือกวิธีชำระเงิน
    Click Element                        ${PAYMENT}
    Select Checkbox                      ${CASH}
    Click Button                         บันทึก


Payment Creditcard
    Page Should Contain                  เลือกวิธีชำระเงิน
    Click Element                        ${PAYMENT}
    Select Checkbox                      ${CREDIT_CARD}
    Click Button                         บันทึก


Promocode KTC
    Click Element                       ${PROMO_CODE}
    Input Text                          ${PROMO_CODE}        KTCTEST
    Click Element                       ${PROMO_CF}
    Wait Until Page Contains Element    //*[@id="container"]/div[1]/div[6]/div/div/div[8]/div[2]/span/div[1]


Promocode VISA
    Click Element                       ${PROMO_CODE}
    Input Text                          ${PROMO_CODE}        VISATEST
    Click Element                       ${PROMO_CF}
    Wait Until Page Contains Element    //*[@id="container"]/div[1]/div[6]/div/div/div[8]/div[2]/span/div[1]


Promocode Radius
    Click Element                       ${PROMO_CODE}
    Input Text                          ${PROMO_CODE}        namtoey100per
    Click Element                       ${PROMO_CF}
    Wait Until Page Contains Element    //*[@id="container"]/div[1]/div[6]/div/div/div[8]/div[2]/span/div[1]


Add Creditcard KTC
    Page Should Contain                  เลือกวิธีชำระเงิน
    Click Element                        ${PAYMENT}
    Select Checkbox                      ${CREDIT_CARD}
#    Click Button                         บันทึก
    Wait Until Page Contains Element                    ${Add_Creditcard}
    Scroll Element Into View                            ${Add_Creditcard}
    Click Element                                       ${Add_Creditcard}
    Input Text                                          ${CREDI_NUM}            4391 3700 1478 6111
    Input Text                                          ${CREDI_NAME}           ${CREDIT_NAME}
    Input Text                                          ${CREDI_EXP}            ${CREDIT_EXP}
    Input Text                                          ${CREDI_CVV}            ${CREDIT_CVV}
    Click Button                                        ${CF_CREDITCARD}
    Wait Until Page Contains                            เพิ่ม/เลือก บัตรสำเร็จแล้วค่ะ ลูกค้าอาจจะได้รับ SMS แจ้งการชาร์จเงิน 20 บาทจากธนาคาร ทั้งนี้เป็นเพียงการ Hold เงินเพื่อทดสอบบัตร และจะทำการคืนเงินจำนวนนี้ให้ในภายหลังค่ะ
    Wait Until Page Contains Element                    ${Add_Creditcard}
    Scroll Element Into View                            ${Add_Creditcard}
    Click Element                                       ${KTC_CARD}
    Click Button                                        บันทึก
    Wait Until Page contains                            สำเร็จ

Add Creditcard VISA
    Page Should Contain                  เลือกวิธีชำระเงิน
    Click Element                        ${PAYMENT}
    Select Checkbox                      ${CREDIT_CARD}
#    Click Button                         บันทึก
    Wait Until Page Contains Element                    ${Add_Creditcard}
    Scroll Element Into View                            ${Add_Creditcard}
    Click Element                                       ${Add_Creditcard}
    Input Text                                          ${CREDI_NUM}            4242 4242 4242 4242
    Input Text                                          ${CREDI_NAME}           ${CREDIT_NAME}
    Input Text                                          ${CREDI_EXP}            ${CREDIT_EXP}
    Input Text                                          ${CREDI_CVV}            ${CREDIT_CVV}
    Click Button                                        ${CF_CREDITCARD}
    Wait Until Page Contains                            เพิ่ม/เลือก บัตรสำเร็จแล้วค่ะ ลูกค้าอาจจะได้รับ SMS แจ้งการชาร์จเงิน 20 บาทจากธนาคาร ทั้งนี้เป็นเพียงการ Hold เงินเพื่อทดสอบบัตร และจะทำการคืนเงินจำนวนนี้ให้ในภายหลังค่ะ
    Wait Until Page Contains Element                    ${Add_Creditcard}
    Scroll Element Into View                            ${Add_Creditcard}
    Click Element                                       ${VISA_CARD}
    Click Button                                        บันทึก
    Wait Until Page contains                            สำเร็จ


Permission Denied KTC
    Wait Until Page Contains                            รหัสส่วนลดนี้ ใช้ได้เฉพาะลูกค้าชำระค่าบริการ SKOOTAR ด้วยบัตรเครดิต KTC เท่านั้นค่ะ


Permission Accepted KTC
    Wait Until Page Contains                            KTC TEST 10%


Permission Denied VISA
    Wait Until Page Contains                             รหัสส่วนลดนี้ ใช้ได้เฉพาะลูกค้าชำระค่าบริการ SKOOTAR ด้วยบัตรเครดิต VISA เท่านั้นค่ะ


Permission Accepted VISA
    Wait Until Page Contains                            สิทธิพิเศษสำหรับสมาชิกบัตรเครดิต VISA รับส่วนลด 50% ทุกการสั่งงาน


Permission Denied Usergroup KTC
#    Wait Until Element Contains         //div[@class='_2I0Z'][2]/div[@class='_29wu']        0
    Wait Until Page Contains                            คุณไม่สามารถใช้โปรโมชั่นได้ค่ะ


Permission Denied Usergroup VISA
#    Wait Until Element Contains         //div[@class='_2I0Z'][2]/div[@class='_29wu']        0
    Wait Until Page Contains                            คุณไม่สามารถใช้โปรโมชั่นได้ค่ะ


Permission Accepted Radius
    Wait Until Page Contains                            ส่วนลด ทดสอบวัดระยะห่างจากจุดที่กำหนด 100%


Distance Over Radius
    Wait Until Page Contains                            ไม่สามารถใช้โปรโมชั่นได้ เนื่องจาก งานอยู่นอกพื้นที่ที่กำหนดค่ะ


Payment Wallet
    Page Should Contain                  เลือกวิธีชำระเงิน
    Click Element                        ${PAYMENT}
    Select Checkbox                      ${WALLET}
    Click Button                         บันทึก


Payment Promptpay
    Page Should Contain                  เลือกวิธีชำระเงิน
    Click Element                        ${PAYMENT}
    Select Checkbox                      ${PROMPTPAY}
    Click Button                         บันทึก


Payment Invoice
    Page Should Contain                  เลือกวิธีชำระเงิน
    Click Element                        ${PAYMENT}
    Select Checkbox                      ${INVOICE}
    Click Button                         บันทึก


Note Job (cash,promptpay)
    Wait Until Page Contains Element     ${REM}
    Input Text                           ${REM}         ${REMARK}
    Click Element                        ${TIME_CF}
    Click Element                        ${PAY_POINT}
    Wait Until Page Contains Element     ${CREATE_JOB}
    Click Element                        ${CREATE_JOB}


Note Job
    Wait Until Page Contains Element     ${REM}
    Input Text                           ${REM}         ${REMARK}
    Click Element                        ${TIME_CF}
#    Click Element                       ${PAY_POINT}
    Click Element                        ${CREATE_JOB}


Alert Invoice Coperate
    Page Should Contain                 ต้องการออกใบเสร็จในนามบริษัท


Alert Crete Job Success
    Page Should Contain                 ยืนยันการสั่งงาน


End Case Testcase 50
    #Take Screenshot                     ${path_test_report}TC_${INDEX_TESTCASE50}.jpg
    Close Browser


End Case Testcase 51
    #Take Screenshot                     ${path_test_report}TC_${INDEX_TESTCASE51}.jpg
    Close Browser


End Case Testcase 52
    #Take Screenshot                     ${path_test_report}TC_${INDEX_TESTCASE52}.jpg
    Close Browser


End Case Testcase 53
    #Take Screenshot                     ${path_test_report}TC_${INDEX_TESTCASE53}.jpg
    Close Browser


End Case Testcase 54
    #Take Screenshot                     ${path_test_report}TC_${INDEX_TESTCASE54}.jpg
    Close Browser


End Case Testcase 55
    #Take Screenshot                     ${path_test_report}TC_${INDEX_TESTCASE55}.jpg
    Close Browser


End Case Testcase 56
    #Take Screenshot                     ${path_test_report}TC_${INDEX_TESTCASE56}.jpg
    Close Browser


End Case Testcase 57
    #Take Screenshot                     ${path_test_report}TC_${INDEX_TESTCASE57}.jpg
    Close Browser


#===========================================================================================================================

Test Teardown Testcase 50
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE50}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE50}
    Disconnect From Database


Save Test Result Pass Testcase 50
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE50}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE50}.html
    ${INDEX_TESTCASE50}=             Evaluate            int(${INDEX_TESTCASE50}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE50}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

Save Test Result Fail Testcase 50
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE50}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE50}.html
    ${INDEX_TESTCASE50}=             Evaluate            int(${INDEX_TESTCASE50}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE50}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#===========================================================================================================================

Test Teardown Testcase 51
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE51}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE51}
    Disconnect From Database


Save Test Result Pass Testcase 51
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE51}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE51}.html
    ${INDEX_TESTCASE51}=             Evaluate            int(${INDEX_TESTCASE51}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE51}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

Save Test Result Fail Testcase 51
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE51}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE51}.html
    ${INDEX_TESTCASE51}=             Evaluate            int(${INDEX_TESTCASE51}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE51}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#===========================================================================================================================

Test Teardown Testcase 52
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE52}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE52}
    Disconnect From Database


Save Test Result Pass Testcase 52
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE52}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE52}.html
    ${INDEX_TESTCASE52}=             Evaluate            int(${INDEX_TESTCASE52}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE52}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

Save Test Result Fail Testcase 52
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE52}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE52}.html
    ${INDEX_TESTCASE52}=             Evaluate            int(${INDEX_TESTCASE52}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE52}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#===========================================================================================================================

Test Teardown Testcase 53
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE53}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE53}
    Disconnect From Database


Save Test Result Pass Testcase 53
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE53}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE53}.html
    ${INDEX_TESTCASE53}=             Evaluate            int(${INDEX_TESTCASE53}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE53}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

Save Test Result Fail Testcase 53
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE53}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE53}.html
    ${INDEX_TESTCASE53}=             Evaluate            int(${INDEX_TESTCASE53}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE53}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#===========================================================================================================================

Test Teardown Testcase 54
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE54}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE54}
    Disconnect From Database


Save Test Result Pass Testcase 54
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE54}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE54}.html
    ${INDEX_TESTCASE54}=             Evaluate            int(${INDEX_TESTCASE54}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE54}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

Save Test Result Fail Testcase 54
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE54}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE54}.html
    ${INDEX_TESTCASE54}=             Evaluate            int(${INDEX_TESTCASE54}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE54}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#===========================================================================================================================

Test Teardown Testcase 55
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE55}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE55}
    Disconnect From Database


Save Test Result Pass Testcase 55
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE55}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE55}.html
    ${INDEX_TESTCASE55}=             Evaluate            int(${INDEX_TESTCASE55}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE55}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

Save Test Result Fail Testcase 55
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE55}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE55}.html
    ${INDEX_TESTCASE55}=             Evaluate            int(${INDEX_TESTCASE55}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE55}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#===========================================================================================================================

Test Teardown Testcase 56
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE56}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE56}
    Disconnect From Database


Save Test Result Pass Testcase 56
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE56}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE56}.html
    ${INDEX_TESTCASE56}=             Evaluate            int(${INDEX_TESTCASE56}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE56}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

Save Test Result Fail Testcase 56
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE56}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE56}.html
    ${INDEX_TESTCASE56}=             Evaluate            int(${INDEX_TESTCASE56}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE56}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#===========================================================================================================================

Test Teardown Testcase 57
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE57}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE57}
    Disconnect From Database


Save Test Result Pass Testcase 57
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE57}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE57}.html
    ${INDEX_TESTCASE57}=             Evaluate            int(${INDEX_TESTCASE57}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE57}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

Save Test Result Fail Testcase 57
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE57}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE57}.html
    ${INDEX_TESTCASE57}=             Evaluate            int(${INDEX_TESTCASE57}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE57}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#=======================================================================================================================

