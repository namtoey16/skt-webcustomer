﻿*** Keywords ***

Connect DB
    connect to database  pymysql    ${DB_name}  ${DB_user}  ${DB_pass}  ${DB_host}  ${DB_port}
    Execute SQL String   ${query_user_status}
    Execute SQL String   ${query_set_usergroup}
    Execute SQL String   ${query_invoice}


Open Browser for Run Test
    Open Browser                            ${URL}              ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed   0.3
    Sleep    2s


Login with Email
    Click Element                           ${LOG_IN}
    Wait Until Page Contains Element        ${MAIL}
    Wait Until Page Contains Element        ${PAS_WORD}
    Input Text                              ${MAIL}             ${EMAIL}
    Input Text                              ${PAS_WORD}         ${PASSWORD}
    Click Element                           ${LOGIN_EMAIL}


Login with Email Coperate
    Click Element                           ${LOG_IN}
    Wait Until Page Contains Element        ${MAIL}
    Wait Until Page Contains Element        ${PAS_WORD}
    Input Text                              ${MAIL}             ${EMAIL_COMP}
    Input Text                              ${PAS_WORD}         ${PASSWORD_COMP}
    Click Element                           ${LOGIN_EMAIL}


New User
    Wait Until Page Contains                ยินดีต้อนรับลูกค้าใหม่
    Click Button                            ยืนยัน


Alert Promotion
    Wait Until Page Contains                งาน Thailand E-Commerce รับส่วนลด 8% ไม่จำกัดจำนวนครั้ง
    Click Button                            ${Copycode}
    Wait Until Page Contains                คัดลอกโค้ดสำเร็จแล้ว



Login Success
    Wait Until Page Does Not Contain Element          ${P_LOADER}
    Wait Until Page Contains Element                  ${LOGO}
    Page Should Contain Element                       ${Icon1}
    Page Should Contain Element                       ${Icon2}
    Page Should Contain Element                       ${Icon3}
    Page Should Contain Element                       ${Icon4}
    Page Should Contain Element                       ${Icon5}
    Page Should Contain Element                       ${Icon6}
    Page Should Contain Element                       ${Icon7}
    Page Should Contain Element                       ${Icon8}
    Page Should Contain Element                       ${Icon9}
    Page Should Contain Element                       ${Icon10}


Profile Origin
    Click Element                       ${Add1stplace}
    Click Element                       ${NowLocation}
    Wait Until Page Contains Element    ${NAM_ORG}
    Press Keys                          ${NAM_ORG}                  CTRL+a\ue017
    Input Text                          ${NAM_ORG}                  ${NAMEORG}
    Press Keys                          ${PHON_ORG}                 CTRL+a\ue017
    Input Text                          ${PHON_ORG}                 ${PHONEORG}
    Click Button                       ยืนยัน


Profile Destination 1
    Click Element                       ${Add2ndplace}
    Click Element                       ${NowLocation}
    Wait Until Page Contains Element    ${NAM_DES}
    Press Keys                          ${NAM_DES}                  CTRL+a\ue017
    Input Text                          ${NAM_DES}                  ${NAMEDES}
    Press Keys                          ${PHON_DES}                 CTRL+a\ue017
    Input Text                          ${PHON_DES}                 ${PHONEDES}
    Click Button                       ยืนยัน


Go to Step 2
#    Wait Until Page Contains           ราคานี้อาจมีการเปลี่ยนแปลงตามการวิ่งงานจริง เช่น กรณีแก้ไข/เพิ่ม/ลด สถานที่ มีค่าที่จอดรถ ค่ารอ ค่าพัสดุน้ำหนักเกิน
    Click Element                       ${NEXT_TO_STEP2}


Select Service
    Click Element                       ${SELECT_SERVICE}


Select Option
    Wait Until Page Does Not Contain Element                        ${P_LOADER}
    ${PRICE_OPTION}=                    Query  ${query_price_option}
    set test variable                   ${PRICE_OPTION}             ${PRICE_OPTION[0][0]}
    ${PRICE_OPTION}=                    Evaluate                    float(${PRICE_OPTION})
    Set Global variable                 ${PRICE_OPTION}
    Log To Console                      \nPrice Option ${PRICE_OPTION} bath
    Click Element                       ${SELECT_OPTION}


Calculate Distance
    Wait Until Page Does Not Contain Element                        ${P_LOADER}
    ${TOTALDISTANCE}                    Get Text                    ${TOTALDISTANCE}
    Set Global variable                 ${TOTALDISTANCE}
    Log To Console                      \nTotal Distance ${TOTALDISTANCE} km


Calculate Price
    Wait Until Page Does Not Contain Element                        ${P_LOADER}
    ${PRICE_USERGROUP}=                 Query  ${query_price.format(distance = ${TOTALDISTANCE})}
    set test variable                   ${PRICE_USERGROUP}          ${PRICE_USERGROUP[0][0]}
    Run Keyword If                      ${PRICE_OPTION} != 0.5      Option Not Return Trip
    Run Keyword If                      ${PRICE_OPTION} == 0.5      Option Return Trip


Option Not Return Trip
    ${PRICE_USERGROUP}=                 Evaluate                    float(${PRICE_USERGROUP})
    ${NETPRICE}=                        Evaluate                    float(${PRICE_USERGROUP}+${PRICE_OPTION})
    ${NETPRICE}=                        Evaluate                    math.ceil(${NETPRICE})      math
    ${NETPRICE}=                        Evaluate                    int(${NETPRICE})
    ${PRICE_USERGROUP}                  Convert To String           ${PRICE_USERGROUP}
    ${NETPRICE}                         Convert To String           ${NETPRICE}
    Log To Console                      \nPrice Option Not *0.5
    Log To Console                      Price ${PRICE_USERGROUP} bath
    Log To Console                      Net Price must be ${NETPRICE} bath
    Wait Until Page Contains Element                                ${CHK_PRICE}
    Element Text Should Be              ${CHK_PRICE}                ${NETPRICE}
    Set Global variable                                             ${NETPRICE}


Option Return Trip
    ${PRICE_USERGROUP}=                 Evaluate                    float(${PRICE_USERGROUP})
    ${NETPRICE}=                        Evaluate                    float(${PRICE_USERGROUP}+(${PRICE_USERGROUP}*${PRICE_OPTION}))
    ${NETPRICE}=                        Evaluate                    math.ceil(${NETPRICE})      math
    ${NETPRICE}=                        Evaluate                    int(${NETPRICE})
    ${PRICE_USERGROUP}                  Convert To String           ${PRICE_USERGROUP}
    ${NETPRICE}                         Convert To String           ${NETPRICE}
    Log To Console                      \nPrice Option *0.5
    Log To Console                      Price ${PRICE_USERGROUP} bath
    Log To Console                      Net Price must be ${NETPRICE} bath
    Wait Until Page Contains Element                                ${CHK_PRICE}
    Element Text Should Be              ${CHK_PRICE}                ${NETPRICE}
    Set Global variable                                             ${NETPRICE}


Calculate Price Discount Free_credit
    ${query_freecredit}=                Query    SELECT free_credit FROM SKOOTAR_DEV.tbl_user where email = "${EMAIL_COMP}" ; #change username email
    set test variable                   ${query_freecredit}         ${query_freecredit[0][0]}
    ${freeCredit}=                      Convert To String           ${query_freecredit}
    ${NEW_NETPRICE}=                    Evaluate                    ${NETPRICE}-${query_freecredit}
    ${NEW_NETPRICE}=                    Convert To String           ${NEW_NETPRICE}
    Run Keyword If      ${query_freecredit} < ${NETPRICE}           Log To Console              ${\n}Net price = ${NEW_NETPRICE}
    Run Keyword If      ${query_freecredit} < ${NETPRICE}           Log To Console              Price = ${NETPRICE}
    Run Keyword If      ${query_freecredit} < ${NETPRICE}           Log To Console              Free credit = ${Freecredit}
    Run Keyword If      ${query_freecredit} < ${NETPRICE}           Element Text Should Be      ${CHK_NETPRICE}     ${NEW_NETPRICE}
    Run Keyword If      ${query_freecredit} < ${NETPRICE}           Element Text Should Be      ${CHK_PRICE}        ${NETPRICE}
    Run Keyword If      ${query_freecredit} < ${NETPRICE}           Element Text Should Be      ${CHK_FREECREDIT}   ${freeCredit}


Go to Step 3
    Sleep   3s
    Click Button                        ขั้นตอนถัดไป


Payment Cash
    Page Should Contain                 เลือกวิธีชำระเงิน
    Click Element                       ${PAYMENT}
    Select Checkbox                     ${CASH}
    Click Button                        บันทึก


Payment Creditcard
    Page Should Contain                 เลือกวิธีชำระเงิน
    Click Element                       ${PAYMENT}
    Select Checkbox                     ${CREDIT_CARD}
    Wait Until Page Contains Element    ${Add_Creditcard}
    Scroll Element Into View            ${Add_Creditcard}
    Click Element                       ${Add_Creditcard}
    Input Text                          ${CREDI_NUM}                4111 1111 1111 1111
    Input Text                          ${CREDI_NAME}               ${CREDIT_NAME}
    Input Text                          ${CREDI_EXP}                ${CREDIT_EXP}
    Input Text                          ${CREDI_CVV}                ${CREDIT_CVV}
    Click Button                        ${CF_CREDITCARD}
    Wait Until Page Contains            เพิ่ม/เลือก บัตรสำเร็จแล้วค่ะ ลูกค้าอาจจะได้รับ SMS แจ้งการชาร์จเงิน 20 บาทจากธนาคาร ทั้งนี้เป็นเพียงการ Hold เงินเพื่อทดสอบบัตร และจะทำการคืนเงินจำนวนนี้ให้ในภายหลังค่ะ
    Click Button                        บันทึก


Payment Wallet
    Page Should Contain                 เลือกวิธีชำระเงิน
    Click Element                       ${PAYMENT}
    Select Checkbox                     ${WALLET}
    Click Button                        บันทึก


Payment Promptpay
    Page Should Contain                         เลือกวิธีชำระเงิน
    Click Element                               ${PAYMENT}
    Select Checkbox                             ${PROMPTPAY}
    Click Button                                บันทึก


Payment Invoice
    Page Should Contain                         เลือกวิธีชำระเงิน
    Click Element                               ${PAYMENT}
    Select Checkbox                             ${INVOICE}
    Click Button                                บันทึก


Note Job (cash,promptpay)
    Wait Until Page Contains Element            ${REM}
    Input Text                                  ${REM}                  ${REMARK}
    Click Element                               ${TIME_CF}
    Click Element                               ${PAY_POINT}
    Wait Until Page Contains Element            ${CREATE_JOB}
    Click Element                               ${CREATE_JOB}


Note Job
    Wait Until Page Contains Element            ${REM}
    Input Text                                  ${REM}                  ${REMARK}
    Click Element                               ${TIME_CF}
#    Click Element                              ${PAY_POINT}
    Click Element                               ${CREATE_JOB}


Alert Invoice Coperate
    Page Should Contain                         ต้องการออกใบเสร็จในนามบริษัท


Alert Crete Job Success
    Page Should Contain                         ยืนยันการสั่งงาน


End Case Testcase 44
    #Take Screenshot                            ${path_test_report}TC_${INDEX_TESTCASE44}.jpg
    Close Browser


End Case Testcase 45
    #Take Screenshot                            ${path_test_report}TC_${INDEX_TESTCASE45}.jpg
    Close Browser


End Case Testcase 46
    #Take Screenshot                            ${path_test_report}TC_${INDEX_TESTCASE46}.jpg
    Close Browser


End Case Testcase 47
    #Take Screenshot                            ${path_test_report}TC_${INDEX_TESTCASE47}.jpg
    Close Browser


End Case Testcase 48
    #Take Screenshot                            ${path_test_report}TC_${INDEX_TESTCASE48}.jpg
    Close Browser


End Case Testcase 66
    #Take Screenshot                            ${path_test_report}TC_${INDEX_TESTCASE66}.jpg
    Close Browser


#===========================================================================================================================

Test Teardown Testcase 44
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE44}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE44}
    Disconnect From Database


Save Test Result Pass Testcase 44
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE44}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE44}.html
    ${INDEX_TESTCASE44}=             Evaluate            int(${INDEX_TESTCASE44}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE44}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents


Save Test Result Fail Testcase 44
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE44}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE44}.html
    ${INDEX_TESTCASE44}=             Evaluate            int(${INDEX_TESTCASE44}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE44}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#===========================================================================================================================

Test Teardown Testcase 45
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE45}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE45}
    Disconnect From Database


Save Test Result Pass Testcase 45
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE45}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE45}.html
    ${INDEX_TESTCASE45}=             Evaluate            int(${INDEX_TESTCASE45}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE45}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents


Save Test Result Fail Testcase 45
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE45}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE45}.html
    ${INDEX_TESTCASE45}=             Evaluate            int(${INDEX_TESTCASE45}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE45}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#===========================================================================================================================

Test Teardown Testcase 46
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE46}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE46}
    Disconnect From Database


Save Test Result Pass Testcase 46
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE46}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE46}.html
    ${INDEX_TESTCASE46}=             Evaluate            int(${INDEX_TESTCASE46}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE46}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents


Save Test Result Fail Testcase 46
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE46}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE46}.html
    ${INDEX_TESTCASE46}=             Evaluate            int(${INDEX_TESTCASE46}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE46}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#===========================================================================================================================

Test Teardown Testcase 47
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE47}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE47}
    Disconnect From Database


Save Test Result Pass Testcase 47
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE47}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE47}.html
    ${INDEX_TESTCASE47}=             Evaluate            int(${INDEX_TESTCASE47}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE47}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents


Save Test Result Fail Testcase 47
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE47}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE47}.html
    ${INDEX_TESTCASE47}=             Evaluate            int(${INDEX_TESTCASE47}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE47}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#===========================================================================================================================

Test Teardown Testcase 48
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE48}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE48}
    Disconnect From Database


Save Test Result Pass Testcase 48
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE48}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE48}.html
    ${INDEX_TESTCASE48}=             Evaluate            int(${INDEX_TESTCASE48}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE48}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents


Save Test Result Fail Testcase 48
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE48}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE48}.html
    ${INDEX_TESTCASE48}=             Evaluate            int(${INDEX_TESTCASE48}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE48}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#===========================================================================================================================

Test Teardown Testcase 66
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE66}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE66}
    Disconnect From Database


Save Test Result Pass Testcase 66
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE66}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE66}.html
    ${INDEX_TESTCASE66}=             Evaluate            int(${INDEX_TESTCASE66}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE66}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents


Save Test Result Fail Testcase 66
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE66}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE66}.html
    ${INDEX_TESTCASE66}=             Evaluate            int(${INDEX_TESTCASE66}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE66}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#===========================================================================================================================