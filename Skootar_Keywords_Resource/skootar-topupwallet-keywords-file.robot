﻿*** Keywords ***

Open Chrome
    ${chrome_options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}    add_argument    --disable-extensions
    Call Method    ${chrome_options}    add_argument    --headless
    Call Method    ${chrome_options}    add_argument    --disable-gpu
    Call Method    ${chrome_options}    add_argument    --no-sandbox
    Create Webdriver    Chrome    chrome_options=${chrome_options}


Connect DB
    connect to database  pymysql    ${DB_name}  ${DB_user}  ${DB_pass}  ${DB_host}  ${DB_port}
#    Execute SQL String   ${query_prepaid_code}


Account Customer
    Open Excel Document     ${path_test_report}customer.xlsx     doc_id=doc1
    ${username}=  Read Excel Cell   row_num=2       col_num=1
    ${password}=  Read Excel Cell   row_num=2       col_num=2
    Log To Console                                  \n${user_name}
    Log To Console                                  ${password}
    Save Excel Document                             ${path_test_report}customer.xlsx
    Close All Excel Documents


Open Browser for Run Test
    Open Browser       ${URL}       ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed   0.3
    Sleep    2s

Login with Email
    Click Element                           ${LOG_IN}
    Wait Until Page Contains Element        ${MAIL}
    Wait Until Page Contains Element        ${PAS_WORD}
    Input Text                              ${MAIL}             ${EMAIL}
    Input Text                              ${PAS_WORD}         ${PASSWORD}
    Click Element                           ${LOGIN_EMAIL}


Login Success
    Wait Until Page Does Not Contain Element                    ${P_LOADER}
    Wait Until Page Contains Element                            ${LOGO}
    Page Should Contain Element                                 ${Icon1}
    Page Should Contain Element                                 ${Icon2}
    Page Should Contain Element                                 ${Icon3}
    Page Should Contain Element                                 ${Icon4}
    Page Should Contain Element                                 ${Icon5}
    Page Should Contain Element                                 ${Icon6}
    Page Should Contain Element                                 ${Icon7}
    Page Should Contain Element                                 ${Icon8}
    Page Should Contain Element                                 ${Icon9}
    Page Should Contain Element                                 ${Icon10}


Skootar Wallet
    Click Element                                               ${TAB_WALLET}
    Wait Until Page Contains                                    ยอดเงินคงเหลือปัจจุบัน


Check Credit Before
    ${before_prepaid_credit}=   Query    select prepaid_credit from SKOOTAR_DEV.tbl_user where user_name="namtoey16@gmail.com";
    set test variable           ${before_prepaid_credit}        ${before_prepaid_credit[0][0]}
    Log To Console              ${\n}Before you have credit ${before_prepaid_credit}


Topup Wallet
    Click Element                                               ${TOPUP_WALLET}
    Wait Until Page Contains                                    เติมเงินด้วยรหัสเติมเงิน (Prepaid code)


Input Prepaid Code
    Wait Until Page Contains    เติมเงินด้วยรหัสเติมเงิน (Prepaid code)
    ${query_prepaid_amount}=    Query    select prepaid_amount from SKOOTAR_DEV.tbl_prepaid_code where prepaid_code_status = 1 and created_by = "NT" LIMIT 1;
    set test variable           ${query_prepaid_amount}         ${query_prepaid_amount[0][0]}
    ${query_prepaid_code}=      Query    select prepaid_code from SKOOTAR_DEV.tbl_prepaid_code where prepaid_code_status = 1 and created_by = "NT" LIMIT 1;
    set test variable           ${query_prepaid_code}           ${query_prepaid_code[0][0]}
    ${prepaid_code}=            Convert To String               ${query_prepaid_code}
    Log To Console              \nPrepaid amount is ${query_prepaid_amount}
    Log To Console              Prepaid code is ${prepaid_code}
    Set Global variable                                         ${prepaid_code}
    Press Keys                  ${input_prepaid_code}           CTRL+a\uE012
    Input Text                  ${input_prepaid_code}           ${prepaid_code}
    Press Keys                  ${input_prepaid_code}           CTRL+a\uE012
    Input Text                  ${input_prepaid_code}           ${prepaid_code}
#    Page Should Contain         ${prepaid_code[0]}${prepaid_code[1]}${prepaid_code[2]}${prepaid_code[3]}${prepaid_code[4]}-${prepaid_code[5]}${prepaid_code[6]}${prepaid_code[7]}${prepaid_code[8]}


Confirm Prepaid Code
    Click Button                                                บันทึก
    Wait Until Page Contains                                    สำเร็จ


Check Credit After
    ${after_prepaid_credit}=    Query    select prepaid_credit from SKOOTAR_DEV.tbl_user where user_name="namtoey16@gmail.com";
    set test variable           ${after_prepaid_credit}         ${after_prepaid_credit[0][0]}
    Log To Console              ${\n}After you have credit ${after_prepaid_credit}


End Case Testcase 43
    #Take Screenshot                     ${path_test_report}TC_${INDEX_TESTCASE43}.jpg
    Close Browser


#===========================================================================================================================

Test Teardown Testcase 43
    Run Keyword If All Tests Passed                     Save Test Result Pass Testcase ${INDEX_TESTCASE43}
    Run Keyword If Any Tests Failed                     Save Test Result Fail Testcase ${INDEX_TESTCASE43}
    Disconnect From Database


Save Test Result Pass Testcase 43
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE43}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE43}.html
    ${INDEX_TESTCASE43}=             Evaluate            int(${INDEX_TESTCASE43}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE43}              3               ${TEST_RESULT_PASS}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

Save Test Result Fail Testcase 43
    Move File                                           ./log.html          ${path_test_report}log_testcase_${INDEX_TESTCASE43}.html
    Move File                                           ./report.html       ${path_test_report}report_testcase_${INDEX_TESTCASE43}.html
    ${INDEX_TESTCASE43}=             Evaluate            int(${INDEX_TESTCASE43}+1)
    Open Excel Document                                 ${path_test_report}sum-test-report.xlsx    sheetNames=${sheetNames}
    Write Excel Cell                                    ${INDEX_TESTCASE43}              3               ${TEST_RESULT_FAIL}
    Save Excel Document                                 ${path_test_report}sum-test-report.xlsx
    Close All Excel Documents

#===========================================================================================================================