﻿# This Python file uses the following encoding: utf-8
import os, sys, random, toeyTest

USERGROUP = '2019_NormalWP20' # random.choice(['DEFAULT','2019_NormalWP20','NormalWalk','45NewSkootar','DEFAULT','NewNormal','P9PreSurvey'])
SERVICE = toeyTest.randomService(0) #random.randint(1,3) # 1 doc ,  2 food  3  parcel
OPTION = toeyTest.chooseOption(USERGROUP)
MAPOPTION = toeyTest.mapOption(OPTION)
TOTALDISTANCE= str ('//div[@class="_2I0Z"][4]/div[@class="_29wu"]')
query_set_usergroup = 'update SKOOTAR_DEV.tbl_user set user_group_id="'+str(USERGROUP)+'" where user_name = "test@mail.com";'
query_price = 'SELECT customer_fee FROM SKOOTAR_DEV.tbl_price_list_v2 where user_group_id = "'+str(USERGROUP)+'" and km={distance} and delivery_type='+str(SERVICE)+';'
query_price_option = 'SELECT customer_fee FROM SKOOTAR_DEV.tbl_price_option_v2 where user_group_id = "'+str(USERGROUP)+'" and delivery_type='+str(SERVICE)+' and job_option='+str(MAPOPTION)+';'
query_user_status = 'update SKOOTAR_DEV.tbl_user set user_status=-1 where user_name = "12345company@gmail.com";'
query_invoice_over = 'update SKOOTAR_DEV.tbl_invoice set invoice_status = 1 , invoice_date ="2019-11-20" , payment_due_date = "2019-11-20" where invoice_id = "INV191000099";'
P_LOADER   = '//*[contains(@class, \'modal-open\')]'
LOG_IN = '//*[@id="gatsby-focus-wrapper"]//li/a[@href="th/login"]'
LOGI_FB = '//div[6]/button/div/div'
MAIL = '//input[@name="email"]'
PAS_WORD = '//input[@name="password"]'
F_EMAIL = '//*[@id="email"]'
F_PASSWORD = '//*[@id="pass"]'
LOGIN_EMAIL = '//button[@name="btnLogin"]'
LOGIN_FB = '//*[@id="u_0_0"]'
LOGO = '//div[3]/div/div/div/span[@class="_2wc-"]/div/div'
LOGO_NOLOGIN = '//div[3]/div/div/div/span[@class="_2wc-"]/div/div'
Icon1 = '//div[1]/div/span/div/div/span'
Icon2 = '//div[2]/div/span/div/div/span'
Icon3 = '//div[3]/div/span/div/div/span'
Icon4 = '//div[4]/div/span/div/div/span'
Icon5 = '//div[5]/div/span/div/div'
Icon6 = '//div[6]/div/span/div/div/span'
Icon7 = '//div[7]/div/span/div/div/span'
Icon8 = '//div[8]/div/span/div/div/span' 
Icon9 = '//div[9]/div/span/div/div/span'
Icon10 = '//div[@role="menu"]'
INV_STATUS_1 = '//*[@id="main-content"]/div/div[1]/div/div/div/div[3]/div[1]/div/div[3]'
PAY_INVOICE = '//div[@class="_1qzK"]/button'
PAYMENT_INVOICE = '//div[@class="_1TKd"]'
CHOOSE_CREDITCARD = '//*[contains(text(),"บัตรเครดิต")]'
PAY_INVOICE_SUCCESS = '//div/div[1]/div/div/div[2]/div[3]/div/div/button'
CheckPrice = '//*[@id="main-content"]/div/div/section[1]/div/div/div/div/div/div/div[1]/a/div/button'
Copycode = '//div[@class="_2aji"]/button' 
Closepopup = '//div[14]/div/div[1]/div/div/div/div[1]/div[1]/div/button/div/div'
Add1stplace = '//*[@id="main-content"]//div[1]/div[2]/div[2]/button/div'
NowLocation = '//*[@id="main-content"]/div/div[1]/div/div[1]/div/div/div[2]/div[2]/div/div[1]/div/button/div/div/div'
NAM_ORG = '//input[@name="contactName"]'
PHON_ORG = '//input[@name="contactPhone"]'
Add2ndplace = '//*[@id="main-content"]//div[2]/div[2]/div[2]/button/div'
Add3rdplace = '//*[@id="main-content"]/div/div[1]/div/div[1]/div/div/div/div[2]/div/div[1]/div/div/div[1]/div[1]/div[2]/button'
LOC_2 = '//input[@type="text"]'
CHOOSE_LOC2 = '//*[@id="main-content"]/div/div[1]/div/div[1]/div/div/div[2]/div[2]/div/div[1]/div/button[2]'
NAM_DES = '//input[@name="contactName"]'
PHON_DES = '//input[@name="contactPhone"]'
SELECT_SERVICE = '//*[@id="main-content"]/div/div[1]/div/div[1]/div/div/div/div[2]/div/div[1]/div/div[1]/div/div['+str(SERVICE)+']/button/div/div/div/div'
SELECT_OPTION = '//*[@id="main-content"]/div/div[1]/div/div[1]/div/div/div/div[2]/div/div[1]/div/div[2]/div/div/div['+str(OPTION)+']/span'
CHK_NETPRICE = "//div[@class='amAj']/div[@class='_29wu']"
CHK_PRICE = "//div[@class='_2I0Z'][1]/div[@class='_29wu']"
CHK_DISCOUNT = "//div[@class='_2I0Z'][2]/div[@class='_29wu']"
CHK_FREECREDIT = "//div[@class='_2I0Z'][3]/div[@class='_29wu']"
#PRICE = str(toeyTest.calPrice(SERVICE,OPTION))
#PRICE_USERGROUP = str(toeyTest.calPrice_UserGroup(SERVICE,OPTION,USERGROUP))
PRICEADD1POINT = str(toeyTest.calPriceAdd1Point(SERVICE,OPTION))
PRICEADD2POINT = str(toeyTest.calPriceAdd2Point(SERVICE,OPTION))
PRICEADD1POINT_USERGROUP = str(toeyTest.calPriceAdd1Point_UserGroup(SERVICE,OPTION,USERGROUP))
PRICEADD2POINT_USERGROUP = str(toeyTest.calPriceAdd2Point_UserGroup(SERVICE,OPTION,USERGROUP))
NEXT_TO_STEP2 = '//*[@id="main-content"]//div[3]/div[2]/div/div/button'
NEXT_TO_STEP3 = '//*[@id="main-content"]//div[2]/div/div/div[2]/button'


print  ("user : ", USERGROUP)
print ("sevice = ", SERVICE)
print ("option = ", OPTION)
print ("*********************************************")