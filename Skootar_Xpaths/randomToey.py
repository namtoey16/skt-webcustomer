﻿import os, sys
import random
xrange=range
#==================randomContactIn=================#
#def randomContactIn(contI):	
#	contI = random.choice(["0818133344","0819355554","0819793337","0819132901","0818374848","0811659246","0818589889","0899245544","0818052929","0898918435","0819898070","0818258848","0854554555","0854554555","0819982992","0818219990","0818643334","0898562225","0849098448","0849098448","0818263339","0818773335","0892541313","0870883663","0861397020","0817833338","0818423429","0854882129","0818662424","0818223334","0818216000"]) 
#	return (contI)

def randomContactIn(contI):  
    contIn = ""
    for contIn in range(1):
        contIn += random.randint(1,101)
        contI = contIn
    return contI

#==================randomContactEx=================#

#def randomContactEx(contE):  
#   contE = random.choice(["0987654321","0987654323","0987632444","0833333333","0987654321","0999999999","0987654321","0987654321","0987654321","0935870200","0896754345","0987654321","0987654321","0987654321","0987654324","0987654324","0987654321","0815484188","0980000000","0888888888","0934585144","0890672209","0850424570","0822224123","0890424595","0846816844","0897800570","0636254565","0954919294","0639699997","0846424289","0924628809","0613919997","0804197971","0900000099","0978784234","0861636397","0926237636","0927022994","0926236121"]) 
#  return (contE)

def randomContactEx(contE):  
    contExt = ""
    for contExt in range(1):
        contExt += random.randint(1,101)
        contE = contExt
    return contE

#==================randomRole=================#
def randomRole(role):  
    #CH_OWNER = '//*[@id="OWNER"]'
    CH_ACCTAX = '//*[@id="ACC_TAX"]'
    CH_ACCONLINE = '//*[@id="ACC_ONLINE"]'
    CH_FINANCE = '//*[@id="FINANCE"]'
    CH_OFFICER = '//*[@id="OFFICER"]'
    role = random.choice([CH_ACCTAX, CH_ACCONLINE, CH_FINANCE, CH_OFFICER]) #CH_OWNER,
    return (role)

#==================randomVataddr=================#
def randomVataddr(vat):  
    VA_ADD1 = '//*[@id="AWN_No"]'
    VA_ADD2 = '//*[@id="AIS_No"]'
    VA_ADD3 = '//*[@id="AMP_No"]'
    VA_ADD4 = '//*[@id="WDS_No"]'
    VA_ADD5 = '//*[@id="DPC_No"]'
    VA_ADD6 = '//*[@id="AIR_No"]'
    VA_ADD7 = '//*[@id="SBN_No"]'
    VA_ADD8 = '//*[@id="ACC_No"]'
    VA_ADD9 = '//*[@id="MMT_No"]'
    vat = random.choice([VA_ADD1, VA_ADD2, VA_ADD3, VA_ADD4, VA_ADD5, VA_ADD6, VA_ADD7, VA_ADD8, VA_ADD9]) 
    return (vat)

#==================randomAddr=================#
def randomAddr(numberx):
    numberxx = ""
    for numberxx in range(1):
        numberxx += random.randint(1,101)
        numberx = numberxx
    return numberx

#print randomAddr(1)