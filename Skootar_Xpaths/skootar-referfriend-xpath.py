﻿# This Python file uses the following encoding: utf-8
import os, sys, random, toeyTest

USERGROUP = 'DEFAULT' # random.choice(['DEFAULT','2019_NormalWP20','NormalWalk','45NewSkootar','DEFAULT','NewNormal','P9PreSurvey'])
SERVICE = toeyTest.randomService(USERGROUP)
OPTION = toeyTest.chooseOption(USERGROUP)
MAPOPTION = toeyTest.mapOption(OPTION)
TOTALDISTANCE= str ('//div[@class="_2I0Z"][4]/div[@class="_29wu"]')
query_set_usergroup = 'update SKOOTAR_DEV.tbl_user set user_group_id="'+str(USERGROUP)+'" where user_name = "test@mail.com";'
query_price = 'SELECT customer_fee FROM SKOOTAR_DEV.tbl_price_list_v2 where user_group_id = "'+str(USERGROUP)+'" and km={distance} and delivery_type='+str(SERVICE)+';'
query_price_option = 'SELECT customer_fee FROM SKOOTAR_DEV.tbl_price_option_v2 where user_group_id = "'+str(USERGROUP)+'" and delivery_type='+str(SERVICE)+' and job_option='+str(MAPOPTION)+';'
P_LOADER   = '//*[contains(@class, \'modal-open\')]'
LOG_IN = '//*[@id="gatsby-focus-wrapper"]//li/a[@href="th/login"]'
LOGI_FB = '//div[6]/button/div/div'
MAIL = '//input[@name="email"]'
PAS_WORD = '//input[@name="password"]'
F_EMAIL = '//*[@id="email"]'
F_PASSWORD = '//*[@id="pass"]'
LOGIN_EMAIL = '//button[@name="btnLogin"]'
LOGIN_FB = '//*[@id="u_0_0"]'
LOGO = '//div[3]/div/div/div/span[@class="_2wc-"]/div/div'
Icon1 = '//div[1]/div/span/div/div/span'
Icon2 = '//div[2]/div/span/div/div/span'
Icon3 = '//div[3]/div/span/div/div/span'
Icon4 = '//div[4]/div/span/div/div/span'
Icon5 = '//div[5]/div/span/div/div'
Icon6 = '//div[6]/div/span/div/div/span'
Icon7 = '//div[7]/div/span/div/div/span'
Icon8 = '//div[8]/div/span/div/div/span' 
Icon9 = '//div[9]/div/span/div/div/span'
Icon10 = '//div[@role="menu"]'
TAB_REFER_FRIEND = '//div/span[@class="skt-icons mn-share-friends"]'
COPY_LINK = '//div[@class="cop"]/button'
#query_prepaid_code = 'select prepaid_code from SKOOTAR_DEV.tbl_prepaid_code where prepaid_code_status = 1 and created_by = "NT" LIMIT 1;'
set_phone_nouse1 = 'update SKOOTAR_DEV.tbl_user set phone = "0800000000" where last_name = "referee";'
set_phone_nouse2 = 'update SKOOTAR_DEV.tbl_user set phone = "0800000000" where email = "test@mail.com";'
set_phone_nouse3 = 'update SKOOTAR_DEV.tbl_user set phone = "0800000000" where email = "namtoey16@gmail.com";'
clear_referee = 'delete from tbl_user where last_name = "referee";'
clear_credit_referer = 'update SKOOTAR_DEV.tbl_user set free_credit = "0" where email = "test@mail.com";'
input_prepaid_code = '//div[@class="_24nL"]//input[@type="text"]'
Signup_Refer = '//div[@class="_3Sb0"]/div/button'
Signup_FbEmail = '//div[@class="_1lfq"]/button'
Signup_Mobile = '//input[@name="phone"]'
Signup = '//button[@name="btnSignup"]'
input_otp = '//*[@type="tel"]'
F_NAME = '//input[@name="firstName"]'
L_NAME = '//input[@name="lastName"]'
SETUP_PROFILE = '//*[@id="main-content"]/div/div[1]/div/div[1]/div/div[2]/div[3]/button'
Copycode = '//div[@class="_2aji"]/button' 
Add1stplace = '//*[@id="main-content"]//div[1]/div[2]/div[2]/button/div'
NowLocation = '//*[@id="main-content"]/div/div[1]/div/div[1]/div/div/div[2]/div[2]/div/div[1]/div/button/div/div/div'
NAM_ORG = '//input[@name="contactName"]'
PHON_ORG = '//input[@name="contactPhone"]'
Add2ndplace = '//*[@id="main-content"]//div[2]/div[2]/div[2]/button/div'
Add3rdplace = '//*[@id="main-content"]/div/div[1]/div/div[1]/div/div/div/div[2]/div/div[1]/div/div/div[1]/div[1]/div[2]/button'
NAM_DES = '//input[@name="contactName"]'
PHON_DES = '//input[@name="contactPhone"]'
SELECT_SERVICE = '//*[@id="main-content"]/div/div[1]/div/div[1]/div/div/div/div[2]/div/div[1]/div/div[1]/div/div['+str(SERVICE)+']/button/div/div/div/div'
SELECT_OPTION = '//*[@id="main-content"]/div/div[1]/div/div[1]/div/div/div/div[2]/div/div[1]/div/div[2]/div/div/div['+str(OPTION)+']/span'
CHK_NETPRICE = "//div[@class='amAj']/div[@class='_29wu']"
CHK_PRICE = "//div[@class='_2I0Z'][1]/div[@class='_29wu']"
CHK_DISCOUNT = "//div[@class='_2I0Z'][2]/div[@class='_29wu']"
CHK_FREECREDIT = "//div[@class='_2I0Z'][3]/div[@class='_29wu']"
#PRICE = str(toeyTest.calPrice(SERVICE,OPTION))
#PRICE_USERGROUP = str(toeyTest.calPrice_UserGroup(SERVICE,OPTION,USERGROUP))
PRICEADD1POINT = str(toeyTest.calPriceAdd1Point(SERVICE,OPTION))
PRICEADD2POINT = str(toeyTest.calPriceAdd2Point(SERVICE,OPTION))
NEXT_TO_STEP2 = '//*[@id="main-content"]//div[3]/div[2]/div/div/button'
NEXT_TO_STEP3 = '//*[@id="main-content"]//div[2]/div/div/div[2]/button'
PAYMENT = '//*[@id="main-content"]//div[2]/span'
CASH = '//*[@id="main-content"]/div/div[1]/div/div[1]/div/div/div/div[1]/div[1]/div/div[2]/div[1]/span/div/div/div[1]/input'
CREDIT_CARD = '//*[@id="main-content"]/div/div[1]/div/div[1]/div/div/div/div[1]/div[1]/div/div[2]/div[2]/span/div/div/div[1]/input'
WALLET = '//*[@id="main-content"]/div/div[1]/div/div[1]/div/div/div/div[1]/div[1]/div/div[2]/div[3]/span/div/div/div[1]/input'
PROMPTPAY = '//*[@id="main-content"]/div/div[1]/div/div[1]/div/div/div/div[1]/div[1]/div/div[2]/div[4]/span/div/div/div[1]/input'
INVOICE = '//*[@id="main-content"]/div/div[1]/div/div[1]/div/div/div/div[1]/div[1]/div/div[2]/div[5]/span/div/div/div[1]/input'
PAYMENT_ADD_CREDITCARD = '//*[@class="_3vhF"]'
REM = '//div/textarea[@maxlength="200"]'
TIME_CF = '//*[@id="timeConfirm"]/div/div[2]/div[1]/span/div/div/div[1]'
PAY_POINT = '//*[@id="pointConfirm"]/div/div[2]/div[1]'
CREATE_JOB = '//*[@id="main-content"]//div[2]/button'
CHK_NEEDRECEIPT = '//div[@id="needReceiptForm"]/div[@class="_3fxY"]'
CONFIRM = '//div[@class="_3tiA _1xNa"]/div/button'
CREDIT_REFEREE = 'select free_credit from tbl_user where last_name = "referee" and free_credit = "100";'
CREDIT_REFER_BEFORE = 'select free_credit from tbl_user where user_name = "test@mail.com";'
CREDIT_REFER_AFTER = 'select free_credit from tbl_user where user_name = "test@mail.com";'

print  ("user : ", USERGROUP)
print ("sevice = ", SERVICE)
print ("option = ", OPTION)
print ("*********************************************")