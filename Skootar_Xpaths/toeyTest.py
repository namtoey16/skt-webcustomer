﻿import os, sys
import random , math , datetime


#=========================================================#

def getDate(dateinvoice):
    today = datetime.date.today()  
    if dateinvoice == 0:
        dateinvoice = datetime.date.today()
    elif dateinvoice == 1:
        dateinvoice = today - datetime.timedelta(days = 1)
    elif dateinvoice == 2:
        dateinvoice = today - datetime.timedelta(days = 7)
    elif dateinvoice == 3:
        dateinvoice = today + datetime.timedelta(days = 3) 
    elif dateinvoice == 4:
        dateinvoice = today + datetime.timedelta(days = 8) 
    return dateinvoice

#=========================================================#

def randomService(service):  
    service = random.choice([1,2,3])  # 1 doc ,  2 food  3  parcel , 4 car
    return service

#=========================================================#

#def randomOption(option):  
#    option = random.choice([1,2,3,4,5])
#    return option

def chooseOption(UserGroup):
    if UserGroup == 'DEFAULT':
         option = random.choice([1,2,3,4])
    elif UserGroup == '2019_NormalWP20':
         option = random.choice([1,2,3,4,5])
    elif UserGroup == 'NormalWalk':
         option = random.choice([1,2,3,4])     
    return option

def chooseOptionNologin(USERGROUP_NOLOGIN):
    option = random.choice([1,2,3,4])     
    return option


def mapOption(option): #default (non login)
    if option == 1:
        mapoption = 1
    elif option == 2:
        mapoption = 2
    elif option == 3:
        mapoption = 4
    elif option == 4:
        mapoption = 6
    elif option == 5:
        mapoption = 10
    return mapoption          


def mapOptionCar(option): #default (non login)
    if option == 1:
        mapOptionCar = 4
    return mapOptionCar          


def mapOptionNologin(option): #default (non login)
    if option == 1:
        mapoption = 1
    elif option == 2:
        mapoption = 2
    elif option == 3:
        mapoption = 4
    elif option == 4:
        mapoption = 10
    elif option == 5:
        mapoption = 66
    elif option == 5:
        mapoption = 77    
    return mapoption       
#=========================================================#

def calPrice(S, O): #default (non login)
    if S == 1:
        price = 70
        if O == 1:  #เดินเอกสาร #นับจาก index ของ option ใน web cust.
            price += 0
        elif O == 2:  #นำเงินเข้าบัญชี
            price += 40
        elif O == 3:  #เก็บเงินปลายทางและนำไปส่ง
            price = price + (price * 0.50)
        elif O == 4:  #ต้องการกล่องใส่อาหาร
            price += 0
        elif O == 5:  #เทส dynamic job option
            price += 1
        elif O == 6:  #เทส dynamic job option2
            price += 11
        elif O == 0:  #ไม่เลือกมา
            price += 0
        
    elif S == 2:
        price = 70
        if O == 1:
            price += 0
        elif O == 2:
            price += 40
        elif O == 3:
            price = price + (price * 0.50)
        elif O == 4:
            price += 0
        elif O == 5:  #เทส dynamic job option
            price += 5
        elif O == 6:  #เทส dynamic job option2
            price += 20
        elif O == 0:
            price += 0

    elif S == 3:
        price = 70
        if O == 1:
            price += 0
        elif O == 2:
            price += 40
        elif O == 3:
            price = price + (price * 0.50)
        elif O == 4:
            price += 0
        elif O == 5:  #เทส dynamic job option
            price += 20
        elif O == 6:  #เทส dynamic job option2
            price += 30
        elif O == 0:
            price += 0

    elif S == 4:
        price = 70
        if O == 1: # เก็บเงินปลายทางแล้วนำมาส่ง
            price = price + (price * 0.50)
        elif O == 0:
            price += 0

    return int (math.ceil(price))   

#=========================================================#

def calPrice_UserGroup(S,O,UserGroup):
    if UserGroup == 'DEFAULT':
        if S == 1:
            price = 70
            if O == 1:  #เดินเอกสาร #นับจาก index ของ option ใน web cust.
               price += 0
            elif O == 2:  #นำเงินเข้าบัญชี
               price += 40
            elif O == 3:  #เก็บเงินปลายทางและนำไปส่ง
               price = price + (price * 0.50)
            elif O == 4:  #ต้องการกล่องใส่อาหาร
               price += 0
            elif O == 5:  #เทส dynamic job option
               price += 1
            elif O == 6:  #เทส dynamic job option2
               price += 11
            elif O == 0:
                price += 0
        
        elif S == 2:
            price = 70
            if O == 1:
                price += 0
            elif O == 2:
                price += 40
            elif O == 3:
                price = price + (price * 0.50)
            elif O == 4:
                price += 0
            elif O == 5:  #เทส dynamic job option
                price += 5
            elif O == 6:  #เทส dynamic job option2
                price += 20
            elif O == 0:
                price += 0
        
        elif S == 3:
            price = 70
            if O == 1:
                price += 0
            elif O == 2:
                price += 40
            elif O == 3:
                price = price + (price * 0.50)
            elif O == 4:
                price += 0
            elif O == 5:  #เทส dynamic job option
                price += 20
            elif O == 6:  #เทส dynamic job option2
                price += 30
            elif O == 0:
                price += 0
        
        elif S == 4:
            price = 70
            if O == 1:
                price = price + (price * 0.50)
            elif O == 0:  #ไม่เลือกมา
                price += 0
        

    elif UserGroup == '2019_NormalWP20':
        if S == 1:
            price = 65
            if O == 1: 
                price += 15
            elif O == 2:
                price += 40
            elif O == 3:
                price = price + (price * 0.50)
            elif O == 4:
                price += 0
            elif O == 5:
                price += 0
            elif O == 0:  #ไม่เลือกมา
                price += 0
                
        elif S == 2:
            price = 65
            if O == 1: 
                price += 15
            elif O == 2:
                price += 40
            elif O == 3:
                price = price + (price * 0.50)
            elif O == 4:
                price += 0
            elif O == 5:
                price += 0
            elif O == 0:  #ไม่เลือกมา
                price += 0
        
        elif S == 3:
            price = 65
            if O == 1: 
                price += 15
            elif O == 2:
                price += 40
            elif O == 3:
                price = price + (price * 0.50)
            elif O == 4:
                price += 0
            elif O == 5:
                price += 0
            elif O == 0:  #ไม่เลือกมา
                price += 0
        

    elif UserGroup == 'NormalWalk':
        if S == 1:
            price = 70
            if O == 1: 
                price += 0
            elif O == 2:
                price += 40
            elif O == 3:
                price = price + (price * 0.50)
            elif O == 4:
                price += 0
            elif O == 0:  #ไม่เลือกมา
                price += 0
        
        elif S == 2:
            price = 70
            if O == 1: 
                price += 0
            elif O == 2:
                price += 40
            elif O == 3:
                price = price + (price * 0.50)
            elif O == 4:
                price  += 0
            elif O == 0:  #ไม่เลือกมา
                price += 0

        elif S == 3:
            price = 70
            if O == 1: 
                price += 0
            elif O == 2:
                price += 40
            elif O == 3:
                price = price + (price * 0.50)
            elif O == 4:
                price += 0
            elif O == 0:  #ไม่เลือกมา
                price += 0


    elif UserGroup == '45skootar':
        if S == 1:
            price = 65
            if O == 1: 
                price += 15
            elif O == 2:
                price += 40
            elif O == 3:
                price = price + (price * 0.50)
            elif O == 4:
                price += 0
            elif O == 0:  #ไม่เลือกมา
                price += 0
        
        elif S == 2:
            price = 65
            if O == 1: 
                price += 15
            elif O == 2:
                price += 40
            elif O == 3:
                price = price + (price * 0.50)
            elif O == 4:
                price += 0
            elif O == 0:  #ไม่เลือกมา
                price += 0

        elif S == 3:
            price = 65
            if O == 1: 
                price += 15
            elif O == 2:
                price += 40
            elif O == 3:
                price = price + (price * 0.50)
            elif O == 4:
                price += 0
            elif O == 0:  #ไม่เลือกมา
                price += 0

    elif UserGroup == '201912_NormalFriend':
        if S == 1:
            price = 59.83
            if O == 1: 
                price += 14
            elif O == 2:
                price += 38
            elif O == 3:
                price = price + (price * 0.50)
            elif O == 0:  #ไม่เลือกมา
                price += 0
        
        elif S == 2:
            price = 59.83
            if O == 1: 
                price += 14
            elif O == 2:
                price += 38
            elif O == 3:
                price = price + (price * 0.50)
            elif O == 4:
                price += 0
            elif O == 0:  #ไม่เลือกมา
                price += 0

        elif S == 3:
            price = 59.83
            if O == 1: 
                price += 14
            elif O == 2:
                price += 38
            elif O == 3:
                price = price + (price * 0.50)
            elif O == 4:
                price += 0
            elif O == 0:  #ไม่เลือกมา
                price += 0


    elif UserGroup == 'P9Survey':
        if S == 1:
            price = 360
            if O == 1: 
                price += 0
            elif O == 2:
                price += 40
            elif O == 3:
                price = price + (price * 0.50)
            elif O == 4:
                price += 0
            elif O == 0:  #ไม่เลือกมา
                price += 0
        
        elif S == 2:
            price = 360
            if O == 1: 
                price += 0
            elif O == 2:
                price += 40
            elif O == 3:
                price = price + (price * 0.50)
            elif O == 4:
                price += 0
            elif O == 0:  #ไม่เลือกมา
                price += 0

        elif S == 3:
            price = 360
            if O == 1: 
                price += 0
            elif O == 2:
                price += 40
            elif O == 3:
                price = price + (price * 0.50)
            elif O == 4:
                price += 0
            elif O == 0:  #ไม่เลือกมา
                price += 0

    elif UserGroup == '20210120_Robinhood': # Robinhood 2021
        if S == 1:
            price = 49 
            if O == 1: # นำเงิน/เช็คเข้าบัญชี
                price += 55
            elif O == 2: # ต้องการกล่องใส่ของ/อาหาร 
                price += 0
            elif O == 0:  #ไม่เลือกมา
                price += 0
        
        elif S == 2:
            price = 49
            if O == 1:
                price += 51
            elif O == 2:
                price += 0
            elif O == 0:  #ไม่เลือกมา
                price += 0

        elif S == 3:
            price = 49
            if O == 1: 
                price += 41
            elif O == 2:
                price += 0
            elif O == 0:  #ไม่เลือกมา
                price += 0

    return int (math.ceil(price))   

#=========================================================#

def extraPrice(extraDistanse): # user_group_id = "DEFAULT
    extraDistanse *= 10
    return int (math.ceil(extraDistanse))

def extraPrice_USERGROUP(extraDistanse_UserGroup, UserGroup): # 
    if UserGroup == '20210120_Robinhood':
        extraDistanse_UserGroup *= 14
    return int (math.ceil(extraDistanse_UserGroup))  
    
#=========================================================#

def priceOver120km(S,O,E): # DEFAULT - no login
    E *= 10
    if S == 1:
        price = 1500
        if O == 1:  #เดินเอกสาร
            price += ( 0 + E ) 
        elif O == 2:  #นำเงินเข้าบัญชี
            price += ( 40 + E ) 
        elif O == 3:  #เก็บเงินปลายทางและนำไปส่ง
            price = price + ( 200 ) + E # (price * 0.50) => MAX=200
        elif O == 4:  #ต้องการกล่องใส่อาหาร
            price += ( 0 + E ) 
        elif O == 5: #เทส dynamic job option
            price += ( 1 + E )
        elif O == 6: #เทส dynamic job option
            price += ( 11 + E )
        elif O == 0: 
            price += ( 0 + E )

        
    elif S == 2:
        price = 1500
        if O == 1:  #เดินเอกสาร
            price += ( 0 + E ) 
        elif O == 2:  #นำเงินเข้าบัญชี
            price += ( 40 + E ) 
        elif O == 3:  #เก็บเงินปลายทางและนำไปส่ง
            price = price + ( 200 ) + E # (price * 0.50) => MAX=200
        elif O == 4:  #ต้องการกล่องใส่อาหาร
            price += ( 0 + E ) 
        elif O == 5: #เทส dynamic job option
            price += ( 5 + E )
        elif O == 6: #เทส dynamic job option
            price += ( 20 + E )
        elif O == 0: 
            price += ( 0 + E )


    elif S == 3:
        price = 1500
        if O == 1:  #เดินเอกสาร
            price += ( 0 + E ) 
        elif O == 2:  #นำเงินเข้าบัญชี
            price += ( 40 + E ) 
        elif O == 3:  #เก็บเงินปลายทางและนำไปส่ง
            price = price + ( 200 ) + E # (price * 0.50) => MAX=200
        elif O == 4:  #ต้องการกล่องใส่อาหาร
            price += ( 0 + E ) 
        elif O == 5: #เทส dynamic job option
            price += ( 20 + E )
        elif O == 6: #เทส dynamic job option
            price += ( 30 + E )
        elif O == 0:
            price += ( 0 + E )


    elif S == 4:
        price = 1500
        if O == 1:  #เก็บเงินปลายทางและนำไปส่ง
            price = price + ( 200 ) + E # (price * 0.50) => MAX=200
        elif O == 0:
            price += ( 0 + E )

    return int (math.ceil(price))   


#=========================================================#

def priceOver120km_UserGroup(S,O,UserGroup,E):
    # E *= 10
    if UserGroup == 'DEFAULT':
        E *= 10
        if S == 1:
            price = 1500
            if O == 1:  #เดินเอกสาร
                price += ( 0 + E ) 
            elif O == 2:  #นำเงินเข้าบัญชี
                price += ( 40 + E ) 
            elif O == 3:  #เก็บเงินปลายทางและนำไปส่ง
                price = price + ( 200 ) + E # (price * 0.50) => MAX=200
            elif O == 4:  #ต้องการกล่องใส่อาหาร
                price += ( 0 + E ) 
            elif O == 5: #เทส dynamic job option
                price += ( 1 + E )
            elif O == 6: #เทส dynamic job option
                price += ( 11 + E )
            elif O == 0: 
                price += ( 0 + E )
        
        elif S == 2:
            price = 1500
            if O == 1:  #เดินเอกสาร
                price += ( 0 + E ) 
            elif O == 2:  #นำเงินเข้าบัญชี
                price += ( 40 + E ) 
            elif O == 3:  #เก็บเงินปลายทางและนำไปส่ง
                price = price + ( 200 ) + E # (price * 0.50) => MAX=200
            elif O == 4:  #ต้องการกล่องใส่อาหาร
                price += ( 0 + E ) 
            elif O == 5: #เทส dynamic job option
                price += ( 5 + E )
            elif O == 6: #เทส dynamic job option
                price += ( 20 + E )
            elif O == 0: 
                price += ( 0 + E )
        
        elif S == 3:
            price = 1500
            if O == 1:  #เดินเอกสาร
                price += ( 0 + E ) 
            elif O == 2:  #นำเงินเข้าบัญชี
                price += ( 40 + E ) 
            elif O == 3:  #เก็บเงินปลายทางและนำไปส่ง
                price = price + ( 200 ) + E # (price * 0.50) => MAX=200
            elif O == 4:  #ต้องการกล่องใส่อาหาร
                price += ( 0 + E ) 
            elif O == 5: #เทส dynamic job option
                price += ( 20 + E )
            elif O == 6: #เทส dynamic job option
                price += ( 30 + E )
            elif O == 0:
                price += ( 0 + E )

        elif S == 4:
            price = 1500
            if O == 1:  #เก็บเงินปลายทางและนำไปส่ง
                price = price + ( 200 ) + E # (price * 0.50) => MAX=200
            elif O == 0:
                price += ( 0 + E )


    elif UserGroup == '2019_NormalWP20':
        E *= 10
        if S == 1:
            price = 1332.50
            if O == 1:  #เดินเอกสาร
                price += ( 15 + E )
            elif O == 2:  #นำเงินเข้าบัญชี
                price += ( 40 + E )
            elif O == 3:  #เก็บเงินปลายทางและนำไปส่ง
                price = price + ( 200 ) + E
            elif O == 4:  #ส่งไปรษณีย์
                price += ( 0 + E )
            elif O == 5:  #ต้องการกล่องใส่อาหาร
                price += ( 0 + E )
            elif O == 0:  #ไม่เลือกมา
                price += ( 0 + E )
                
        elif S == 2:
            price = 1332.50
            if O == 1:  #เดินเอกสาร
                price += ( 15 + E )
            elif O == 2:  #นำเงินเข้าบัญชี
                price += ( 40 + E )
            elif O == 3:  #เก็บเงินปลายทางและนำไปส่ง
                price = price + ( 200 ) + E
            elif O == 4:  #ส่งไปรษณีย์
                price += ( 0 + E )
            elif O == 5:  #ต้องการกล่องใส่อาหาร
                price += ( 0 + E )
            elif O == 0:  #ไม่เลือกมา
                price += ( 0 + E )
        
        elif S == 3:
            price = 1332.50
            if O == 1:  #เดินเอกสาร
                price += ( 15 + E )
            elif O == 2:  #นำเงินเข้าบัญชี
                price += ( 40 + E )
            elif O == 3:  #เก็บเงินปลายทางและนำไปส่ง
                price = price + ( 200 ) + E
            elif O == 4:  #ส่งไปรษณีย์
                price += ( 0 + E )
            elif O == 5:  #ต้องการกล่องใส่อาหาร
                price += ( 0 + E )
            elif O == 0:  #ไม่เลือกมา
                price += ( 0 + E )
        

    elif UserGroup == 'NormalWalk':
        E *= 10
        if S == 1:
            price = 1500
            if O == 1:  #เดินเอกสาร
                price += ( 0 + E )
            elif O == 2:  #นำเงินเข้าบัญชี
                price += ( 40 + E )
            elif O == 3:  #เก็บเงินปลายทางและนำไปส่ง
                price = price + ( 200 ) + E
            elif O == 4:  #ส่งไปรษณีย์
                price += ( 0 + E )
            elif O == 0:  #ไม่เลือกมา
                price += ( 0 + E )
        
        elif S == 2:
            price = 1500
            if O == 1:  #เดินเอกสาร
                price += ( 0 + E )
            elif O == 2:  #นำเงินเข้าบัญชี
                price += ( 40 + E )
            elif O == 3:  #เก็บเงินปลายทางและนำไปส่ง
                price = price + ( 200 ) + E
            elif O == 4:  #ส่งไปรษณีย์
                price += ( 0 + E )
            elif O == 0:  #ไม่เลือกมา
                price += ( 0 + E )

        elif S == 3:
            price = 1500
            if O == 1:  #เดินเอกสาร
                price += ( 0 + E )
            elif O == 2:  #นำเงินเข้าบัญชี
                price += ( 40 + E )
            elif O == 3:  #เก็บเงินปลายทางและนำไปส่ง
                price = price + ( 200 ) + E
            elif O == 4:  #ส่งไปรษณีย์
                price += ( 0 + E )
            elif O == 0:  #ไม่เลือกมา
                price += ( 0 + E )


    elif UserGroup == '45skootar':
        E *= 10
        if S == 1:
            price = 1360
            if O == 1:  #เดินเอกสาร
                price += ( 15 + E )
            elif O == 2:  #นำเงินเข้าบัญชี
                price += ( 40 + E )
            elif O == 3:  #เก็บเงินปลายทางและนำไปส่ง
                price = price + ( 200 ) + E
            elif O == 4:  #ส่งไปรษณีย์
                price += ( 0 + E )
            elif O == 0:  #ไม่เลือกมา
                price += ( 0 + E )
        
        elif S == 2:
            price = 1360
            if O == 1:  #เดินเอกสาร
                price += ( 15 + E )
            elif O == 2:  #นำเงินเข้าบัญชี
                price += ( 40 + E )
            elif O == 3:  #เก็บเงินปลายทางและนำไปส่ง
                price = price + ( 200 ) + E
            elif O == 4:  #ส่งไปรษณีย์
                price += ( 0 + E )
            elif O == 0:  #ไม่เลือกมา
                price += ( 0 + E )

        elif S == 3:
            price = 1360
            if O == 1:  #เดินเอกสาร
                price += ( 15 + E )
            elif O == 2:  #นำเงินเข้าบัญชี
                price += ( 40 + E )
            elif O == 3:  #เก็บเงินปลายทางและนำไปส่ง
                price = price + ( 200 ) + E
            elif O == 4:  #ส่งไปรษณีย์
                price += ( 0 + E )
            elif O == 0:  #ไม่เลือกมา
                price += ( 0 + E )

    elif UserGroup == '201912_NormalFriend':
        E *= 9
        if S == 1:
            price = 1226.46
            if O == 1:  #เดินเอกสาร
                price += ( 14 + E )
            elif O == 2:  #นำเงินเข้าบัญชี
                price += ( 38 + E )
            elif O == 3:  #เก็บเงินปลายทางและนำไปส่ง
                price = price + ( 200 ) + E
            elif O == 0:  #ไม่เลือกมา
                price += ( 0 + E )
        
        elif S == 2:
            price = 1226.46
            if O == 1:  #เดินเอกสาร
                price += ( 14 + E )
            elif O == 2:  #นำเงินเข้าบัญชี
                price += ( 38 + E )
            elif O == 3:  #เก็บเงินปลายทางและนำไปส่ง
                price = price + ( 200 ) + E
            elif O == 4:  #ส่งไปรษณีย์
                price += ( 0 + E )
            elif O == 0:  #ไม่เลือกมา
                price += ( 0 + E )

        elif S == 3:
            price = 1226.46
            if O == 1:  #เดินเอกสาร
                price += ( 14 + E )
            elif O == 2:  #นำเงินเข้าบัญชี
                price += ( 38 + E )
            elif O == 3:  #เก็บเงินปลายทางและนำไปส่ง
                price = price + ( 200 ) + E
            elif O == 4:  #ส่งไปรษณีย์
                price += ( 0 + E )
            elif O == 0:  #ไม่เลือกมา
                price += ( 0 + E )


    elif UserGroup == 'P9Survey':
        E *= 10
        if S == 1:
            price = 360
            if O == 1:  #เดินเอกสาร
                price += ( 0 + E )
            elif O == 2:  #นำเงินเข้าบัญชี
                price += ( 40 + E )
            elif O == 3:  #เก็บเงินปลายทางและนำไปส่ง
                price = price + ( 200 ) + E
            elif O == 4:  #ส่งไปรษณีย์
                price += ( 0 + E )
            elif O == 0:  #ไม่เลือกมา
                price += ( 0 + E )
        
        elif S == 2:
            price = 360
            if O == 1:  #เดินเอกสาร
                price += ( 0 + E )
            elif O == 2:  #นำเงินเข้าบัญชี
                price += ( 40 + E )
            elif O == 3:  #เก็บเงินปลายทางและนำไปส่ง
                price = price + ( 200 ) + E
            elif O == 4:  #ส่งไปรษณีย์
                price += ( 0 + E )
            elif O == 0:  #ไม่เลือกมา
                price += ( 0 + E )

        elif S == 3:
            price = 360
            if O == 1:  #เดินเอกสาร
                price += ( 0 + E )
            elif O == 2:  #นำเงินเข้าบัญชี
                price += ( 40 + E )
            elif O == 3:  #เก็บเงินปลายทางและนำไปส่ง
                price = price + ( 200 ) + E
            elif O == 4:  #ส่งไปรษณีย์
                price += ( 0 + E )
            elif O == 0:  #ไม่เลือกมา
                price += ( 0 + E )
        
    elif UserGroup == '20210120_Robinhood':
        E *= 14
        if S == 1:
            price = 1578
            if O == 1:  #นำเงิน/เช็คเข้าบัญชี
                price += ( 55 + E )
            elif O == 2:  #ต้องการกล่องใส่ของ/อาหาร 
                price += ( 0 + E )
            elif O == 0:  #ไม่เลือกมา
                price += ( 0 + E )
        
        elif S == 2:
            price = 1578
            if O == 1:  
                price += ( 51 + E )
            elif O == 2:  
                price += ( 0 + E )
            elif O == 0:  #ไม่เลือกมา
                price += ( 0 + E )

        elif S == 3:
            price = 1578
            if O == 1: 
                price += ( 41 + E )
            elif O == 2: 
                price += ( 0 + E )
            elif O == 0:  #ไม่เลือกมา
                price += ( 0 + E )

    return int (math.ceil(price))   


#=========================================================#

def calPriceAdd1Point(S,O): #DEFAULT
    if S == 1:
        begin_price = 70
        price_waypoint = 40
        price_2waypoint = 40*2
        if O == 1: 
            price = begin_price + ( price_waypoint + 0 )
        elif O == 2:
            price = begin_price + ( price_waypoint + 40 )
        elif O == 3:
            price = ( begin_price + (begin_price * 0.50) ) + price_waypoint
        elif O == 4:
            price = begin_price + ( price_waypoint + 0 )
        elif O == 5:
            price = begin_price + ( price_waypoint + 1 )
        elif O == 6:
            price = begin_price + ( price_waypoint + 11 )
        elif O == 0:
            price = begin_price + ( price_waypoint + 0 )

    elif S == 2:
        begin_price = 70
        price_waypoint = 40
        price_2waypoint = 40*2
        if O == 1: 
            price = begin_price + ( price_waypoint + 0 )
        elif O == 2:
            price = begin_price + ( price_waypoint + 40 )
        elif O == 3:
            price = ( begin_price + (begin_price * 0.50) ) + price_waypoint
        elif O == 4:
            price = begin_price + ( price_waypoint + 0 )
        elif O == 5:
            price = begin_price + ( price_waypoint + 5 )
        elif O == 6:
            price = begin_price + ( price_waypoint + 20 )
        elif O == 0:
            price = begin_price + ( price_waypoint + 0 )

    elif S == 3:
        begin_price = 70
        price_waypoint = 40
        price_2waypoint = 40*2
        if O == 1: 
            price = begin_price + ( price_waypoint + 0 )
        elif O == 2:
            price = begin_price + ( price_waypoint + 40 )
        elif O == 3:
            price = ( begin_price + (begin_price * 0.50) ) + price_waypoint
        elif O == 4:
            price = begin_price + ( price_waypoint + 0 )
        elif O == 5:
            price = begin_price + ( price_waypoint + 20 )
        elif O == 6:
            price = begin_price + ( price_waypoint + 30 )
        elif O == 0:
            price = begin_price + ( price_waypoint + 0 )

    elif S == 4:
        begin_price = 70
        price_waypoint = 40
        price_2waypoint = 40*2
        if O == 1:
            price = ( begin_price + (begin_price * 0.50) ) + price_waypoint
        elif O == 0:
            price = begin_price + ( price_waypoint + 0 )

    return int (math.ceil(price))   

#=========================================================#

def calPriceAdd2Point(S,O): #DEFAULT
    if S == 1:
        begin_price = 70
        price_waypoint = 40
        price_2waypoint = 40*2
        if O == 1: 
            price = begin_price + ( price_2waypoint + 0 )
        elif O == 2:
            price = begin_price + ( price_2waypoint + 40 )
        elif O == 3:
            price = ( begin_price + (begin_price * 0.50) ) + price_2waypoint
        elif O == 4:
            price = begin_price + ( price_2waypoint + 0 )
        elif O == 5:
            price = begin_price + ( price_2waypoint + 1 )
        elif O == 6:
            price = begin_price + ( price_2waypoint + 11 )
        elif O == 0:
            price = begin_price + ( price_2waypoint + 0 )
        
    elif S == 2:
        begin_price = 70
        price_waypoint = 40
        price_2waypoint = 40*2
        if O == 1: 
            price = begin_price + ( price_2waypoint + 0 )
        elif O == 2:
            price = begin_price + ( price_2waypoint + 40 )
        elif O == 3:
            price = ( begin_price + (begin_price * 0.50) ) + price_2waypoint
        elif O == 4:
            price = begin_price + ( price_2waypoint + 0 )
        elif O == 5:
            price = begin_price + ( price_2waypoint + 5 )
        elif O == 6:
            price = begin_price + ( price_2waypoint + 20 )
        elif O == 0:
            price = begin_price + ( price_2waypoint + 0 )

    elif S == 3:
        begin_price = 70
        price_waypoint = 40
        price_2waypoint = 40*2
        if O == 1: 
            price = begin_price + ( price_2waypoint + 0 )
        elif O == 2:
            price = begin_price + ( price_2waypoint + 40 )
        elif O == 3:
            price = ( begin_price + (begin_price * 0.50) ) + price_2waypoint
        elif O == 4:
            price = begin_price + ( price_2waypoint + 0 )
        elif O == 5:
            price = begin_price + ( price_2waypoint + 20 )
        elif O == 6:
            price = begin_price + ( price_2waypoint + 30 )
        elif O == 0:
            price = begin_price + ( price_2waypoint + 0 )

    elif S == 4:
        begin_price = 70
        price_waypoint = 40
        price_2waypoint = 40*2
        if O == 1:
            price = ( begin_price + (begin_price * 0.50) ) + price_2waypoint
        elif O == 0:
            price = begin_price + ( price_2waypoint + 0 )

    return int (math.ceil(price))   

#=========================================================#

def calPriceAdd1Point_UserGroup(S,O,UserGroup):
    if UserGroup == 'DEFAULT':
        if S == 1:
            begin_price = 70
            price_waypoint = 40
            price_2waypoint = 40*2
            if O == 1: 
                price = begin_price + ( price_waypoint + 0 )
            elif O == 2:
                price = begin_price + ( price_waypoint + 40 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_waypoint
            elif O == 4:
                price = begin_price + ( price_waypoint + 0 )
            elif O == 5:
                price = begin_price + ( price_waypoint + 1 )
            elif O == 6:
                price = begin_price + ( price_waypoint + 11 )
            elif O == 0:
                price = begin_price + ( price_waypoint + 0 )
        
        elif S == 2:
            begin_price = 70
            price_waypoint = 40
            price_2waypoint = 40*2
            if O == 1: 
                price = begin_price + ( price_waypoint + 0 )
            elif O == 2:
                price = begin_price + ( price_waypoint + 40 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_waypoint
            elif O == 4:
                price = begin_price + ( price_waypoint + 0 )
            elif O == 5:
                price = begin_price + ( price_waypoint + 5 )
            elif O == 6:
                price = begin_price + ( price_waypoint + 20 )
            elif O == 0:
                price = begin_price + ( price_waypoint + 0 )

        elif S == 3:
            begin_price = 70
            price_waypoint = 40
            price_2waypoint = 40*2
            if O == 1: 
                price = begin_price + ( price_waypoint + 0 )
            elif O == 2:
                price = begin_price + ( price_waypoint + 40 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_waypoint
            elif O == 4:
                price = begin_price + ( price_waypoint + 0 )
            elif O == 5:
                price = begin_price + ( price_waypoint + 20 )
            elif O == 6:
                price = begin_price + ( price_waypoint + 30 )
            elif O == 0:
                price = begin_price + ( price_waypoint + 0 )

        elif S == 4:
            begin_price = 70
            price_waypoint = 40
            price_2waypoint = 40*2
            if O == 1:
                price = ( begin_price + (begin_price * 0.50) ) + price_waypoint
            elif O == 0:
                price = begin_price + ( price_waypoint + 0 )


    elif UserGroup == '2019_NormalWP20':
        if S == 1:
            begin_price = 65
            price_waypoint = 20
            price_2waypoint = 20*2
            if O == 1: 
                price = begin_price + ( price_waypoint + 15 )
            elif O == 2:
                price = begin_price + ( price_waypoint + 40 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_waypoint
            elif O == 4:
                price = begin_price + ( price_waypoint + 0 )
            elif O == 5:
                price = begin_price + ( price_waypoint + 0 )
            elif O == 0:
                price = begin_price + ( price_waypoint + 0 )
        
        elif S == 2:
            begin_price = 65
            price_waypoint = 20
            price_2waypoint = 20*2
            if O == 1: 
                price = begin_price + ( price_waypoint + 15 )
            elif O == 2:
                price = begin_price + ( price_waypoint + 40 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_waypoint
            elif O == 4:
                price = begin_price + ( price_waypoint + 0 )
            elif O == 5:
                price = begin_price + ( price_waypoint + 0 )
            elif O == 0:
                price = begin_price + ( price_waypoint + 0 )

        elif S == 3:
            begin_price = 65
            price_waypoint = 20
            price_2waypoint = 20*2
            if O == 1: 
                price = begin_price + ( price_waypoint + 15 )
            elif O == 2:
                price = begin_price + ( price_waypoint + 40 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_waypoint
            elif O == 4:
                price = begin_price + ( price_waypoint + 0 )
            elif O == 5:
                price = begin_price + ( price_waypoint + 0 )
            elif O == 0:
                price = begin_price + ( price_waypoint + 0 )

    
    elif UserGroup == 'NormalWalk':
        if S == 1:
            begin_price = 70
            price_waypoint = 40
            price_2waypoint = 40*2
            if O == 1: 
                price = begin_price + ( price_waypoint + 0 )
            elif O == 2:
                price = begin_price + ( price_waypoint + 40 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_waypoint
            elif O == 4:
                price = begin_price + ( price_waypoint + 0 )
            elif O == 0:
                price = begin_price + ( price_waypoint + 0 )
        
        elif S == 2:
            begin_price = 70
            price_waypoint = 40
            price_2waypoint = 40*2
            if O == 1: 
                price = begin_price + ( price_waypoint + 0 )
            elif O == 2:
                price = begin_price + ( price_waypoint + 40 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_waypoint
            elif O == 4:
                price = begin_price + ( price_waypoint + 0 )
            elif O == 0:
                price = begin_price + ( price_waypoint + 0 )

        elif S == 3:
            begin_price = 70
            price_waypoint = 40
            price_2waypoint = 40*2
            if O == 1: 
                price = begin_price + ( price_waypoint + 0 )
            elif O == 2:
                price = begin_price + ( price_waypoint + 40 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_waypoint
            elif O == 4:
                price = begin_price + ( price_waypoint + 0 )
            elif O == 0:
                price = begin_price + ( price_waypoint + 0 )


    elif UserGroup == '45skootar':
        if S == 1:
            begin_price = 65
            price_waypoint = 25
            price_2waypoint = 25*2
            if O == 1: 
                price = begin_price + ( price_waypoint + 15 )
            elif O == 2:
                price = begin_price + ( price_waypoint + 40 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_waypoint
            elif O == 4:
                price = begin_price + ( price_waypoint + 0 )
            elif O == 0:
                price = begin_price + ( price_waypoint + 0 )
        
        elif S == 2:
            begin_price = 65
            price_waypoint = 25
            price_2waypoint = 25*2
            if O == 1: 
                price = begin_price + ( price_waypoint + 15 )
            elif O == 2:
                price = begin_price + ( price_waypoint + 40 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_waypoint
            elif O == 4:
                price = begin_price + ( price_waypoint + 0 )
            elif O == 0:
                price = begin_price + ( price_waypoint + 0 )

        elif S == 3:
            begin_price = 65
            price_waypoint = 25
            price_2waypoint = 25 * 2
            if O == 1: 
                price = begin_price + ( price_waypoint + 15 )
            elif O == 2:
                price = begin_price + ( price_waypoint + 40 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_waypoint
            elif O == 4:
                price = begin_price + ( price_waypoint + 0 )
            elif O == 0:
                price = begin_price + ( price_waypoint + 0 )


    elif UserGroup == '201912_NormalFriend':
        if S == 1:
            begin_price = 59.83
            price_waypoint = 28
            price_2waypoint = 28*2
            if O == 1: 
                price = begin_price + ( price_waypoint + 14 )
            elif O == 2:
                price = begin_price + ( price_waypoint + 38 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_waypoint
            elif O == 0:
                price = begin_price + ( price_waypoint + 0 )


        elif S == 2:
            begin_price = 59.83
            price_waypoint = 28
            price_2waypoint = 28*2
            if O == 1: 
                price = begin_price + ( price_waypoint + 14 )
            elif O == 2:
                price = begin_price + ( price_waypoint + 38 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_waypoint
            elif O == 4:
                price = begin_price + ( price_waypoint + 0 )
            elif O == 0:
                price = begin_price + ( price_waypoint + 0 )

        elif S == 3:
            begin_price = 59.83
            price_waypoint = 28
            price_2waypoint = 28*2
            if O == 1: 
                price = begin_price + ( price_waypoint + 14 )
            elif O == 2:
                price = begin_price + ( price_waypoint + 38 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_waypoint
            elif O == 4:
                price = begin_price + ( price_waypoint + 0 )
            elif O == 0:
                price = begin_price + ( price_waypoint + 0 )


    elif UserGroup == 'P9Survey':
        if S == 1:
            begin_price = 360
            price_waypoint = 40
            price_2waypoint = 40*2
            if O == 1: 
                price = begin_price +  ( price_waypoint + 0 )
            elif O == 2:
                price = begin_price + ( price_waypoint + 40 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_waypoint
            elif O == 4:
                price = begin_price + ( price_waypoint + 0 )
            elif O == 0:
                price = begin_price + ( price_waypoint + 0 )
        
        elif S == 2:
            begin_price = 360
            price_waypoint = 40
            price_2waypoint = 40*2
            if O == 1: 
                price = begin_price + ( price_waypoint + 0 )
            elif O == 2:
                price = begin_price + ( price_waypoint + 40 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_waypoint
            elif O == 4:
                price = begin_price + ( price_waypoint + 0 )
            elif O == 0:
                price = begin_price + ( price_waypoint + 0 )

        elif S == 3:
            begin_price = 360
            price_waypoint = 40
            price_2waypoint = 40*2
            if O == 1: 
                price = begin_price + ( price_waypoint + 0 )
            elif O == 2:
                price = begin_price + ( price_waypoint + 40 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_waypoint
            elif O == 4:
                price = begin_price + ( price_waypoint + 0 )
            elif O == 0:
                price = begin_price + ( price_waypoint + 0 )

    elif UserGroup == '20210120_Robinhood':
        if S == 1:
            begin_price = 49
            price_waypoint = 26
            price_2waypoint = 26*2
            if O == 1: 
                price = begin_price +  ( price_waypoint + 55 )
            elif O == 2:
                price = begin_price + ( price_waypoint + 0 )
            elif O == 0:
                price = begin_price + ( price_waypoint + 0 )
        
        elif S == 2:
            begin_price = 49
            price_waypoint = 26
            price_2waypoint = 26*2
            if O == 1: 
                price = begin_price + ( price_waypoint + 51 )
            elif O == 2:
                price = begin_price + ( price_waypoint + 0 )
            elif O == 0:
                price = begin_price + ( price_waypoint + 0 )

        elif S == 3:
            begin_price = 49
            price_waypoint = 26
            price_2waypoint = 26*2
            if O == 1: 
                price = begin_price + ( price_waypoint + 41 )
            elif O == 2:
                price = begin_price + ( price_waypoint + 0 )
            elif O == 0:
                price = begin_price + ( price_waypoint + 0 )

    return int (math.ceil(price))   

#=========================================================#

def calPriceAdd2Point_UserGroup(S,O,UserGroup):
    if UserGroup == 'DEFAULT':
        if S == 1:
            begin_price = 70
            price_waypoint = 40
            price_2waypoint = 40*2
            if O == 1: 
                price = begin_price + ( price_2waypoint + 0 )
            elif O == 2:
                price = begin_price + ( price_2waypoint + 40 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_2waypoint
            elif O == 4:
                price = begin_price + ( price_2waypoint + 0 )
            elif O == 5:
                price = begin_price + ( price_2waypoint + 1 )
            elif O == 6:
                price = begin_price + ( price_2waypoint + 11 )
            elif O == 0:
                price = begin_price + ( price_2waypoint + 0 )
        
        elif S == 2:
            begin_price = 70
            price_waypoint = 40
            price_2waypoint = 40*2
            if O == 1: 
                price = begin_price + ( price_2waypoint + 0 )
            elif O == 2:
                price = begin_price + ( price_2waypoint + 40 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_2waypoint
            elif O == 4:
                price = begin_price + ( price_2waypoint + 0 )
            elif O == 5:
                price = begin_price + ( price_2waypoint + 5 )
            elif O == 6:
                price = begin_price + ( price_2waypoint + 20 )
            elif O == 0:
                price = begin_price + ( price_2waypoint + 0 )

        elif S == 3:
            begin_price = 70
            price_waypoint = 40
            price_2waypoint = 40*2
            if O == 1: 
                price = begin_price + ( price_2waypoint + 0 )
            elif O == 2:
                price = begin_price + ( price_2waypoint + 40 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_2waypoint
            elif O == 4:
                price = begin_price + ( price_2waypoint + 0 )
            elif O == 5:
                price = begin_price + ( price_2waypoint + 20 )
            elif O == 6:
                price = begin_price + ( price_2waypoint + 30 )    
            elif O == 0:
                price = begin_price + ( price_2waypoint + 0 )

        elif S == 4:
            begin_price = 70
            price_waypoint = 40
            price_2waypoint = 40*2
            if O == 1:
                price = ( begin_price + (begin_price * 0.50) ) + price_2waypoint
            elif O == 0:
                price = begin_price + ( price_2waypoint + 0 )
                

    elif UserGroup == '2019_NormalWP20':
        if S == 1:
            begin_price = 65
            price_waypoint = 20
            price_2waypoint = 20*2
            if O == 1: 
                price = begin_price +  ( price_2waypoint + 15 )
            elif O == 2:
                price = begin_price + ( price_2waypoint + 40 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_2waypoint
            elif O == 4:
                price = begin_price + ( price_2waypoint + 0 )
            elif O == 5:
                price = begin_price + ( price_2waypoint + 0 )
            elif O == 0:
                price = begin_price + ( price_2waypoint + 0 )
        
        elif S == 2:
            begin_price = 65
            price_waypoint = 20
            price_2waypoint = 20*2
            if O == 1: 
                price = begin_price + ( price_2waypoint + 15 )
            elif O == 2:
                price = begin_price + ( price_2waypoint + 40 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_2waypoint
            elif O == 4:
                price = begin_price + ( price_2waypoint + 0 )
            elif O == 5:
                price = begin_price + ( price_2waypoint + 0 )
            elif O == 0:
                price = begin_price + ( price_2waypoint + 0 )

        elif S == 3:
            begin_price = 65
            price_waypoint = 20
            price_2waypoint = 20*2
            if O == 1: 
                price = begin_price + ( price_2waypoint + 15 )
            elif O == 2:
                price = begin_price + ( price_2waypoint + 40 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_2waypoint
            elif O == 4:
                price = begin_price + ( price_2waypoint + 0 )
            elif O == 5:
                price = begin_price + ( price_2waypoint + 0 )
            elif O == 0:
                price = begin_price + ( price_2waypoint + 0 )

  
    elif UserGroup == 'NormalWalk':
        if S == 1:
            begin_price = 70
            price_waypoint = 40
            price_2waypoint = 40*2
            if O == 1: 
                price = begin_price + ( price_2waypoint + 0 )
            elif O == 2:
                price = begin_price + ( price_2waypoint + 40 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_2waypoint
            elif O == 4:
                price = begin_price + ( price_2waypoint + 0 )
            elif O == 0:
                price = begin_price + ( price_2waypoint + 0 )
        
        elif S == 2:
            begin_price = 70
            price_waypoint = 40
            price_2waypoint = 40*2
            if O == 1: 
                price = begin_price + ( price_2waypoint + 0 )
            elif O == 2:
                price = begin_price + ( price_2waypoint + 40 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_2waypoint
            elif O == 4:
                price = begin_price + ( price_2waypoint + 0 )
            elif O == 0:
                price = begin_price + ( price_2waypoint + 0 )

        elif S == 3:
            begin_price = 70
            price_waypoint = 40
            price_2waypoint = 40*2
            if O == 1: 
                price = begin_price +  begin_price + ( price_2waypoint + 0 )
            elif O == 2:
                price = begin_price + ( price_2waypoint + 40 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_2waypoint
            elif O == 4:
                price = begin_price + ( price_2waypoint + 0 )
            elif O == 0:
                price = begin_price + ( price_2waypoint + 0 )


    elif UserGroup == '45skootar':
        if S == 1:
            begin_price = 65
            price_waypoint = 25
            price_2waypoint = 25*2
            if O == 1: 
                price = begin_price + ( price_2waypoint + 15 )
            elif O == 2:
                price = begin_price + ( price_2waypoint + 40 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_2waypoint
            elif O == 4:
                price = begin_price + ( price_2waypoint + 0 )
            elif O == 0:
                price = begin_price + ( price_2waypoint + 0 )
        
        elif S == 2:
            begin_price = 65
            price_waypoint = 25
            price_2waypoint = 25*2
            if O == 1: 
                price = begin_price + ( price_2waypoint + 15 )
            elif O == 2:
                price = begin_price + ( price_2waypoint + 40 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_2waypoint
            elif O == 4:
                price = begin_price + ( price_2waypoint + 0 )
            elif O == 0:
                price = begin_price + ( price_2waypoint + 0 )

        elif S == 3:
            begin_price = 65
            price_waypoint = 25
            price_2waypoint = 25 * 2
            if O == 1: 
                price = begin_price + ( price_2waypoint + 15 )
            elif O == 2:
                price = begin_price + ( price_2waypoint + 40 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_2waypoint
            elif O == 4:
                price = begin_price + ( price_2waypoint + 0 )
            elif O == 0:
                price = begin_price + ( price_2waypoint + 0 )


    elif UserGroup == '201912_NormalFriend':
        if S == 1:
            begin_price = 59.83
            price_waypoint = 28
            price_2waypoint = 28*2
            if O == 1: 
                price = begin_price + ( price_2waypoint + 14 )
            elif O == 2:
                price = begin_price + ( price_2waypoint + 38 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_2waypoint
            elif O == 0:
                price = begin_price + ( price_2waypoint + 0 )


        elif S == 2:
            begin_price = 59.83
            price_waypoint = 28
            price_2waypoint = 28*2
            if O == 1: 
                price = begin_price + ( price_2waypoint + 14 )
            elif O == 2:
                price = begin_price + ( price_2waypoint + 38 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_2waypoint
            elif O == 4:
                price = begin_price + ( price_2waypoint + 0 )
            elif O == 0:
                price = begin_price + ( price_2waypoint + 0 )

        elif S == 3:
            begin_price = 59.83
            price_waypoint = 28
            price_2waypoint = 28*2
            if O == 1: 
                price = begin_price + ( price_2waypoint + 14 )
            elif O == 2:
                price = begin_price + ( price_2waypoint + 38 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_2waypoint
            elif O == 4:
                price = begin_price + ( price_2waypoint + 0 )
            elif O == 0:
                price = begin_price + ( price_2waypoint + 0 )


    elif UserGroup == 'P9Survey':
        if S == 1:
            begin_price = 360
            price_waypoint = 40
            price_2waypoint = 40*2
            if O == 1: 
                price = begin_price + ( price_2waypoint + 0 )
            elif O == 2:
                price = begin_price + ( price_2waypoint + 40 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_2waypoint
            elif O == 4:
                price = begin_price + ( price_2waypoint + 0 )
            elif O == 0:
                price = begin_price + ( price_2waypoint + 0 )
        
        elif S == 2:
            begin_price = 360
            price_waypoint = 40
            price_2waypoint = 40*2
            if O == 1: 
                price = begin_price + ( price_2waypoint + 0 )
            elif O == 2:
                price = begin_price + ( price_2waypoint + 40 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_2waypoint
            elif O == 4:
                price = begin_price + ( price_2waypoint + 0 )
            elif O == 0:
                price = begin_price + ( price_2waypoint + 0 )

        elif S == 3:
            begin_price = 360
            price_waypoint = 40
            price_2waypoint = 40*2
            if O == 1: 
                price = begin_price + ( price_2waypoint + 0 )
            elif O == 2:
                price = begin_price + ( price_2waypoint + 40 )
            elif O == 3:
                price = ( begin_price + (begin_price * 0.50) ) + price_2waypoint
            elif O == 4:
                price = begin_price + ( price_2waypoint + 0 )
            elif O == 0:
                price = begin_price + ( price_2waypoint + 0 )

    elif UserGroup == '20210120_Robinhood':
        if S == 1:
            begin_price = 49
            price_waypoint = 26
            price_2waypoint = 26*2
            if O == 1: 
                price = begin_price + ( price_2waypoint + 55 )
            elif O == 2:
                price = begin_price + ( price_2waypoint + 0 )
            elif O == 0:
                price = begin_price + ( price_2waypoint + 0 )
        
        elif S == 2:
            begin_price = 49
            price_waypoint = 26
            price_2waypoint = 26*2
            if O == 1: 
                price = begin_price + ( price_2waypoint + 51 )
            elif O == 2:
                price = begin_price + ( price_2waypoint + 0 )
            elif O == 0:
                price = begin_price + ( price_2waypoint + 0 )

        elif S == 3:
            begin_price = 49
            price_waypoint = 26
            price_2waypoint = 26*2
            if O == 1: 
                price = begin_price + ( price_2waypoint + 41 )
            elif O == 2:
                price = begin_price + ( price_2waypoint + 0 )
            elif O == 0:
                price = begin_price + ( price_2waypoint + 0 )

    return int (math.ceil(price))   
#print (calPrice (1,2))

#==================randomContactEx=================#

#def randomContactEx(contE):  
#   contE = random.choice(["0987654321","0987654323","0987632444","0833333333","0987654321","0999999999","0987654321","0987654321","0987654321","0935870200","0896754345","0987654321","0987654321","0987654321","0987654324","0987654324","0987654321","0815484188","0980000000","0888888888","0934585144","0890672209","0850424570","0822224123","0890424595","0846816844","0897800570","0636254565","0954919294","0639699997","0846424289","0924628809","0613919997","0804197971","0900000099","0978784234","0861636397","0926237636","0927022994","0926236121"]) 
#  return (contE)

def randomContactEx(contE):  
    contExt = ""
    for contExt in range(1):
        contExt += random.randint(1,101)
        contE = contExt
    return contE

#==================randomRole=================#
def randomRole(role):  
    #CH_OWNER = '//*[@id="OWNER"]'
    CH_ACCTAX = '//*[@id="ACC_TAX"]'
    CH_ACCONLINE = '//*[@id="ACC_ONLINE"]'
    CH_FINANCE = '//*[@id="FINANCE"]'
    CH_OFFICER = '//*[@id="OFFICER"]'
    role = random.choice([CH_ACCTAX, CH_ACCONLINE, CH_FINANCE, CH_OFFICER]) #CH_OWNER,
    return (role)

#==================randomVataddr=================#
def randomVataddr(vat):  
    VA_ADD1 = '//*[@id="AWN_No"]'
    VA_ADD2 = '//*[@id="AIS_No"]'
    VA_ADD3 = '//*[@id="AMP_No"]'
    VA_ADD4 = '//*[@id="WDS_No"]'
    VA_ADD5 = '//*[@id="DPC_No"]'
    VA_ADD6 = '//*[@id="AIR_No"]'
    VA_ADD7 = '//*[@id="SBN_No"]'
    VA_ADD8 = '//*[@id="ACC_No"]'
    VA_ADD9 = '//*[@id="MMT_No"]'
    vat = random.choice([VA_ADD1, VA_ADD2, VA_ADD3, VA_ADD4, VA_ADD5, VA_ADD6, VA_ADD7, VA_ADD8, VA_ADD9]) 
    return (vat)

#==================randomAddr=================#
def randomAddr(numberx):
    numberxx = ""
    for numberxx in range(1):
        numberxx += random.randint(1,101)
        numberx = numberxx
    return numberx

#print randomAddr(1)