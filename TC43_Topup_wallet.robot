﻿*** Settings ***
Library             Selenium2Library
Library             DatabaseLibrary
Library             BuiltIn
Library             String
Library             openpyxl
Library             ExcelLibrary
Library             Collections
Library             OperatingSystem
Library             Screenshot
Resource            Skootar_Keywords_Resource/skootar-topupwallet-keywords-file.robot
Variables           Skootar_Variables/skootar-variables.py
Variables           Skootar_Xpaths/skootar-topupwallet-xpath.py
#Suite Setup         Open Chrome
Suite Setup         Connect DB
Suite Teardown      Test Teardown Testcase 43

*** Test Cases ***
Test step 0: Connect DB                                     Connect DB
Test step 1: Open browser                                   Open Browser for Run Test
Test step 2: Login                                          Login with Email
Test step 3: Login Success                                  Login Success
Test step 4: Skootar Wallet                                 Skootar Wallet
Test step 5: Check Credit Before                            Check Credit Before
Test step 6: Topup Wallet                                   Topup Wallet
Test step 7: Input Prepaid Code                             Input Prepaid Code
Test step 8: Confirm Prepaid Code                           Confirm Prepaid Code
Test step 9: Check Credit After                             Check Credit After
End Case                                                    End Case Testcase 43

