﻿import os, sys
import random
import time
import datetime
xrange=range
# ************************** Auto Assign Sort Score **************************

def NowScore(distance,rating,level,lastad,adp,fav,deliverytype,job_option,isdriverdoc,isdriverfood,job_distance,job_zone,driver_zone,current_driver_zone,channel):  
    d = datetime.datetime.today()

    if (distance) <= 5 :

        if (distance < 0.5) :
            scoreDistance = 10
        elif (distance < 1) :
            scoreDistance = 9
        elif (distance < 2) :
            scoreDistance = 8
        elif (distance < 3) :
            scoreDistance = 7
        elif (distance < 5) :
            scoreDistance = 6
        elif (distance < 10) :
            scoreDistance = 5
        elif (distance < 15) :
            scoreDistance = 4
        elif (distance < 20) :
            scoreDistance = 3
        elif (distance < 30) :
            scoreDistance = 2
        else :
            scoreDistance = 1
          
        if (rating >= 5) :
            scoreRating = 10
        elif (rating >= 4.93) :
            scoreRating = 9
        elif (rating >= 4.89) :
            scoreRating = 8
        elif (rating >= 4.84) :
            scoreRating = 7
        elif (rating >= 4.80) :
            scoreRating = 6
        elif (rating >= 4.77) :
                scoreRating = 5
        elif (rating >= 4.73) :
           scoreRating = 4
        elif (rating >= 4.68) :
            scoreRating = 3
        elif (rating >= 4.61) :
            scoreRating = 2
        else :
            scoreRating = 1
        
        if (level == 3 ) :  
            scoreLevel = 5
        elif (level == 2 ) :  
            scoreLevel = 3
        elif (level == 1 ) :  
            scoreLevel = 1
        else :
            scoreLevel = 0

        if (fav == 1 ) :  
            scoreFav = 2
        else :
            scoreFav = 0

        if ( deliverytype == 1):
            if (isdriverdoc == 1 ) :
                #scoredoc = 3    ไม่บวกคะแนนแล้ว
                scoredoc = 0
            else:
                scoredoc = 0
        elif (deliverytype == 2 ) :
            if (isdriverdoc != 1):
                #scoredoc = 3    ไม่บวกคะแนนแล้ว
                scoredoc = 0
            else:
                scoredoc = 0  
        elif (deliverytype == 3 ) :
            if (isdriverdoc != 1):   
                #scoredoc = 3    ไม่บวกคะแนนแล้ว
                scoredoc = 0
                #if (isdriverfood == 1):
                    #scoredoc = 3
                #else :
                    #scoredoc = 0  
            else:
                scoredoc = 0
        else :
            scoredoc = 0  


    elif (distance) > 5 :

        if (distance < 0.5) :
            scoreDistance = 10
        elif (distance < 1) :
            scoreDistance = 9
        elif (distance < 2) :
            scoreDistance = 8
        elif (distance < 3) :
            scoreDistance = 7
        elif (distance < 5) :
            scoreDistance = 6
        elif (distance < 10) :
            scoreDistance = 5
        elif (distance < 15) :
            scoreDistance = 4
        elif (distance < 20) :
            scoreDistance = 3
        elif (distance < 30) :
            scoreDistance = 2
        else :
            scoreDistance = 1
          

        if (rating >= 5) :
            scoreRating = 10
        elif (rating >= 4.93) :
            scoreRating = 9
        elif (rating >= 4.89) :
            scoreRating = 8
        elif (rating >= 4.84) :
            scoreRating = 7
        elif (rating >= 4.80) :
            scoreRating = 6
        elif (rating >= 4.77) :
                scoreRating = 5
        elif (rating >= 4.73) :
           scoreRating = 4
        elif (rating >= 4.68) :
            scoreRating = 3
        elif (rating >= 4.61) :
            scoreRating = 2
        else :
            scoreRating = 1
        

        if (level == 3 ) :  
            scoreLevel = 5
        elif (level == 2 ) :  
            scoreLevel = 3
        elif (level == 1 ) :  
            scoreLevel = 1
        else :
            scoreLevel = 0


        if (fav == 1 ) :  
            scoreFav = 0
        else :
            scoreFav = 0
        

        if ( deliverytype == 1):
            if (isdriverdoc == 1 ) :
                #scoredoc = 3    ไม่บวกคะแนนแล้ว
                scoredoc = 0
            else:
                scoredoc = 0
        elif (deliverytype == 2 ) :
            if (isdriverdoc != 1):
                #scoredoc = 3    ไม่บวกคะแนนแล้ว
                scoredoc = 0
            else:
                scoredoc = 0  
        elif (deliverytype == 3 ) :
            if (isdriverdoc != 1):   
                #scoredoc = 3    ไม่บวกคะแนนแล้ว
                scoredoc = 0
                #if (isdriverfood == 1):
                    #scoredoc = 3
                #else :
                    #scoredoc = 0  
            else:
                scoredoc = 0   
        else :
            scoredoc = 0  

    if ( channel != "robinhood"):         
        if ( lastad != "null"):
            scoreAd =  10 - (d.day-lastad) + adp 
        else :
            scoreAd = 0  
    else :
        scoreAd = 0  


# ************************** Food Zone **************************

    if (deliverytype == 3) :      
        if (job_distance <= 8) :
            if (job_zone != "null" ) and (driver_zone != "null") :
                if (job_zone == driver_zone ) :
                    if (driver_zone == current_driver_zone ) :
                        if (distance <= 5 ) :
                            scoreZone = ( scoreDistance*2.5 )
                        elif (distance > 5 ) :
                            scoreZone = ( scoreDistance*1.8 )
                    elif (driver_zone != current_driver_zone ) :
                        scoreZone = 0    
                elif (job_zone != driver_zone ) :
                    if (driver_zone == current_driver_zone ) :
                        scoreZone = ( scoreDistance*1.8 )
                    elif (driver_zone != current_driver_zone ) :
                        scoreZone = 0  
                else :
                    scoreZone = 0 
            else :
                scoreZone = 0  
        else :
            scoreZone = 0  

    elif (deliverytype == 2) and (job_option == "null") : 
        if (job_distance <= 8) :
            if (job_zone != "null" ) and (driver_zone != "null") :
                if (job_zone == driver_zone ) :
                    if (driver_zone == current_driver_zone ) :
                        if (distance <= 5 ) :
                            scoreZone = ( scoreDistance*2.5 )
                        elif (distance > 5 ) :
                            scoreZone = ( scoreDistance*1.8 )
                    elif (driver_zone != current_driver_zone ) :
                        scoreZone = 0      
                elif (job_zone != driver_zone ) :
                    if (driver_zone == current_driver_zone ) :
                        scoreZone = ( scoreDistance*1.8 )
                    elif (driver_zone != current_driver_zone ) :
                        scoreZone = 0  
                else :
                    scoreZone = 0 
            else :
                scoreZone = 0  
        else :
            scoreZone = 0 

    elif (deliverytype == 2) and (job_option == 10) :   # job_option = 10 need box
        if (job_distance <= 8) :
            if (job_zone != "null" ) and (driver_zone != "null") :
                if (job_zone == driver_zone ) :
                    if (driver_zone == current_driver_zone ) :
                        if (distance <= 5 ) :
                            scoreZone = ( scoreDistance*2.5 )
                        elif (distance > 5 ) :
                            scoreZone = ( scoreDistance*1.8 )
                    elif (driver_zone != current_driver_zone ) :
                        scoreZone = 0     
                elif (job_zone != driver_zone ) :
                    if (driver_zone == current_driver_zone ) :
                        scoreZone = ( scoreDistance*1.8 )
                    elif (driver_zone != current_driver_zone ) :
                        scoreZone = 0  
                else :
                    scoreZone = 0 
            else :
                scoreZone = 0  
        else :
            scoreZone = 0 

    else :
            scoreZone = 0 


# ************************** End Food Zone **************************


    CalDistance = scoreDistance*0.95
    CalRating = scoreRating*0.05
    CalLevel = scoreLevel*0.05
    CalAD = scoreAd*0.25
    CalFav = scoreDistance*scoreFav
    CalDoc = scoredoc
    CalZone = scoreZone

    #sumPoint = CalDistance+CalRating+CalLevel+CalAD+CalFav+CalDoc    เอา CalAD ออก 21062021

    sumPoint = CalDistance+CalRating+CalLevel+CalFav+CalDoc+CalZone


    print '======================== NOW ============================'
    print 'Distance point = ' + str (CalDistance)
    print 'rating point = ' + str (CalRating)
    print 'level point = ' + str (CalLevel)
    #print 'ad point = ' + str (CalAD)
    print 'fav point = ' + str (CalFav)
    print 'doc point = ' + str (CalDoc)
    print 'zone point = ' + str (CalZone)
    print 'sumPoint = ' + str (sumPoint)

    return (sumPoint)



def ScheduleScore(distance,rating,level,lastad,adp,fav,deliverytype,isdriverdoc,isdriverfood):  
    d = datetime.datetime.today()
    if (distance < 0.5) :
        scoreDistance = 10
    elif (distance < 1) :
        scoreDistance = 9
    elif (distance < 2) :
        scoreDistance = 8
    elif (distance < 3) :
        scoreDistance = 7
    elif (distance < 5) :
        scoreDistance = 6
    elif (distance < 10) :
        scoreDistance = 5
    elif (distance < 15) :
        scoreDistance = 4
    elif (distance < 20) :
        scoreDistance = 3
    elif (distance < 30) :
        scoreDistance = 2
    else :
        scoreDistance = 1
          

    if (rating >= 5) :
        scoreRating = 10
    elif (rating >= 4.93) :
       scoreRating = 9
    elif (rating >= 4.89) :
        scoreRating = 8
    elif (rating >= 4.84) :
        scoreRating = 7
    elif (rating >= 4.80) :
        scoreRating = 6
    elif (rating >= 4.77) :
        scoreRating = 5
    elif (rating >= 4.73) :
        scoreRating = 4
    elif (rating >= 4.68) :
        scoreRating = 3
    elif (rating >= 4.61) :
        scoreRating = 2
    else :
        scoreRating = 1
        

    if (level == 3 ) :  
        scoreLevel = 5
    elif (level == 2 ) :  
        scoreLevel = 3
    elif (level == 1 ) :  
        scoreLevel = 1
    else :
        scoreLevel = 0


    if (fav == 1 ) :  
        scoreFav = 5
    else :
        scoreFav = 0


    if ( deliverytype == 1):
        if (isdriverdoc == 1 ) :
                #scoredoc = 3    ไม่บวกคะแนนแล้ว
                scoredoc = 0
        else:
            scoredoc = 0
    elif (deliverytype == 2 ) :
        if (isdriverdoc != 1):
                #scoredoc = 3    ไม่บวกคะแนนแล้ว
                scoredoc = 0
        else:
            scoredoc = 0  
    elif (deliverytype == 3 ) :
        if (isdriverdoc != 1):   
                #scoredoc = 3    ไม่บวกคะแนนแล้ว
                scoredoc = 0
            #if (isdriverfood == 1):
                #scoredoc = 3
            #else :
                #scoredoc = 0  
        else:
            scoredoc = 0
    else :
        scoredoc = 0    

    
    if ( lastad != "null"):
        scoreAd =  10 - (d.day-lastad) + adp 
    else :
        scoreAd = 0  


    #print('Current date and time: ', d)
    #print('Current year: ', d.year)
    #print('Current month: ', d.month)
    #print('Current day: ', d.day)
    CalDistance = scoreDistance*0.30
    CalRating = scoreRating*0.05
    CalLevel = scoreLevel*0.7
    CalAD = scoreAd*0.25
    CalFav = scoreFav*scoreDistance
    CalDoc = scoredoc

    #sumPoint = CalDistance+CalRating+CalLevel+CalAD+CalFav+CalDoc    เอา CalAD ออก 21062021
    sumPoint = CalDistance+CalRating+CalLevel+CalFav+CalDoc

    print 'job_zone = ' + str (job_zone)
    print '======================== SCHEDULE ============================'
    print 'Distance point = ' + str (CalDistance)
    print 'rating point = ' + str (CalRating)
    print 'level point = ' + str (CalLevel)
    #print 'ad point = ' + str (CalAD)
    print 'fav point = ' + str (CalFav)
    print 'doc point = ' + str (CalDoc)
    print 'sumPoint = ' + str (sumPoint)

    return (sumPoint)


# ************************** EstimateDuration API **************************

def EstimateDuration(distanceKm):  

    tripduration = (1 + 0.3) * (6.6 + 1.62 * distanceKm)
    ETA = tripduration + 30

    print '======================== EstimateDuration ============================'
    print 'tripduration(min) = ' + str (tripduration)
    print 'ETA +30(min) = ' + str (ETA)
    return (tripduration)

# ************************** End EstimateDuration API **************************


# ********** Sort Score *************

distance = 0.00370283753466897       # ระยะห่างงานกับแมส ไม่เกิน6ถึงกระจายเข้าแมสโซน
rating = 4.49            # แมส -- robo
level = 3               # แมส -- robo
lastad = 21             # แมส -- robo date latest login   "null" or int
ad = 0                  # แมส -- robo
fav = 1                # -- fav = 1 / normal = 0
deliverytype = 3        # 1=doc 2=parcel 3=food    
job_option = "null"     # 10=need box / "null"  1
isdriverdoc = 0         # แมส -- robo doc  0=N 1=Y
isdriverfood = 1        # แมส -- robo food  0=N 1=Y
job_distance = 12        # ระยะทางรวมงาน <=8km เป็นงาน zone
job_zone = "x"           # robo mission เอา lat long ไปหาใน robo zone / ไม่ใช่ zone ให้ใส่ "null"  
driver_zone = "x"         # tbl_skootar_zone เอาเวลาปัจจุบัน
current_driver_zone = "x"    #  robo skootar เอา lat long ไปหาใน robo zone
channel = "robinhood"        # robinhood,web,androind,ios,api
NowScore(distance,rating,level,lastad,ad,fav,deliverytype,job_option,isdriverdoc,isdriverfood,job_distance,job_zone,driver_zone,current_driver_zone,channel)
ScheduleScore(distance,rating,level,lastad,ad,fav,deliverytype,isdriverdoc,isdriverfood)
# ===================================================================================
distanceKm = 15.1879997253418
EstimateDuration(distanceKm)
