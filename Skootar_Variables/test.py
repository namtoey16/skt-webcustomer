﻿import os, sys
import random
import time
import datetime
xrange=range
#===========================  QUERY FILE EXCEL  ====================================
#from openpyxl import load_workbook 
#wb = load_workbook(filename = 'template-test-report.xlsx') 
#sheet_ranges = wb['test-report'] 
#TESTCASE = (sheet_ranges['A'+str(test.excelcell(0))].value)
#print(sheet_ranges['A'+str(test.excelcell(0))].value) 
#print MOBILE
#===================================================================================

def RamdomNumber100(x):  
    x = (random.randint(1, 100))
    return x

# ********** namtest *************

def namtest(toey):	
	a = ""
	for x in xrange(0,toey):
		a += random.choice('0123456789') 
	toey = a
	return (toey)

# ********** RandomIDCard *************
def RandomIDCard(x):
    # number of x don't start with 0 or 9
    x = random.choice(x)
    d = 13
    e = 0
 
    for i in xrange (0,11):  
        b = random.choice('0123456789')
        x+=str(b)
    
    # *** find the last number for ID Card *** 
    for a in x:
        a = int(a)*int(d)
        e += int(a)
        d-=1
    r = str(11-(int(e%11)))

    if len(r) >1 :
      for f in r :
        r = f  
    x = x+r
    return x

# ********** selectID *************
def selectID(idtype):
    if len(idtype) < 10 :
        id_type = ['0', random.choice('123456789') , namtest(11) ] 
    elif len(idtype) < 15:
        id_type = RandomIDCard('12345678')
    elif  len(idtype) > 15:
        id_type = ['0', random.choice('123456789') , namtest(11) ] 
    idtype = id_type
    return idtype

# ********** selectCOMP *************
def selectCOMP(comp):
    if len(comp) < 10 :
         comp_title = random.choice([u'บง.',u'บงล.',u'บจ.',u'บมจ.',u'บล.',u'หจก.',u'หสน.',u'หสม.']) 
    elif len(comp) < 15:
        comp_title = random.choice([u'นาย',u'นาง']) 
    elif  len(comp) > 15:
        comp_title = random.choice([u'บง.',u'บงล.',u'บจ.',u'บมจ.',u'บล.',u'หจก.',u'หสน.',u'หสม.']) 
    comp = comp_title
    return comp
    
# ********** randomDate *************
def randomDate(year):
        day = ""

        mouth = (random.randint(1, 12))
        if mouth < 10:
            mouth = '0' + str (mouth)
        if mouth == 1 or 3 or 5 or 7 or 8 or 10 or 12 :
            day = (random.randint(1, 31))
        elif  mouth == 4 or 6 or 9 or 11 :
            day = (random.randint(1, 30))
        elif  mouth == 2 :
            day = (random.randint(1, 29))
        if day < 10 :
            day = '0' + str (day)
        
        year = '20'+ namtest(2)

        random_date = ( str (day) + '/' + str (mouth) + '/' + str (year) )
        return random_date

#==================randomZipcode=================#
#def randomZipcode(zippcode):
#    zippcode = random.choice([10400,10600,10900,10210])
#    return zippcode 

#==================randomZip=================#
def randomZip(z):
    if z == 10400 :
        zipp = random.choice([u'ถนนเพชรบุรี',u'ถนนพญาไท',u'สามเสนใน',u'ทุ่งพญาไท',u'มักกะสัน'])
    elif z == 10600 :
        zipp = random.choice([u'คลองสาน'])
    elif  z == 10900 :
        zipp = random.choice([u'จตุจักร'])
    elif  z == 10210 :
        zipp = random.choice([u'ดอนเมือง',u'สนามบิน'])
    else:         
        print("ERROR ZIPCODE")
    return zipp

#==================randomBank=================#
def randomBank(numberb):
    numberbb = ""
    if numberb == '008' or numberb == '009':
       numberb = '1'

    else: 
        for numberb in range(1):
            numberb += random.randint(1,101)
    return numberb

#==================Todey=================#

def Date():
    from datetime import date
    today = date.today()
    print("Today's date:", today)
    return today
#print randomBank('001')


