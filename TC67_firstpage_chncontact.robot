﻿*** Settings ***
Library             Selenium2Library
Library             DatabaseLibrary
Library             BuiltIn
Library             String
Library             openpyxl
Library             ExcelLibrary
Library             Collections
Library             OperatingSystem
Library             Screenshot
Resource            Skootar_Keywords_Resource/skootar-chncontact-keywords-file.robot
Variables           Skootar_Variables/skootar-variables.py
Variables           Skootar_Xpaths/skootar-xpath.py
Suite Setup         Connect DB
Suite Teardown      Test Teardown Testcase 67
#Test TearDown      Custom Teardown

*** Test Cases ***
# Check Price No Login
Test step 1: Open browser                                   Open Browser for Run Test
Test step 2: Channel Contact Skootar                        Channel Contact Skootar
Test step 3: Contact by phone                               Contact by phone
Test step 4: Contact by line                                Contact by line
Test step 5: Contact by fb                                  Contact by fb
Test step 6: Go to Business Page                            Go to Business Page
Test step 7: Business Page                                  Business Page Channel Contact Skootar
Test step 8: Contact by phone                               Business Contact by phone
Test step 9: Contact by line                                Business Contact by line
End Case                                                    End Case Testcase 67

