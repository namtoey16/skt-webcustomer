﻿*** Settings ***
Library             Selenium2Library
Library             DatabaseLibrary
Library             BuiltIn
Library             String
Library             openpyxl
Library             ExcelLibrary
Library             Collections
Library             OperatingSystem
Library             Screenshot
Resource            Skootar_Keywords_Resource/skootar-alertinvoice-today-keywords-file.robot
Variables           Skootar_Variables/skootar-variables.py
Variables           Skootar_Xpaths/skootar-alertinvoice-xpath.py
Suite Setup         Connect DB
Suite Teardown      Test Teardown Testcase 37

*** Test Cases ***
Connect DB                              Connect DB
Test step 1: Open browser               Open Browser for Run Test
Test step 2: Login                      Login with Email
Test step 3: Login Success              Login Success
Test step 4: Check invoice              Alert Invoice
End Case                                End Case Testcase 37