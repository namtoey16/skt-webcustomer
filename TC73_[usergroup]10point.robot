﻿*** Settings ***
Library             Selenium2Library
Library             DatabaseLibrary
Library             BuiltIn
Library             String
Library             openpyxl
Library             ExcelLibrary
Library             Collections
Library             OperatingSystem
Library             Screenshot
Resource            Skootar_Keywords_Resource/skootar-user10p-keywords-file.robot
Variables           Skootar_Variables/skootar-variables.py
Variables           Skootar_Xpaths/skootar-xpath.py
Suite Setup         Connect DB
Suite Teardown      Test Teardown Testcase 73

*** Test Cases ***
# Check Price No Login
Test step 1: Open browser                                   Open Browser for Run Test
Test step 2: Login                                          Login with Email
Test step 3: Login Success                                  Login Success
Test step 4: STEP1 : Detail Direction Origin                Profile Origin
Test step 5: STEP1 : Detail Direction Destination 1         Profile Destination 1
Test step 6: STEP1 : Detail Direction Destination 2         Profile Destination 2
Test step 7: STEP1 : Detail Direction Destination 3         Profile Destination 3
Test step 8: STEP1 : Detail Direction Destination 4         Profile Destination 4
Test step 9: STEP1 : Detail Direction Destination 5         Profile Destination 5
Test step 10: STEP1 : Detail Direction Destination 6        Profile Destination 6
Test step 11: STEP1 : Detail Direction Destination 7        Profile Destination 7
Test step 12: STEP1 : Detail Direction Destination 8        Profile Destination 8
Test step 13: STEP1 : Detail Direction Destination 9        Profile Destination 9
Test step 15: Next step                                     Go to Step 2
Test step 16: STEP2 : Choose Service                        Select Service
Test step 17: STEP2 : Choose Option                         Select Option
Test step 18: Next step                                     Go to Step 3
Test step 19: Check Distance                                Calculate Distance
Test step 20: Check Price                                   Calculate Price
Test step 21: STEP3 : Sum Job                               Check Point Pay
End Case                                                    End Case Testcase 73