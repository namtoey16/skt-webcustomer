﻿*** Settings ***
Library             Selenium2Library
Library             DatabaseLibrary
Library             BuiltIn
Library             String
Library             openpyxl
Library             ExcelLibrary
Library             Collections
Library             OperatingSystem
Library             Screenshot
Resource            Skootar_Keywords_Resource/skootar-verifycreditcard-keywords-file.robot
Variables           Skootar_Variables/skootar-variables.py
Variables           Skootar_Xpaths/skootar-verifycreditcard-xpath.py
Suite Setup         Connect DB
Suite Teardown      Test Teardown Testcase 49

*** Test Cases ***
Test step 1: Open browser               Open Browser for Run Test
Test step 2: Login                      Login with Email
Test step 3: Login Success              Login Success
Test step 4: Go to Payment              Go to Payment
CASE1 Add New Creditcard                Creditcard Invalid_security_code
Clear Field                             Clear Field
CASE2 Add New Creditcard                Creditcard Fail 3DS Card Enrollment
CASE3 Add New Creditcard                Creditcard Fail 3DS Card Validation
CASE4 Add New Creditcard                Creditcard Insufficient_fund
CASE5 Add New Creditcard                Creditcard Stolen_or_lost_card
CASE6 Add New Creditcard                Creditcard Failed_processing
CASE7 Add New Creditcard                Creditcard Payment_rejected
CASE8 Add New Creditcard                Creditcard Failed_fraud_check
CASE9 Add New Creditcard                Creditcard Invalid_account_number
CASE10 Add New Creditcard               Creditcard Success
Remove Creditcard                       Remove Creditcard
End Case                                End Case Testcase 49