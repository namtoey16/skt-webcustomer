﻿*** Settings ***
Library             Selenium2Library
Library             DatabaseLibrary
Library             BuiltIn
Library             String
Library             openpyxl
Library             ExcelLibrary
Library             Collections
Library             OperatingSystem
Library             Screenshot
Resource            Skootar_Keywords_Resource/skootar-nologin3p-keywords-file.robot
Variables           Skootar_Variables/skootar-variables.py
Variables           Skootar_Xpaths/skootar-xpath.py
Suite Setup         Connect DB
Suite Teardown      Test Teardown Testcase 3

*** Test Cases ***
# Check Price No Login
Test step 1: Open browser                                   Open Browser for Run Test
Test step 2: Check Price                                    Go to Check Price
Test step 3: Go to New Order                                Alert Promotion
Test step 4: No Login Success                               No Login Success
Test step 5: STEP1 : Detail Direction Origin                Profile Origin
Test step 6: STEP1 : Detail Direction Destination 1         Profile Destination 1
Test step 7: STEP1 : Detail Direction Destination 2         Profile Destination 2
Test step 8: STEP1 : Detail Direction Destination 3         Profile Destination 3
Test step 9: Next step                                      Go to Step 2
Test step 10: STEP2 : Choose Service                        Select Service
Test step 11: STEP2 : Choose Option                         Select Option
Test step 12: Next step                                     Go to Step 3
Test step 13: Check Distance                                Calculate Distance
Test step 13: Check Price                                   Calculate Price
End Case                                                    End Case Testcase 3