﻿*** Settings ***
Library             Selenium2Library
Library             DatabaseLibrary
Library             BuiltIn
Library             String
Library             openpyxl
Library             ExcelLibrary
Library             Collections
Library             OperatingSystem
Library             Screenshot
Resource            Skootar_Keywords_Resource/skootar-changepassword-keywords-file.robot
Variables           Skootar_Variables/skootar-variables.py
Variables           Skootar_Xpaths/skootar-changepassword-xpath.py
Suite Setup         Connect DB
Suite Teardown      Test Teardown Testcase 9

*** Test Cases ***
Test step: Open Browser                     Open Browser For Run Test
Test step: Login                            Login With Email
Test step: Login Success                    Login Success
Test step: Go to Setting                    Go To Setting
Test step: Go to Change Password 1          Go To Change Password
Test case 1 * Under 6characters             Test Case Under 6characters
Test step: Clear Field 1                    Clear Field
Test case 2 * Password Mismatch             Test Case Password Mismatch
Test step: Clear Field 2                    Clear Field
Test case 3 * Invalid Password              Test Case Invalid Password
Test case 3 * Change Password Invalid       Change Password Invalid
Test step: Clear Field 3                    Clear Field
Test case 4 * Set New Password              Test Case Set New Password
Test case 4 * Change Password Success       Change Password Success
Test step: Go To Change Password 2          Go To Change Password
Test case 5 * Set Old Password              Test Case Set Old Password
Test case 5 * Change Password Success       Change Password Success
#Save Test Result                            Save Test Result Testcase 9
End Case                                    End Case Testcase 9